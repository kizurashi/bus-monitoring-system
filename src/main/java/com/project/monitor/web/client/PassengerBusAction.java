package com.project.monitor.web.client;

import com.project.monitor.activity.dao.ActivityDao;
import com.project.monitor.admin.bus.dao.BusDao;
import com.project.monitor.admin.bus.dao.BusStationDao;
import com.project.monitor.admin.bus.dao.BusTripsDao;
import com.project.monitor.admin.bus.model.BusTrips;
import com.project.monitor.admin.bus.model.FilterBus;
import com.project.monitor.admin.company.dao.CompanyBusStationsDao;
import com.project.monitor.admin.company.dao.CompanyBusTripsDao;
import com.project.monitor.admin.company.dao.CompanyDao;
import com.project.monitor.page.Pageable;
import com.project.monitor.permission.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping( "/" )
public class PassengerBusAction {

    @Autowired
    private UserDao userDao;

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private CompanyBusStationsDao busStationDao;

    @Autowired
    private BusDao busDao;

    @Autowired
    private CompanyBusTripsDao busTripsDao;

    private Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @RequestMapping( value = "", method = RequestMethod.GET)
    public String client( @RequestParam(required=false, defaultValue="1") int page,
                          @RequestParam(required = false, defaultValue = "10") int size,
                          Model model ) {
        model.addAttribute( "companies", companyDao.getCompanies() );
        model.addAttribute( "bustrips", busTripsDao.getCompanyBusTrips(14L, Pageable.create(page, size)) );
        model.addAttribute( "busStations", busStationDao.getCompanyBusStations( 14L, Pageable.create(page, 100000)) );
        FilterBus filterBus = FilterBus.builder().setBusStationId( 2L ).setCompanyId( 14L ).setBusTripId( 6L ).build();
        LOGGER.info("{}", busDao.filterBus( filterBus ) );
        model.addAttribute( "bus", busDao.filterBus( filterBus ) );
        return "/passenger/bus-trips";
    }
}
