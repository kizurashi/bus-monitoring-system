package com.project.monitor.web.admin;

import com.project.monitor.activity.dao.ActivityDao;
import com.project.monitor.activity.model.Activity;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping( "/admin" + ConstantConfig.ACTIVITY )
public class ActivityAction {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private ActivityDao activityDao;

    @RequestMapping( value = "/{companyId}", method = RequestMethod.GET )
    public String activity( @PathVariable( "companyId" ) Long companyId,
                            @RequestParam( required = false, defaultValue="1") int page,
                            @RequestParam( required = false, defaultValue="10") int size,
                            @RequestParam( required = false, defaultValue = "") String date,
                            Model model ) {
        Page<Map<String, List<Activity>>> activities  = activityDao.getActivityByCompany( companyId, date, Pageable.create( page, size ) ) ;
        model.addAttribute( "activities", activities );
        model.addAttribute( "date", date );
        return ConstantConfig.BUS_ACTIVITY_LIST;
    }

    @RequestMapping( value = "/add-activity", method = RequestMethod.GET )
    public String addActivityForm( Model model ) {
        return ConstantConfig.ADD_CONDUCTOR_PAGE;
    }

    @RequestMapping( value = "/add-activity", method = RequestMethod.POST )
    public String addActivity( @Valid Activity activity, BindingResult result, RedirectAttributes redirectAttributes ) {
        LOGGER.info( "Activity: {}", activity );
        return "redirect:/admin/activity";
    }
}
