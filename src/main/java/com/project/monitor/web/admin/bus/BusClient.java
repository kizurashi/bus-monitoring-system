package com.project.monitor.web.admin.bus;

import com.project.monitor.activity.dao.ActivityDao;
import com.project.monitor.admin.bus.dao.BusDao;
import com.project.monitor.admin.bus.model.Bus;
import com.project.monitor.admin.bus.model.BusReservedSeatSlot;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.PageRequest;
import com.project.monitor.page.Pageable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashSet;
import java.util.Set;

@Controller
@RequestMapping( "bus-client" )
public class BusClient {

    private Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private BusDao busDao;


    @RequestMapping( value = "", method = RequestMethod.GET )
    public String index( Model model ) {
        model.addAttribute( "page", "Bus Client" );
        return "/bus/index";
    }

    @RequestMapping( value = "/log-activities", method = RequestMethod.GET )
    public String getActivities( @RequestParam( required = false, defaultValue = "") String date,
                                 @RequestParam( required = false, defaultValue="1") int page,
                                 @RequestParam( required = false, defaultValue="10") int size,
                                 Model model ) {
        model.addAttribute( "activities", activityDao.getActivityByBus(3L, date, Pageable.create(page, size)) );
        model.addAttribute( "page", "Bus Client" );
        return "/bus/log-activity";
    }

    @RequestMapping( value = "/update-source-destination/{busId}", method = RequestMethod.POST )
    public String updateBusTravelMode(@PathVariable( "busId" ) Long busId,
                                      @RequestParam( "travelToDestination" ) Long travelToDestination,
                                      BindingResult result,
                                      RedirectAttributes redirectAttributes ) {
        Bus bus = busDao.getBus( busId );
        bus.setTravelToDestination( travelToDestination );
        busDao.saveBus( bus );
        redirectAttributes.addFlashAttribute( "message", "Success" );
        return "redirect:/admin/bus-client";
    }

}
