package com.project.monitor.web.admin.conductor;

import com.project.monitor.admin.company.dao.CompanyDao;
import com.project.monitor.admin.company.model.Company;
import com.project.monitor.admin.conductor.dao.ConductorDao;
import com.project.monitor.admin.conductor.model.Conductor;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Pageable;
import com.project.monitor.permission.dao.UserDao;
import com.project.monitor.permission.dao.UserRoleDao;
import com.project.monitor.permission.model.User;
import com.project.monitor.permission.model.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;
import java.util.Optional;

@Controller
@RequestMapping( "/admin" + ConstantConfig.CONDUCTOR )
public class ConductorAction {

    private Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private ConductorDao conductorDao;

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserRoleDao userRoleDao;

    private Company getCompanyDetails() {
        Optional<User> user = userDao.getUserCurrentlyLoggedIn();
        return companyDao.getCompanyByUserId( user.orElse( new User() ).getId() );
    }

    @RequestMapping( value = "", method = RequestMethod.GET)
    public String conductors( @RequestParam( required = false ) String conductorName,
                              @RequestParam( required = false, defaultValue="1") int page,
                              @RequestParam( required = false, defaultValue="10") int size,
                              Model model ) {
        Company company = getCompanyDetails();
        model.addAttribute( "company", company );
        model.addAttribute( "page", "conductor" );
        model.addAttribute( "conductors", conductorDao.getConductorsByCompanyId( company.getCompanyId(), conductorName, Pageable.create(page,size)) );
        return ConstantConfig.CONDUCTOR_LIST_PAGE;
    }

    @RequestMapping( value = "/add-conductor", method = RequestMethod.GET )
    public String addConductorForm( @ModelAttribute("conductorForm") Conductor conductorForm, Model model ) {
        //model.addAttribute( "conductorForm", new Conductor() );
        model.addAttribute( "page", "Add Conductor" );
        return ConstantConfig.ADD_CONDUCTOR_PAGE;
    }

    @RequestMapping( value = "/add-conductor", method = RequestMethod.POST )
    public String addConductor( @Valid Conductor conductor, BindingResult result, RedirectAttributes redirectAttributes ) {
        if( result.hasErrors() )
            return ConstantConfig.ADD_CONDUCTOR_PAGE;
        if( conductor.getConductorId() == null ) {
            if( userDao.getUserByUsername( conductor.getUser().getUsername() ) != null  ){
                redirectAttributes.addFlashAttribute(   "conductorForm", conductor );
                redirectAttributes.addFlashAttribute("status", "danger");
                redirectAttributes.addFlashAttribute("message", "Username " + conductor.getUser().getUsername() + " is already existed!");
                return "redirect:/admin/conductor/add-conductor";
            }
        }
        User user = conductor.getUser();
        user.setPassword("123456");
        UserRole userRole = userRoleDao.findRole( "CONDUCTOR" );
        if( userRole != null ) {
            Set<UserRole> userRoleSet = new HashSet<>();
            userRoleSet.add(userRole);
            user.setUserRoles( userRoleSet );
        }
        Long userId = userDao.saveUser( user );
        if( userId != null ){
            getCompanyDetails().getUser().setId(userId);
        }
        conductorDao.saveConductor( conductor );
        redirectAttributes.addFlashAttribute("status", "success");
        redirectAttributes.addFlashAttribute("message", "User " + conductor.getFirstName() + " "+ conductor.getLastName() + " added successfully");
        return "redirect:/admin/conductor/";
    }

    @RequestMapping( value = "/update-conductor/{conductorId}", method = RequestMethod.GET )
    public String updateConductorForm(@PathVariable( "conductorId" ) Long conductorId, Model model ) {
        Conductor conductor = conductorDao.getConductor( conductorId );
        model.addAttribute( "conductorForm", conductor );
        model.addAttribute( "page", "Update Conductor" );
        return ConstantConfig.ADD_CONDUCTOR_PAGE;
    }
}
