package com.project.monitor.web.admin.company;

import com.project.monitor.admin.company.dao.CompanyDao;
import com.project.monitor.admin.company.model.Company;
import com.project.monitor.admin.conductor.dao.ConductorDao;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.permission.dao.UserDao;
import com.project.monitor.permission.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping( "/admin" + ConstantConfig.COMPANY )
public class CompanyAction {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private UserDao userDao;

    @Autowired
    private CompanyDao companyDao;

    @Autowired
   private ConductorDao conductorDao;

    private Company getCompanyDetails( Model model ) {
        Optional<User> user = userDao.getUserCurrentlyLoggedIn();
        Company company = companyDao.getCompanyByUserId( user.orElse( new User() ).getId() );
        model.addAttribute( "permission",  user );
        model.addAttribute( "company",  company );
        return company;
    }

    @RequestMapping( value = "", method = RequestMethod.GET)
    public String getCompany( Model model ) {
        Company company = companyDao.getCompany(5L );
        model.addAttribute("page", "company" );
        return ConstantConfig.COMPANY_PROFILE;
    }

    @RequestMapping( value = "/update-profile", method = RequestMethod.GET )
    public String updateCompanyProfile( Model model ) {
        model.addAttribute("companyForm", new Company() );
        model.addAttribute( "page", "Update Company Profile" );
        return ConstantConfig.UPDATE_COMPANY_PROFILE;
    }

    @RequestMapping( value = "/update-profile", method = RequestMethod.POST )
    public String updateCompanyProfileForm( @Valid Company companyForm, BindingResult result, RedirectAttributes redirectAttributes ) {
        companyDao.saveCompany( companyForm );
        return "redirect:/admin/company/profile";
    }

    @RequestMapping( value = "/profile", method = RequestMethod.GET )
    public String companyProfile( Model model ) {
        model.addAttribute( "page", "Company Profile" );
        return ConstantConfig.COMPANY_PROFILE;
    }

}
