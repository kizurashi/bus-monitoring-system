package com.project.monitor.web.admin;

import com.project.monitor.activity.dao.ActivityDao;
import com.project.monitor.admin.bus.dao.BusDao;
import com.project.monitor.admin.bus.dao.BusTravelLogDao;
import com.project.monitor.admin.company.dao.CompanyDao;
import com.project.monitor.admin.company.model.Company;
import com.project.monitor.admin.conductor.dao.ConductorDao;
import com.project.monitor.admin.driver.dao.DriverDao;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.permission.dao.UserDao;
import com.project.monitor.permission.model.User;
import com.project.monitor.permission.model.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping
public class CommonAction {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private UserDao userDao;

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private BusDao busDao;

    @Autowired
    private DriverDao driverDao;

    @Autowired
    private ConductorDao conductorDao;

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private BusTravelLogDao busTravelLogDao;

    @RequestMapping( value = "/travel/logs/{busId}/{assigneeId}", method = RequestMethod.GET )
    public String busTravelLogs( @PathVariable Long busId,
                                 @PathVariable Long assigneeId,
                                 Model model) {
        LOGGER.info("{}", busTravelLogDao.getBusTravelLog(busId,assigneeId));
        model.addAttribute("travellog", busTravelLogDao.getBusTravelLog(busId,assigneeId));
        return "/bus/html/arrival-departure";
    }

    private Company getCompanyDetails( Model model ) {
        Optional<User> user = userDao.getUserCurrentlyLoggedIn();
        Company company = companyDao.getCompanyByUserId( user.orElse( new User() ).getId() );
        model.addAttribute( "permission",  user );
        model.addAttribute( "company",  company );
        model.addAttribute( "page", "Dashboard" );
        return company;
    }

    @RequestMapping( value = { "/admin", "/admin/dashboard" }, method = RequestMethod.GET )
    public String dashboard( Model model ) {
        Company company = getCompanyDetails( model );
        model.addAttribute( "busCount", busDao.getTotalBus( company.getCompanyId() ) );
        model.addAttribute( "conductorCount", conductorDao.getConductorCountByCompany( company.getCompanyId() ) );
        model.addAttribute( "driverCount", driverDao.getDriverCountByCompany( company.getCompanyId() ) );
        model.addAttribute( "activities", activityDao.getActivityPreviewByCompany( company.getCompanyId() ) );
        return ConstantConfig.DASHBOARD_PAGE;
    }

    @RequestMapping( value = "/login", method = RequestMethod.GET )
    public String login() {
        return ConstantConfig.LOGIN_PAGE;
    }

    @RequestMapping( value = "/privacy", method = RequestMethod.GET )
    public String privacy() {
        return "/privacy";
    }

    /**
     * This method handles logout requests.
     * Toggle the handlers if you are RememberMe functionality is useless in your app.
     */
    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "redirect:/admin/login?error=test&logout";
    }

    @RequestMapping( value = "/change-password", method = RequestMethod.GET )
    public String userProfile( Model model ) {
        getCompanyDetails( model );
        model.addAttribute( "page", "User Profile" );
        return "/change-password";
    }

    @RequestMapping( value = "/registration", method = RequestMethod.GET )
    public String registrationForm( Model model ) {
        model.addAttribute("companyForm", new Company() );
        return "/admin/register";
    }

    @RequestMapping( value = "/register", method = RequestMethod.POST )
    public String registerCompany( @Valid Company company, BindingResult result, RedirectAttributes redirectAttributes ) {
        LOGGER.warn( company.toString() );
        Set<UserRole> userRoles = new HashSet<>();
        UserRole userRole = new UserRole();
        userRole.setId(1);
        userRoles.add( userRole );
        company.getUser().setUserRoles( userRoles );
        companyDao.saveCompany( company );
        return "redirect:/admin/registration";
    }

}
