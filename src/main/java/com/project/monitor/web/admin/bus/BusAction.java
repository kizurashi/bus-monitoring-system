package com.project.monitor.web.admin.bus;

import com.project.monitor.admin.bus.dao.BusAssigneeDao;
import com.project.monitor.admin.bus.dao.BusStationDao;
import com.project.monitor.admin.bus.dao.BusTravelLogDao;
import com.project.monitor.admin.bus.dao.SourceDestinationDao;
import com.project.monitor.admin.bus.model.*;
import com.project.monitor.admin.bus.service.BusService;
import com.project.monitor.admin.company.dao.CompanyBusSourceDao;
import com.project.monitor.admin.company.dao.CompanyBusStationsDao;
import com.project.monitor.admin.company.dao.CompanyBusTripsDao;
import com.project.monitor.admin.company.dao.CompanyDao;
import com.project.monitor.admin.company.model.Company;
import com.project.monitor.admin.company.model.CompanyBusSource;
import com.project.monitor.admin.company.model.CompanyBusStations;
import com.project.monitor.admin.company.model.CompanyBusTrips;
import com.project.monitor.admin.conductor.dao.ConductorDao;
import com.project.monitor.admin.driver.dao.DriverDao;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.exception.ProjectRuntimeException;
import com.project.monitor.page.Pageable;
import com.project.monitor.permission.dao.UserDao;
import com.project.monitor.permission.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;

@Controller
@RequestMapping( "/admin" + ConstantConfig.BUS )
public class BusAction {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private UserDao userDao;

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private BusService busService;

    @Autowired
    private DriverDao driverDao;

    @Autowired
    private ConductorDao conductorDao;

    @Autowired
    private CompanyBusTripsDao companyBusTripsDao;

    @Autowired
    private CompanyBusStationsDao companyBusStationsDao;

    @Autowired
    private CompanyBusSourceDao companyBusSourceDao;

    @Autowired
    private SourceDestinationDao sourceDestinationDao;

    @Autowired
    private BusAssigneeDao busAssigneeDao;

    @Autowired
    private BusTravelLogDao busTravelLogDao;

    private Company getCompanyDetails() {
        Optional<User> user = userDao.getUserCurrentlyLoggedIn();
        return companyDao.getCompanyByUserId( user.orElse( new User() ).getId() );
    }


    @RequestMapping( value = {"/travel/logs"}, method = RequestMethod.GET)
    public String busTraveLogs( @RequestParam( required = false, defaultValue="") String date,
                          @RequestParam( required = false, defaultValue="1") int page,
                          @RequestParam( required = false, defaultValue="10") int size,
                          Model model ) {
        model.addAttribute( "page", "Bus Travel Logs" );
        model.addAttribute( "date", date );
        model.addAttribute( "traveLogs", busTravelLogDao.getBusTravelLogs( date, Pageable.create(page,size) ) );
        return "/admin/bus/bustravellogs";
    }

    @RequestMapping( value = {"", "/"}, method = RequestMethod.GET)
    public String busses( @RequestParam( required = false, defaultValue="") String plateNo,
                          @RequestParam( required = false, defaultValue="1") int page,
                          @RequestParam( required = false, defaultValue="10") int size,
                          Model model ) {
        Company company = getCompanyDetails();
        model.addAttribute( "page", "Bus" );
        model.addAttribute( "busses", busService.getBussesByCompanyId( company.getCompanyId(), plateNo, Pageable.create(page,size) ) );
        return ConstantConfig.BUS_LIST_PAGE;
    }

    @RequestMapping( value = "/{busId}", method = RequestMethod.GET)
    public String busProfile(@PathVariable( "busId" ) Long busId,
                             Model model ) {
        Bus bus = busService.getBus( busId );
        if( bus == null )
            throw new ProjectRuntimeException( String.format( "Bus with an id %d not found!", busId ) );
        model.addAttribute( "bus", bus );
        model.addAttribute( "busStations", new HashSet(bus.getBusTrips().getCompanyBusStations()) );
        model.addAttribute( "page", "Bus Profile " );
        return ConstantConfig.BUS_PROFILE;
    }

    @RequestMapping( value = "/add-bus", method = RequestMethod.GET )
    public String addBusForm( @RequestParam( required = false, defaultValue="1") int page,
                              @RequestParam( required = false, defaultValue="10") int size,
                              Model model ) {
        Company company = getCompanyDetails();
        model.addAttribute( "busForm", new Bus() );
        model.addAttribute( "customs", companyBusTripsDao.getCompanyBusTrips( company.getCompanyId(), Pageable.create(page,size) ) );
        model.addAttribute( "page", "Add Bus" );
        return ConstantConfig.ADD_BUS_PAGE;
    }

    @RequestMapping( value = "/update-bus/{busId}", method = RequestMethod.GET )
    public String updateBusForm( @RequestParam( required = false, defaultValue="1") int page,
                                 @RequestParam( required = false, defaultValue="10") int size,
                                 @PathVariable("busId") Long busId, Model model ) {
        Bus bus = busService.getBus( busId );
        LOGGER.info( "{}", bus );
        Company company = getCompanyDetails();
        model.addAttribute( "busForm", bus );
        model.addAttribute( "customs", companyBusTripsDao.getCompanyBusTrips( company.getCompanyId(), Pageable.create( page, size ) ) );
        model.addAttribute( "page", "Update Bus" );
        return ConstantConfig.ADD_BUS_PAGE;
    }

    @RequestMapping( value = "/bus-locator", method = RequestMethod.GET )
    public String trackBus( @RequestParam( required = false, defaultValue="") String plateNo,
                            @RequestParam( required = false, defaultValue="1") int page,
                            @RequestParam( required = false, defaultValue="10") int size,
                            Model model ) {
        Company company = getCompanyDetails();
        model.addAttribute( "page", "Bus Locator" );
        model.addAttribute( "busses", busService.getBussesByCompanyId( company.getCompanyId(), plateNo, Pageable.create(page,size) ) );
        return ConstantConfig.BUS_TRACKING_INDEX;
    }

    @RequestMapping( value = "/add-bus", method = RequestMethod.POST )
    public String addBus( @Valid Bus bus, BindingResult result, RedirectAttributes redirectAttributes ) {
        Company company = getCompanyDetails();
        if( result.hasErrors() )
            return ConstantConfig.ADD_CONDUCTOR_PAGE;
        bus.setAvailableSeat( bus.getPassengerCapacity() );
        busService.saveBus( bus );
        redirectAttributes.addFlashAttribute( "status", "success" );
        redirectAttributes.addFlashAttribute( "message", String.format("Successfully created bus %s", bus.getPlateNumber() ));
        return "redirect:/admin/bus";
    }

    @RequestMapping( value = "/assign/{busId}", method = RequestMethod.GET )
    public String assignConductDriverForm(@ModelAttribute("busAssigneeForm") BusAssignee busAssigneeForm,
                                          @PathVariable("busId") Long busId,
                                          Model model ) {
        Long companyId = getCompanyDetails().getCompanyId();
        model.addAttribute( "busId", busId );
        model.addAttribute( "conductors", conductorDao.getUnAssigneedConductors( companyId ) );
        model.addAttribute( "drivers", driverDao.getUnAssigneedDrivers( companyId ) );
        model.addAttribute( "page", "Add Bus Conductor & Driver Assignee" );
        model.addAttribute( "action", "Add" );
        return ConstantConfig.BUS_ASSIGN_CONDUCTOR_DRIVER;
    }

    @RequestMapping( value = "/update-assignee/{assigneeId}", method = RequestMethod.GET )
    public String updateConductDriverForm(@ModelAttribute("busAssigneeForm") BusAssignee busAssigneeForm,
                                          @PathVariable("assigneeId") Long assigneeId,
                                          Model model ) {
        Long companyId = getCompanyDetails().getCompanyId();
        model.addAttribute( "busAssigneeForm", busService.getBusAssignee( assigneeId ) );
        model.addAttribute( "conductors", conductorDao.getUnAssigneedConductors( companyId ) );
        model.addAttribute( "drivers", driverDao.getUnAssigneedDrivers( companyId ) );
        model.addAttribute( "page", "Update Bus Conductor & Driver Assignee" );
        model.addAttribute( "action", "Update" );
        return ConstantConfig.BUS_ASSIGN_CONDUCTOR_DRIVER;
    }


    @RequestMapping( value = "/assign", method = RequestMethod.POST )
    public String assignConductDriver(@Valid BusAssignee busAssignee, BindingResult result, RedirectAttributes redirectAttributes ) {
        BusAssignee retrievedBusAssignee = busAssigneeDao.validteBusSchedule(busAssignee);
        if( retrievedBusAssignee.getAssigneeId() != null ) {
            redirectAttributes.addFlashAttribute( "status", "danger" );
            redirectAttributes.addFlashAttribute( "message", String.format("Ooops! The inputted working hours is conflict with the other one.",
                    busAssignee.getBus().getPlateNumber() ));
            redirectAttributes.addFlashAttribute("busAssigneeForm", busAssignee);
            if( busAssignee.getAssigneeId() == null )
                return "redirect:/admin/bus/assign/"+busAssignee.getBus().getBusId();
            else
                return "redirect:/admin/bus/update-assignee/"+busAssignee.getBus().getBusId();
        }
        busService.saveBusAssignee( busAssignee );
        redirectAttributes.addFlashAttribute( "status", "success" );
        redirectAttributes.addFlashAttribute( "message", String.format("Successfully assign driver/conductor.") );
        return "redirect:/admin/bus";
    }

    @RequestMapping( value = "/bus-source-or-destination", method = RequestMethod.GET )
    public String busSourceOrDestination( @RequestParam( required = false, defaultValue="1") int page,
                                          @RequestParam( required = false, defaultValue="10") int size,
                                          Model model ) {
        Company company = getCompanyDetails();
        //model.addAttribute( "sourceForm", new SourceDestination() );
        model.addAttribute( "sources", companyBusSourceDao.getCompanyBusSources( company.getCompanyId(), Pageable.create(page, size) ) );
        model.addAttribute( "page", "Bus Source Or Destination" );
        return ConstantConfig.BUS_SOURCE_DESTINATION;
    }

    @RequestMapping( value = "/update-bus-source/{sourceId}", method = RequestMethod.GET )
    public String updateBusSourceForm( @PathVariable("sourceId") Long sourceId, Model model ) {
        model.addAttribute( "sourceForm", companyBusSourceDao.getCompanyBusSource( sourceId ) );
        return "/admin/bus/add-source-or-destination";
    }

    @RequestMapping( value = "/add-bus-source", method = RequestMethod.GET )
    public String addBusSourceForm( Model model ) {
        model.addAttribute( "company", getCompanyDetails() );
        model.addAttribute( "sourceForm", new CompanyBusSource() );
        return "/admin/bus/add-source-or-destination";
    }

    @RequestMapping( value = "/add-bus-source", method = RequestMethod.POST )
    public String addBusSource(@Valid CompanyBusSource sourceForm, BindingResult result, RedirectAttributes redirectAttributes ) {
        sourceForm.setCompany( getCompanyDetails() );
        busService.saveSourceDestination( sourceForm );
        redirectAttributes.addFlashAttribute( "status", "success" );
        redirectAttributes.addFlashAttribute( "message", String.format("Successfully assigned Bus Station(s) %s", sourceForm.getSourceDestination().getSource()) );
        return "redirect:/admin/bus/bus-source-or-destination";
    }

    @RequestMapping( value = "/assign/bus-station/{busTripId}", method = RequestMethod.GET )
    public String assignBusStationForm( @RequestParam( required = false, defaultValue="1") int page,
                                        @RequestParam( required = false, defaultValue="10") int size,
                                        @PathVariable("busTripId") Long busTripId, Model model ) {
        Long companyId = getCompanyDetails().getCompanyId();
        model.addAttribute( "busStations", companyBusStationsDao.getCompanyBusStations( companyId, Pageable.create(page,size) ) );
        model.addAttribute( "customSourceDestinationForm", new CompanyBusTrips() );
        model.addAttribute( "customSourceDestination", companyBusTripsDao.getCompanyBusTrip( busTripId ) );
        model.addAttribute( "page", "Assign Bus Station(s)" );
        return ConstantConfig.ASSIGN_BUS_STATION;
    }

    @RequestMapping( value = "/assign/bus-station", method = RequestMethod.POST )
    public String assignCustomSourceBusStation( @Valid CompanyBusTrips customSourceDestinationForm, BindingResult result, RedirectAttributes redirectAttributes ) {
        LOGGER.info("{}",  customSourceDestinationForm.getCompanyBusTripId() );
        CompanyBusTrips customSourceDestination = companyBusTripsDao.getCompanyBusTrip( customSourceDestinationForm.getCompanyBusTripId() );
        LOGGER.info("{}", customSourceDestination );
        customSourceDestination.setCompanyBusStations( new ArrayList<>( customSourceDestinationForm.getCompanyBusStations() ) );
        companyBusTripsDao.saveCompanyBusTrips( customSourceDestination );
        redirectAttributes.addFlashAttribute( "status", "success" );
        redirectAttributes.addFlashAttribute( "message", String.format("Successfully assigned Bus Station(s)") );
        return "redirect:/admin/bus/bus-trips";
    }

    @RequestMapping( value = "/bus-trips", method = RequestMethod.GET )
    public String busCustomSourceDestination(  @RequestParam( required = false, defaultValue="1") int page,
                                               @RequestParam( required = false, defaultValue="10") int size,
                                               Model model ) {
        Company company = getCompanyDetails();
        model.addAttribute( "busTripForm", new CompanyBusTrips() );
        LOGGER.info("{}",companyBusSourceDao.getCompanyBusSources( company.getCompanyId(), Pageable.create( page, size )));
        model.addAttribute( "companyBusSource", companyBusSourceDao.getCompanyBusSources( company.getCompanyId(), Pageable.create( page, 100000 )) );
        model.addAttribute( "customSourceDestinations", companyBusTripsDao.getCompanyBusTrips( company.getCompanyId(), Pageable.create(page, size) ) );
        model.addAttribute( "page", "Bus Trips" );
        return ConstantConfig.BUS_CUSTSOM_SOURCE_DESTINATION;
    }

    @RequestMapping( value = "/add-company-bus-trip", method = RequestMethod.POST )
    public String addCustomSourceDestination( @Valid CompanyBusTrips busTripForm, BindingResult result, RedirectAttributes redirectAttributes ) {
        busTripForm.setCompany( getCompanyDetails() );
        LOGGER.info("{}", busTripForm);
        busService.saveBusCustomSourceDestination(  busTripForm );
        redirectAttributes.addFlashAttribute( "status", "success" );
        redirectAttributes.addFlashAttribute("message", String.format("Successfully created bus station %s - %s",
                busTripForm.getBusTrips().getSource().getSource(),
                busTripForm.getBusTrips().getDestination().getSource()
            ));
        return "redirect:/admin/bus/bus-trips";
    }

    @RequestMapping( value = "/bus-station", method = RequestMethod.GET )
    public String busStation( @RequestParam( required = false, defaultValue="1") int page,
                              @RequestParam( required = false, defaultValue="10") int size,
                              Model model ) {
        Company company = getCompanyDetails();
        //model.addAttribute( "company", company );
        model.addAttribute( "stationForm", new BusStation() );
        model.addAttribute( "busStations", companyBusStationsDao.getCompanyBusStations( company.getCompanyId(), Pageable.create(page, size) ) );
        model.addAttribute( "page", "Bus Station" );
        return ConstantConfig.BUS_STATION;
    }

    @RequestMapping( value = "/add-bus-station", method = RequestMethod.GET )
    public String addBusStationForm( Model model ) {
        model.addAttribute( "stationForm", new CompanyBusStations() );
        model.addAttribute( "page", "Add Bus Station" );
        return "/admin/bus/add-bus-station";
    }

    @RequestMapping( value = "/update-bus-station/{companyBusStationId}", method = RequestMethod.GET )
    public String updateBusStationForm( @PathVariable("companyBusStationId") Long companyBusStationId, Model model ) {
        model.addAttribute( "stationForm", companyBusStationsDao.getCompanyBusStation( companyBusStationId ) );
        model.addAttribute( "page", "Update Bus Station" );
        return "/admin/bus/add-bus-station";
    }

    @RequestMapping( value = "/add-bus-station", method = RequestMethod.POST )
    public String addBusStation(@Valid CompanyBusStations companyBusStation, BindingResult result, RedirectAttributes redirectAttributes ) {
        companyBusStation.setCompany( getCompanyDetails() );
        busService.saveCompanyBusStation(  companyBusStation );
        redirectAttributes.addFlashAttribute( "status", "success" );
        redirectAttributes.addFlashAttribute("message", String.format("Successfully created bus station %s",
                companyBusStation.getBusStation().getStationName()));
        return "redirect:/admin/bus/bus-station";
    }
}