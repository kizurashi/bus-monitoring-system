package com.project.monitor.web.admin.driver;

import com.project.monitor.admin.company.dao.CompanyDao;
import com.project.monitor.admin.company.model.Company;
import com.project.monitor.admin.driver.dao.DriverDao;
import com.project.monitor.admin.driver.model.Driver;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Pageable;
import com.project.monitor.permission.dao.UserDao;
import com.project.monitor.permission.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping( "/admin" + ConstantConfig.DRIVER )
public class DriverAction {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private UserDao userDao;

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private DriverDao driverDao;

    private Company getCompanyDetails() {
        Optional<User> user = userDao.getUserCurrentlyLoggedIn();
        return companyDao.getCompanyByUserId( user.orElse( new User() ).getId() );
    }

    @RequestMapping( value = "", method = RequestMethod.GET)
    public String drivers( @RequestParam( required = false, defaultValue="") String driverName,
                           @RequestParam( required = false, defaultValue="1") int page,
                           @RequestParam( required = false, defaultValue="10") int size,
                           Model model ) {
        Company company = getCompanyDetails();
        model.addAttribute( "drivers", driverDao.getDriversByCompanyId( company.getCompanyId(), driverName, Pageable.create(page ,size)) );
        model.addAttribute( "page", "Driver" );
        return ConstantConfig.DRIVER_INDEX;
    }

    @RequestMapping( value = "/add-driver", method = RequestMethod.GET)
    public String addDriverForm( Model model ) {
        model.addAttribute( "page", "Driver" );
        model.addAttribute( "driverForm", new Driver() );
        model.addAttribute( "page", "Add Driver" );
        return ConstantConfig.ADD_DRIVER_PAGE;
    }

    @RequestMapping( value = "/update-driver/{driverId}", method = RequestMethod.GET)
    public String updateDriverForm(@PathVariable( "driverId" ) Long driverId, Model model ) {
        Driver driver = driverDao.getDriver( driverId );
        model.addAttribute( "page", "Driver" );
        model.addAttribute( "driverForm", driver );
        model.addAttribute( "page", "Update Driver" );
        return ConstantConfig.ADD_DRIVER_PAGE;
    }

    @RequestMapping( value = "/add-driver", method = RequestMethod.POST)
    public String addDriver( @Valid Driver driver, BindingResult result, RedirectAttributes redirectAttributes ) {
        if ( result.hasErrors() )
            return ConstantConfig.DRIVER_INDEX;
//        redirectAttributes.addFlashAttribute("driverForm", driver);
//        return "redirect:/admin/driver/add-driver";
        driverDao.saveDriver( driver );
        redirectAttributes.addFlashAttribute("status", "OK");
        redirectAttributes.addFlashAttribute("message", "User " + driver.getFirstName() + " "+ driver.getLastName() + " added successfully");
        return "redirect:/admin/driver";
    }

}
