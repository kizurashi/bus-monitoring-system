package com.project.monitor.websocket;

import com.project.monitor.activity.dao.ActivityDao;
import com.project.monitor.activity.model.Activity;
import com.project.monitor.admin.bus.dao.BusDao;
import com.project.monitor.admin.bus.dao.BusGeoLocationDao;
import com.project.monitor.admin.bus.model.Bus;
import com.project.monitor.admin.bus.model.BusReservedSeatSlot;
import com.project.monitor.admin.bus.model.BusStation;
import com.project.monitor.common.dao.GeoLocationDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private BusGeoLocationDao busGeoLocationDao;

    @Autowired
    private BusDao busDao;

    @Autowired
    private ActivityDao activityDao;

    @Autowired
    private GeoLocationDao geoLocationDao;

    @MessageMapping("/geolocation/{companyId}")
    @SendTo( "/topic/geolocation/{companyId}" )
    public Bus busGeoLocation(@DestinationVariable Long companyId, Bus bus ) throws Exception {
        bus.setCompanyId( companyId );
        return bus;
    }

    @MessageMapping( "/post/activity/{busId}" )
    @SendTo( "/topic/admin/listener")
    public Activity postActivity( @DestinationVariable Long busId, Activity activity ) {
        Bus bus = busDao.getBus( busId );
        activity.setBus(bus);
        activityDao.saveActivity( activity );
        bus.setLastActivity(activity);
        busDao.saveBus(bus);
        return activity;
    }

    @MessageMapping( "/update-bus-stop/{busId}" )
    @SendTo( "/topic/nextBusStop/listener")
    public Long updateBusNextStop( @DestinationVariable Long busId, Long stationId ) {
        Bus bus = busDao.getBus( busId );
        if( stationId == 0 ) {
            bus.setNextBusStop(null);
        } else {
            BusStation station = new BusStation();
            station.setStationId( stationId );
            bus.setNextBusStop( station );
        }
        busDao.saveBus( bus );
        return stationId;
    }

    @MessageMapping( "/update-available-seat/{busId}" )
    @SendTo( "/topic/availableSeat/listener")
    public Bus updateAvailabeSeat( @DestinationVariable Long busId, Long availableSeat ) {
        Bus bus = busDao.getBus( busId );
        bus.setAvailableSeat( availableSeat );
        busDao.saveBus( bus );
        return bus;
    }
}
