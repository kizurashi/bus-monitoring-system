package com.project.monitor.admin.driver.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project.monitor.admin.bus.model.BusAssignee;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Objects;

@Transactional
@Entity
@Table( name = "driver" )
public final class Driver implements Serializable{

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long driverId;

    @Column( name = "firstName", unique = true, nullable = false, length = 60 )
    private String firstName;

    @Column( name = "lastName", unique = true, nullable = false, length = 60 )
    private String lastName;

    @Column( name = "age", unique = true, nullable = false, length = 60 )
    private int age;

    /*@Column( name = "companyName", unique = true, nullable = false, length = 60 )
    private Address address;*/

    @Column( name = "contactNo", unique = true, nullable = false, length = 60 )
    private String contactNo;

    @Column( name = "emailAddress", unique = true, nullable = false, length = 60 )
    private String emailAddress;

    @JsonIgnoreProperties( { "driver", "conductor" } )
    @OneToOne( fetch = FetchType.LAZY, mappedBy = "driver", cascade = {CascadeType.MERGE, CascadeType.PERSIST} )
    private BusAssignee busAssignee;

    private Long companyId;

    public void setDriverId(Long driverId) {
        this.driverId = driverId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getDriverId() {
        return driverId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getContactNo() {
        return contactNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public BusAssignee getBusAssignee() {
        return busAssignee;
    }

    public void setBusAssignee(BusAssignee busAssignee) {
        this.busAssignee = busAssignee;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "id='" + driverId + '\'' +
                ",firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", contactNo='" + contactNo + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Driver driver = (Driver) o;
        return Objects.equals(driverId, driver.driverId) &&
                Objects.equals(firstName, driver.firstName) &&
                Objects.equals(lastName, driver.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(driverId);
    }

}
