package com.project.monitor.admin.driver.dao.impl;

import com.project.monitor.admin.driver.dao.DriverDao;
import com.project.monitor.admin.driver.model.Driver;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Transactional
public class DriverDaoImpl extends HibernateSessionFactoryImpl<Driver> implements DriverDao {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void saveDriver(Driver driver) {
        save( driver );
    }

    @Override
    public void updateDriver(Driver driver) throws Exception {
        save( driver );
    }

    @Override
    public void deleteDriver(Long driverId) {
        delete( driverId );
    }

    @Override
    public Page<Set<Driver>> getDrivers(Pageable pageable) {
        Query query = getSession().createQuery( String.format("from %s",
                                                getActualParameterize().getSimpleName() ) )
                                                .setFirstResult( pageable.getOffset() )
                                                .setMaxResults( pageable.getSize() );
        return new DefaultPage<>(new HashSet(query.list()), pageable, getDriversCount());
    }

    @Override
    public Driver getDriver( Long driverId ) {
        return get( driverId );
    }

    @Override
    public Page<Set<Driver>> getDriversByCompanyId(Long companyId, String driverName, Pageable pageable) {
        Disjunction disjunction = Restrictions.disjunction();
        if( driverName != null && driverName != "" ) {
            String[] name = driverName.split(" ");
            disjunction.add(Restrictions.disjunction(
                    Restrictions.like("firstName", name[0], MatchMode.ANYWHERE),
                    Restrictions.like("lastName", name[0], MatchMode.ANYWHERE)) );
            if( name.length >= 2 ) {
                disjunction.add(Restrictions.disjunction(
                        Restrictions.like("firstName", name[1], MatchMode.ANYWHERE),
                        Restrictions.like("lastName", name[1], MatchMode.ANYWHERE)) );
            }
        }
        Criteria criteria = getSession().createCriteria(Driver.class)
                                        .add(disjunction)
                                        .setFirstResult( pageable.getOffset() )
                                        .setMaxResults( pageable.getSize() );
        return new DefaultPage<>(new HashSet(criteria.list()), pageable, getDriverCountByCompany(companyId) );
    }

    @Override
    public List<Driver> getUnAssigneedDrivers( Long companyId ) {
        List<Driver> drivers =  getSession().createCriteria(Driver.class, "driver")
                                        .createAlias("driver.busAssignee", "busAssignee", JoinType.LEFT_OUTER_JOIN)
                                        .add( Restrictions.conjunction(
                                                Restrictions.isNull("busAssignee.assigneeId"),
                                                Restrictions.eq("companyId", companyId ) ) )
                                        .list();
        return new ArrayList<>( new HashSet<>( drivers ) );
    }

    @Override
    public Long getDriverCountByCompany(Long companyId) {
        return (Long) getSession()
                .createCriteria( Driver.class )
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }

    private Long getDriversCount() {
        return (Long) getSession()
                .createCriteria( Driver.class )
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }

}
