package com.project.monitor.admin.driver.controller;

import com.project.monitor.admin.driver.dao.DriverDao;
import com.project.monitor.admin.driver.model.Driver;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping( value = ConstantConfig.API_PREFIX + ConstantConfig.DRIVER )
public class DriverController {

    @Autowired
    private DriverDao driverDao;

    @RequestMapping( value = "", method = RequestMethod.GET )
    public Page<Set<Driver>> getDrivers( @RequestParam( required = false ) Long companyId,
                                         @RequestParam( required = false ) String driverName,
                                         @RequestParam( required = false, defaultValue="1") int page,
                                         @RequestParam( required = false, defaultValue="10") int size) {
        return driverDao.getDriversByCompanyId(companyId, driverName, Pageable.create(page, size));
    }

    @RequestMapping( value = "/{driverId}", method = RequestMethod.GET )
    public Driver getDriver( @PathVariable( value = "driverId" ) Long driverId ) {
        return driverDao.getDriver( driverId );
    }

    @RequestMapping( value = "/{driverId}", method = RequestMethod.DELETE )
    public void deleteDriver( @PathVariable( "driverId" ) Long driverId ) {
        driverDao.deleteDriver( driverId );
    }

    @RequestMapping( value = "", method = RequestMethod.PUT )
    public void updateDriver(@RequestBody Driver driver ) throws Exception {
        driverDao.updateDriver( driver );
    }

    @RequestMapping( value = "", method = RequestMethod.POST )
    public void saveDriver(@RequestBody Driver driver ) {
        driverDao.saveDriver( driver );
    }

}
