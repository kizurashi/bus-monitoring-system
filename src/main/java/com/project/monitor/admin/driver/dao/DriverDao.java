package com.project.monitor.admin.driver.dao;

import com.project.monitor.admin.driver.model.Driver;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import java.util.List;
import java.util.Set;

public interface DriverDao extends HibernateSessionFactory<Driver> {

    public void saveDriver(Driver driver);

    public void updateDriver(Driver driver) throws Exception;

    public void deleteDriver(Long driverId);

    public Page<Set<Driver>> getDrivers(Pageable pageable);

    //public List<Driver> getDrivers( int page, int limit );

    public Driver getDriver(Long driverId);

    public Page<Set<Driver>> getDriversByCompanyId(Long companyId, String driverName, Pageable pageable);

    public List<Driver> getUnAssigneedDrivers( Long companyId );

    public Long getDriverCountByCompany( Long companyId );
}
