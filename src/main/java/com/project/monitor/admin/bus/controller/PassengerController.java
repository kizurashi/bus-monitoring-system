package com.project.monitor.admin.bus.controller;

import com.project.monitor.admin.bus.dao.BusDao;
import com.project.monitor.admin.bus.model.Bus;
import com.project.monitor.admin.bus.model.FilterBus;
import com.project.monitor.config.ConstantConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping( "api/bus-search" )
public class PassengerController {

    @Autowired
    private BusDao busDao;

    @RequestMapping( value = "", method = RequestMethod.GET )
    public Set<Bus> searchBus( @RequestParam(name = "companyId", required =  false, defaultValue = "") Long companyId,
                                @RequestParam(name = "busTypeId", required =  false, defaultValue = "") Long busTypeId,
                                @RequestParam(name = "busTripId", required =  false, defaultValue = "") Long busTripId,
                                @RequestParam(name = "busStationId", required =  false, defaultValue = "") Long busStationId ) {
        FilterBus filterBus = FilterBus.builder()
                                            .setCompanyId( companyId )
                                            .setBusStationId( busStationId )
                                            .setBusTripId( busTripId )
                                            .setBusTypeId( busTypeId ).build();
        return busDao.filterBus( filterBus );
    }
}
