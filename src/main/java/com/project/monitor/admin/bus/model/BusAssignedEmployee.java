package com.project.monitor.admin.bus.model;

import com.project.monitor.admin.conductor.model.Conductor;
import com.project.monitor.admin.driver.model.Driver;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table( name = "assignedemployee" )
public class BusAssignedEmployee {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @ManyToOne
    @JoinColumn( name = "conductorId" )
    private Conductor conductor;

    @ManyToOne
    @JoinColumn( name = "driverId" )
    private Driver driver;

    @ManyToOne
    @JoinColumn( name = "busId" )
    private Bus bus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Conductor getConductor() {
        return conductor;
    }

    public void setConductor(Conductor conductor) {
        this.conductor = conductor;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BusAssignedEmployee that = (BusAssignedEmployee) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
