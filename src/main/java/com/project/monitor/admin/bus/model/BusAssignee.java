package com.project.monitor.admin.bus.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project.monitor.admin.conductor.model.Conductor;
import com.project.monitor.admin.driver.model.Driver;

import javax.persistence.*;
import java.sql.Time;
import java.util.Objects;

@Entity
@Table( name = "busassignee" )
public class BusAssignee {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long assigneeId;

    private Time timeIn;

    private Time timeOut;

    @ManyToOne
    @JoinColumn( name = "conductorId" )
    private Conductor conductor;

    @ManyToOne
    @JoinColumn( name = "driverId" )
    private Driver driver;

    @JsonIgnoreProperties( { "busAssignees" } )
    @ManyToOne
    @JoinColumn( name = "busId" )
    private Bus bus;

    public Long getAssigneeId() {
        return assigneeId;
    }

    public void setAssigneeId(Long assigneeId) {
        this.assigneeId = assigneeId;
    }

    public Time getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(Time timeIn) {
        this.timeIn = timeIn;
    }

    public Time getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Time timeOut) {
        this.timeOut = timeOut;
    }

    public Conductor getConductor() {
        return conductor;
    }

    public void setConductor(Conductor conductor) {
        this.conductor = conductor;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BusAssignee that = (BusAssignee) o;
        return Objects.equals(assigneeId, that.assigneeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(assigneeId);
    }
}
