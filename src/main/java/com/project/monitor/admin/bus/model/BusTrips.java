package com.project.monitor.admin.bus.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table( name = "bustrips" )
public class BusTrips {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long busTripId;

    @ManyToOne
    @JoinColumn( name = "sourceId")
    private SourceDestination source;

    @ManyToOne
    @JoinColumn( name = "destinationId" )
    private SourceDestination destination;

    public Long getBusTripId() {
        return busTripId;
    }

    public void setBusTripId(Long busTripId) {
        this.busTripId = busTripId;
    }

    public SourceDestination getSource() {
        return source;
    }

    public void setSource(SourceDestination source) {
        this.source = source;
    }

    public SourceDestination getDestination() {
        return destination;
    }

    public void setDestination(SourceDestination destination) {
        this.destination = destination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BusTrips busTrips = (BusTrips) o;
        return Objects.equals(busTripId, busTrips.busTripId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(busTripId);
    }

    @Override
    public String toString() {
        return "BusTrips{" +
                "busTripId=" + busTripId +
                ", source=" + source +
                ", destination=" + destination +
                '}';
    }
}
