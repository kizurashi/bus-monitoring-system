package com.project.monitor.admin.bus.model;

public class FilterBus {

    private Long companyId;

    private Long busTypeId;

    private Long busTripId;

    private Long busStationId;

    private FilterBus( Builder builder ) {
        companyId = builder.companyId;
        busTypeId = builder.busTypeId;
        busTripId = builder.busTripId;
        busStationId = builder.busStationId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public Long getBusTypeId() {
        return busTypeId;
    }

    public Long getBusTripId() {
        return busTripId;
    }

    public Long getBusStationId() {
        return busStationId;
    }

    public static Builder builder() {
        return new Builder();
    }
    public static class Builder {

        private Long companyId;

        private Long busTypeId;

        private Long busTripId;

        private Long busStationId;

        public  Builder setCompanyId(Long companyId) {
            this.companyId = companyId;
            return this;
        }

        public Builder setBusTypeId(Long busTypeId) {
            this.busTypeId = busTypeId;
            return this;
        }

        public Builder setBusTripId(Long busTripId) {
            this.busTripId = busTripId;
            return this;
        }

        public Builder setBusStationId(Long busStationId) {
            this.busStationId = busStationId;
            return this;
        }

        public FilterBus build() {
            return new FilterBus( this );
        }

    }

    @Override
    public String toString() {
        return "FilterBus{" +
                "companyId=" + companyId +
                ", busTypeId=" + busTypeId +
                ", busTripId=" + busTripId +
                ", busStationId=" + busStationId +
                '}';
    }
}
