package com.project.monitor.admin.bus.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Table(name = "busreservedseatslot")
@Entity
public class BusReservedSeatSlot  implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    private String code;

    private Long reservedSlot;

    private Timestamp created_at;

    @ManyToOne
    @JoinColumn( name = "stationId")
    private BusStation busStation;

    @ManyToOne
    @JoinColumn( name = "busId")
    private Bus bus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getReservedSlot() {
        return reservedSlot;
    }

    public void setReservedSlot(Long reservedSlot) {
        this.reservedSlot = reservedSlot;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public BusStation getBusStation() {
        return busStation;
    }

    public void setBusStation(BusStation busStation) {
        this.busStation = busStation;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BusReservedSeatSlot that = (BusReservedSeatSlot) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(code, that.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code);
    }

    @Override
    public String toString() {
        return "BusReservedSeatSlot{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", reservedSlot=" + reservedSlot +
                ", created_at=" + created_at +
                ", busStation=" + busStation +
                ", bus=" + bus +
                '}';
    }
}
