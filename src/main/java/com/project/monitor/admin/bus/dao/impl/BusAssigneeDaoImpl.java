package com.project.monitor.admin.bus.dao.impl;

import com.project.monitor.admin.bus.dao.BusAssigneeDao;
import com.project.monitor.admin.bus.model.BusAssignee;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Transactional
public class BusAssigneeDaoImpl extends HibernateSessionFactoryImpl<BusAssignee> implements BusAssigneeDao {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void saveAssignee(BusAssignee busAssignee) {
        if ( busAssignee.getAssigneeId() == null )
            save( busAssignee );
        else
            merge( busAssignee );
    }

    @Override
    public Page<List<BusAssignee>> getBusAssignees(Pageable pageable) {
        Query query = getSession().createQuery( String.format( "from %s",
                                                                getActualParameterize().getSimpleName() ) )
                                                .setFirstResult(pageable.getOffset())
                                                .setMaxResults(pageable.getPage());
        return new DefaultPage<>(query.list(), pageable, getBusAssigneesCount() );
    }

    @Override
    public BusAssignee getBusAssignee( Long busAssigneeId ) {
        return get( busAssigneeId );
    }

    @Override
    public List<BusAssignee> getBusAssigneeByBusId(Long busId ) {
/*        getBusAssignees( ).stream()
            .filter( busAssignee -> busAssignee.getBus().getBusId() == busId )
            .collect(Collectors.toList());*/
        return new ArrayList<>();
    }

    @Override
    public Page<List<BusAssignee>> getBusAssignees(Long companyId, Pageable pageable) {
        Criteria criteria = getSession().createCriteria( BusAssignee.class, "busAssignee" )
                                    .createAlias( "busAssignee.bus.company", "company" )
                                    .add(Restrictions.eq( "company.companyId", companyId ) )
                                    .setFirstResult( pageable.getOffset() )
                                    .setMaxResults( pageable.getPage() );
        return new DefaultPage<>(  criteria.list(), pageable, getBusAssigneesCount( companyId ) );
    }

    @Override
    public void deleteBusAssignee(Long busAssigneeId) {
        delete( busAssigneeId );
    }

    private Long getBusAssigneesCount( Long companyId ) {
        return (Long) getSession()
                .createCriteria( BusAssignee.class )
                .add(Restrictions.eq( "companyId", companyId ) )
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }

    private Long getBusAssigneesCount() {
        return (Long) getSession()
                .createCriteria( BusAssignee.class )
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }

    @Override
    public BusAssignee validteBusSchedule(BusAssignee busAssignee) {
        String testQuery = "";
        if( busAssignee.getAssigneeId() != null ){
            testQuery = "AND assigneeId != :assigneeId";
        }
        Query query = getSession().createQuery( String.format("from %s where busId = :busId AND ((timeIn BETWEEN :timeIn AND :timeOut) OR (timeOut BETWEEN :timeIn AND :timeOut) OR ((timeIn BETWEEN :timeOut AND :timeIn) OR (timeOut BETWEEN :timeOut AND :timeIn)))"+testQuery,
                                                    getActualParameterize().getSimpleName() ) )
                                    .setParameter("timeIn", busAssignee.getTimeIn())
                                    .setParameter("timeOut", busAssignee.getTimeOut())
                                    .setParameter("busId", busAssignee.getBus().getBusId());
        if( busAssignee.getAssigneeId() != null ){
            query.setParameter("assigneeId", busAssignee.getAssigneeId());
        }
        return query.list().isEmpty() ? new BusAssignee() : (BusAssignee)query.list().get(0);
    }
}
