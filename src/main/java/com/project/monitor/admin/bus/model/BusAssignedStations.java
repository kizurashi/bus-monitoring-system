package com.project.monitor.admin.bus.model;

import com.project.monitor.admin.company.model.CompanyBusTrips;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table( name = "busassigneestations" )
public class BusAssignedStations {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    private Long priority;

    @ManyToOne
    @JoinColumn( name = "stationId" )
    private BusStation busStation;

    private Long companyBusTripId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BusStation getBusStation() {
        return busStation;
    }

    public void setBusStation(BusStation busStation) {
        this.busStation = busStation;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public Long getCompanyBusTripId() {
        return companyBusTripId;
    }

    public void setCompanyBusTripId(Long companyBusTripId) {
        this.companyBusTripId = companyBusTripId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BusAssignedStations that = (BusAssignedStations) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "BusAssignedStations{" +
                "id=" + id +
                ", priority=" + priority +
                ", companyBusTripId=" + companyBusTripId +
                '}';
    }
}
