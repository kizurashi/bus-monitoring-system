package com.project.monitor.admin.bus.dao.impl;

import com.project.monitor.admin.bus.model.Bus;
import com.project.monitor.admin.bus.dao.BusDao;
import com.project.monitor.admin.bus.model.FilterBus;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.spel.ast.Projection;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Transactional
public class BusDaoImpl extends HibernateSessionFactoryImpl<Bus> implements BusDao {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void saveBus(Bus bus) {
        save( bus );
    }


    @Override
    public void updateBus(Bus bus) {
        save( bus );
    }

    @Override
    public void deleteBus( Long busId ) {
        delete( busId );
    }

    @Override
    public Page<List<Bus>> getBusses(String plateNo, Pageable pageable) {
        Query query = getSession().createQuery( String.format( "from %s",
                getActualParameterize().getSimpleName() ) )
                .setFirstResult(pageable.getOffset())
                .setMaxResults(pageable.getSize());
        return new DefaultPage<>(query.list(), pageable, getBusCountByCompany() );
    }

    @Override
    public Bus getBus( Long busId ) {
        return get( busId );
    }


    @Override
    public Page<Set<Bus>> getBussesByCompanyId(Long companyId, String plateNo, Pageable pageable) {
        Conjunction conjunction = Restrictions.conjunction();
        String query = String.format("from %s", getActualParameterize().getSimpleName());
        conjunction.add( Restrictions.eq( "companyId", companyId) );
        query += " WHERE companyId = " + companyId;
        if( plateNo != null ) {
            query += " AND plateNumber LIKE '%" + plateNo + "%'";
        }
        List<Bus> bus =  getSession().createQuery( query )
                .setFirstResult(pageable.getOffset())
                .setMaxResults(pageable.getSize())
                .list();
        return new DefaultPage<>( new HashSet<>(bus), pageable , getTotalBus( companyId ) );
    }

    @Override
    public List<Bus> getUnassigneedBusses(Long companyId) {
        /*return getBussesByCompanyId( companyId ).stream()
                                                    .filter( bus -> bus.getBusAssignee() == null )
                                                    .collect( Collectors.toList() );*/
        return null;
    }

    @Override
    public Long getBusCountByCompany() {
        return (Long) getSession()
                            .createCriteria( Bus.class )
                            .setProjection( Projections.rowCount() )
                            .uniqueResult();
    }

    @Override
    public Set<Bus> filterBus(FilterBus filterBus) {
        Conjunction conjunction = Restrictions.conjunction();
        if( filterBus.getCompanyId() != null) {
            conjunction.add( Restrictions.like( "companyId", filterBus.getCompanyId()) );
        }
        if( filterBus.getBusTripId() != null) {
            conjunction.add( Restrictions.like( "bustrips.busTripId", filterBus.getBusTripId()) );
        }
        if( filterBus.getBusStationId() != null ) {
            conjunction.add( Restrictions.like( "busStation.stationId", filterBus.getBusStationId()) );
        }
        if( filterBus.getBusTypeId() != null ) {
            conjunction.add( Restrictions.like( "bustrips.busTripId", filterBus.getBusTripId()) );
        }
        List<Bus> bus = (List<Bus>) getSession().createCriteria( Bus.class, "bus" )
                .createAlias("bus.busTrips.busTrips", "bustrips", JoinType.LEFT_OUTER_JOIN)
                .createAlias("bus.nextBusStop", "busStation", JoinType.LEFT_OUTER_JOIN)
                .add( conjunction ).list();
        return new HashSet<>(bus);
    }

    @Override
    public Long getTotalBus(Long companyId) {
         return (Long) getSession()
                 .createCriteria( Bus.class )
                 .add(Restrictions.eq( "companyId", companyId ) )
                 .setProjection( Projections.rowCount() )
                 .uniqueResult();
    }
}
