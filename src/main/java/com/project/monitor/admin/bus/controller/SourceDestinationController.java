package com.project.monitor.admin.bus.controller;

import com.project.monitor.admin.bus.dao.SourceDestinationDao;
import com.project.monitor.admin.bus.model.SourceDestination;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( ConstantConfig.API_PREFIX + ConstantConfig.BUS_SOURCE )
public class SourceDestinationController {

    @Autowired
    private SourceDestinationDao sourceDestinationDao;

    @RequestMapping( value = "/{sourceDestinationId}", method = RequestMethod.DELETE )
    public void deleteBusStation(@PathVariable( "sourceDestinationId" ) Long sourceDestinationId ) {
        sourceDestinationDao.deleteBusSourceDestination( sourceDestinationId );
    }

    @RequestMapping( value = "", method = RequestMethod.GET )
    public Page<List<SourceDestination>> busStations(@RequestParam( required = false, defaultValue="1") int page,
                                                     @RequestParam( required = false, defaultValue="10") int size) {
        return sourceDestinationDao.getBusSourceDestinations( Pageable.create( page, size ) );
    }
}
