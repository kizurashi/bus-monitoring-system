package com.project.monitor.admin.bus.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project.monitor.activity.model.Activity;
import com.project.monitor.admin.company.model.CompanyBusTrips;
import com.project.monitor.common.model.GeoLocation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table( name = "bus" )
@JsonIgnoreProperties( { "activities", "busAssignees" } )
public final class Bus implements Serializable{

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long busId;

    @Column( name = "plateNumber", nullable = false, unique = true, length = 20)
    private String plateNumber;

    @Column( name = "passengerCapacity", nullable = false )
    private Long passengerCapacity;

    @Column( name = "availableSeat" )
    private Long availableSeat;

    @OneToOne( fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.PERSIST } )
    @JoinColumn( name = "locationId" )
    private GeoLocation currentGeoLocation;

    @ManyToOne//( cascade = CascadeType.ALL )
    @JoinColumn( name = "stationId" )
    private BusStation nextBusStop;

    private Long companyId;

    private Long travelToDestination;

    @JsonIgnoreProperties( { "bus" } )
    @OneToMany( fetch = FetchType.EAGER, mappedBy = "bus" )
    private Set<BusAssignee> busAssignees = new HashSet<>();

    @JsonIgnoreProperties( { "bus" } )
    @OneToOne
    @JoinColumn( name = "companyBusTripId" )
    private CompanyBusTrips busTrips;

/*    @OneToMany( fetch = FetchType.EAGER, mappedBy = "bus", cascade = { CascadeType.MERGE, CascadeType.PERSIST } )
    private Set<Activity> activities = new HashSet<>();*/

    @OneToOne
    @JoinColumn( name = "lastActivityId")
    @JsonIgnoreProperties( { "bus" } )
    private Activity lastActivity;

    public void setBusId(Long busId) {
        this.busId = busId;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public void setPassengerCapacity(Long passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }

    public void setAvailableSeat(Long availableSeat) {
        this.availableSeat = availableSeat;
    }

    public void setCurrentGeoLocation(GeoLocation currentGeoLocation) {
        this.currentGeoLocation = currentGeoLocation;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

/*    public void setActivities(Set<Activity> activities) {
        this.activities = activities;
    }*/

    public Long getBusId() {
        return busId;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public Long getPassengerCapacity() {
        return passengerCapacity;
    }

    public Long getAvailableSeat() {
        return availableSeat;
    }

    public GeoLocation getCurrentGeoLocation() {
        return currentGeoLocation;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public CompanyBusTrips getBusTrips() {
        return busTrips;
    }

    public void setBusTrips(CompanyBusTrips busTrips) {
        this.busTrips = busTrips;
    }

    public Set<BusAssignee> getBusAssignees() {
        return busAssignees;
    }

    public void setBusAssignees(Set<BusAssignee> busAssignees) {
        this.busAssignees = busAssignees;
    }

    public BusStation getNextBusStop() {
        return nextBusStop;
    }

    public void setNextBusStop(BusStation nextBusStop) {
        this.nextBusStop = nextBusStop;
    }

    public Long getTravelToDestination() {
        return travelToDestination;
    }

    public void setTravelToDestination(Long travelToDestination) {
        this.travelToDestination = travelToDestination;
    }

    public Activity getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(Activity lastActivity) {
        this.lastActivity = lastActivity;
    }

    /*    public Set<Activity> getActivities() {
        return activities;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bus bus = (Bus) o;
        return Objects.equals(plateNumber, bus.plateNumber);
    }

    @Override
    public int hashCode() {

        return Objects.hash(plateNumber);
    }

    @Override
    public String toString() {
        return "Bus{" +
                "busId=" + busId +
                ", plateNumber='" + plateNumber + '\'' +
                ", passengerCapacity=" + passengerCapacity +
                ", availableSeat=" + availableSeat +
                ", currentGeoLocation=" + currentGeoLocation +
                ", nextStop=" + nextBusStop +
                ", companyId=" + companyId +
                '}';
    }
}
