package com.project.monitor.admin.bus.dao.impl;

import com.project.monitor.admin.bus.dao.BusTripsDao;
import com.project.monitor.admin.bus.model.Bus;
import com.project.monitor.admin.bus.model.BusTrips;
import com.project.monitor.admin.bus.model.SourceDestination;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import javax.persistence.PersistenceException;
import java.util.List;

public class BusTripsDaoImpl extends HibernateSessionFactoryImpl<BusTrips> implements BusTripsDao {

    @Override
    public void saveBusTrip(BusTrips busTrip) {
        save( busTrip );
    }

    @Override
    public void deleteBusTrip(Long busTripId) {
        delete( busTripId );
    }

    @Override
    public Page<List<BusTrips>> getBusTrips(Pageable pageable) {
        Query query = getSession().createQuery( String.format( "from %s",
                                                    getActualParameterize().getSimpleName() ) )
                                    .setFirstResult(pageable.getOffset())
                                    .setMaxResults(pageable.getSize());
        return new DefaultPage<>(query.list(), pageable, getBusTripsCount());
    }

    @Override
    public BusTrips getBusTrip( Long busTripId ) {
        return get( busTripId );
    }

    @Override
    public BusTrips getBusTrip( BusTrips busTrips ) {
        Criteria criteria =  getSession().createCriteria(BusTrips.class, "busTrips")
                .createAlias( "busTrips.source", "origin")
                .createAlias( "busTrips.destination", "destination")
                .add(   Restrictions.or(
                            Restrictions.and(
                                    Restrictions.eq( "origin.sourceId", busTrips.getSource().getSourceId() ),
                                    Restrictions.eq( "destination.sourceId", busTrips.getDestination().getSourceId() )
                            ),
                            Restrictions.and(
                                    Restrictions.eq( "origin.sourceId", busTrips.getDestination().getSourceId() ),
                                    Restrictions.eq( "destination.sourceId", busTrips.getSource().getSourceId() )
                            )
                        )
                );
        return (BusTrips)( criteria.list().isEmpty()? null : criteria.list().get(0) );
    }

    private Long getBusTripsCount() {
        return (Long) getSession()
                .createCriteria( BusTrips.class )
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }
}
