package com.project.monitor.admin.bus.dao.impl;

import com.project.monitor.admin.bus.dao.BusGeoLocationDao;
import com.project.monitor.common.model.GeoLocation;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;

import javax.transaction.Transactional;

@Transactional
public class BusGeoLocationDaoImpl extends HibernateSessionFactoryImpl<GeoLocation> implements BusGeoLocationDao {

    @Override
    public void saveBusGeoLocation(GeoLocation busGeoLocation) {
        save( busGeoLocation );
    }

    @Override
    public GeoLocation getBusGeoLocation(Long busId) {
        return get( busId );
    }

    @Override
    public void deleteBusAssignee(Long busId) {
        delete( busId );
    }
}
