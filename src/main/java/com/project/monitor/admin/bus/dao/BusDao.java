package com.project.monitor.admin.bus.dao;

import com.project.monitor.admin.bus.model.Bus;
import com.project.monitor.admin.bus.model.FilterBus;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

public interface BusDao extends HibernateSessionFactory<Bus> {

    public void saveBus(Bus bus);

    public void updateBus(Bus bus);

    public void deleteBus(Long busId);

    public Page<List<Bus>> getBusses(String plateNo, Pageable pageable);

    public Bus getBus(Long busId);

    public Page<Set<Bus>> getBussesByCompanyId(Long companyId, String plateNo, Pageable pageable);

    public List<Bus> getUnassigneedBusses( Long companyId );

    public Long getBusCountByCompany();

    public Set<Bus> filterBus(FilterBus filterBus);

    public Long getTotalBus( Long companyId );

}
