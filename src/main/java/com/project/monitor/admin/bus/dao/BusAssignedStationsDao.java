package com.project.monitor.admin.bus.dao;

import com.project.monitor.admin.bus.model.BusAssignedEmployee;
import com.project.monitor.admin.bus.model.BusAssignedStations;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import java.util.List;

public interface BusAssignedStationsDao extends HibernateSessionFactory<BusAssignedStations> {

    public void saveBusAssignedStation( BusAssignedStations busAssignedStations );

    public void deleteBusAssignedStation( Long id );

    public Page<List<BusAssignedStations>> getBusAssignedStations(Long id, Pageable pageable);

}
