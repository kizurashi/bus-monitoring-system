package com.project.monitor.admin.bus.controller;

import com.project.monitor.admin.bus.dao.BusStationDao;
import com.project.monitor.admin.bus.model.BusStation;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( ConstantConfig.API_PREFIX + ConstantConfig.BUS_STATION_API )
public class BusStationController {

    @Autowired
    private BusStationDao busStationDao;

    @RequestMapping( value = "/{busStationId}", method = RequestMethod.DELETE )
    public void deleteBusStation(@PathVariable( "busStationId" ) Long busStationId ) {
        busStationDao.deleteBusStation( busStationId );
    }

    @RequestMapping( value = "", method = RequestMethod.GET )
    public Page<List<BusStation>> busStations(@RequestParam( required = false, defaultValue="1") int page,
                                              @RequestParam( required = false, defaultValue="10") int size) {
        return busStationDao.getBusStations(Pageable.create(page, size));
    }
}
