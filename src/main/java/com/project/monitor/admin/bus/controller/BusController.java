package com.project.monitor.admin.bus.controller;

import com.project.monitor.admin.bus.model.Bus;
import com.project.monitor.admin.bus.dao.BusDao;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping( value = ConstantConfig.API_PREFIX + ConstantConfig.BUS  )
public class BusController {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private BusDao busDao;

    @RequestMapping( value = "", method = RequestMethod.GET )
    public Page<Set<Bus>> getBusses(@RequestParam( required = false, defaultValue="") Long companyId,
                                     @RequestParam( required = false, defaultValue="") String plateNo,
                                     @RequestParam( required = false, defaultValue="1") int page,
                                     @RequestParam( required = false, defaultValue="10") int size) {
        return busDao.getBussesByCompanyId(companyId, plateNo, Pageable.create(page,size));
    }

    @RequestMapping( value = "/{busId}", method = RequestMethod.GET )
    public Bus getBus( @PathVariable( value = "busId" ) Long busId ) {
        return busDao.getBus( busId );
    }

    @RequestMapping( value = "/{busId}", method = RequestMethod.DELETE )
    public void deleteBus( @PathVariable( "busId" ) Long busId ) {
        busDao.deleteBus( busId );
    }

    @RequestMapping( value = "", method = RequestMethod.PUT )
    public void updateBus(@RequestBody Bus bus ) throws Exception {
        busDao.updateBus( bus );
    }

    @RequestMapping( value = "/reverse-bus-origin/{busId}", method = RequestMethod.POST )
    public void reverseBusOrigin( @PathVariable( "busId" ) Long busId,
                                  Long travelMode ) throws Exception {
        Bus bus = busDao.getBus( busId );
        bus.setTravelToDestination( travelMode );
        busDao.saveBus( bus );
    }
}
