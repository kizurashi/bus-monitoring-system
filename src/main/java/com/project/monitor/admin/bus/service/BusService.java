package com.project.monitor.admin.bus.service;

import com.project.monitor.admin.bus.dao.*;
import com.project.monitor.admin.bus.model.*;
import com.project.monitor.admin.company.dao.CompanyBusSourceDao;
import com.project.monitor.admin.company.dao.CompanyBusStationsDao;
import com.project.monitor.admin.company.dao.CompanyBusTripsDao;
import com.project.monitor.admin.company.model.Company;
import com.project.monitor.admin.company.model.CompanyBusSource;
import com.project.monitor.admin.company.model.CompanyBusStations;
import com.project.monitor.admin.company.model.CompanyBusTrips;
import com.project.monitor.admin.conductor.dao.ConductorDao;
import com.project.monitor.admin.conductor.model.Conductor;
import com.project.monitor.admin.driver.dao.DriverDao;
import com.project.monitor.admin.driver.model.Driver;
import com.project.monitor.common.model.GeoLocation;
import com.project.monitor.exception.ProjectRuntimeException;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Set;
import java.util.List;

@Transactional
public class BusService {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private BusDao busDao;

    @Autowired
    private BusAssigneeDao busAssigneeDao;

    @Autowired
    private BusGeoLocationDao busGeoLocationDao;

    @Autowired
    private BusStationDao busStationDao;

    @Autowired
    private CompanyBusStationsDao companyBusStationsDao;

    @Autowired
    private DriverDao driverDao;

    @Autowired
    private ConductorDao conductorDao;

    @Autowired
    private SourceDestinationDao sourceDestinationDao;

    @Autowired
    private CompanyBusTripsDao companyBusTripsDao;

    @Autowired
    private CompanyBusSourceDao companyBusSourceDao;

    @Autowired
    private BusTripsDao busTripsDao;

    public Bus getBus( Long busId ) {
        if ( busId == null )
            throw new ProjectRuntimeException( "busId can't be null!" );
        return busDao.getBus( busId );
    }

    public BusAssignee getBusAssignee( Long assigneeId ) {
        if ( assigneeId == null )
            throw new ProjectRuntimeException( "assigneeId can't be null!" );
        return busAssigneeDao.getBusAssignee( assigneeId );
    }

    public void saveBus( Bus bus ) {
        /*if( bus.getCompany() == null )
            throw new ProjectRuntimeException( "Company can't be null" );*/
        busDao.save( bus );
    }

    public Page<Set<Bus>> getBussesByCompanyId( Long companyId, String plateNo, Pageable pageable ) {
        if( companyId == null )
            throw new ProjectRuntimeException( "companyId can't be null" );
        Page<Set<Bus>> busses = busDao.getBussesByCompanyId( companyId, plateNo, pageable );
/*        List<Bus> newBusses = new ArrayList<>();
        for ( Bus bus : busses ) {
            BusCustomSourceDestination customSourceDestination = busCustomSourceDestinationDao.getBusCustomSourceDestination(5L);
            bus.setBusCustomSourceDestination( customSourceDestination );
            newBusses.add( bus );
        }*/
        return busses;
    }

    public List<Bus> getUnassigneedBusses( Long companyId ){
        if( companyId == null )
            throw new ProjectRuntimeException( "companyId can't be null" );
        return busDao.getUnassigneedBusses( companyId );
    }

    private void assignDriver( Driver driver ) {
        if ( driver == null )
            throw new ProjectRuntimeException( "Driver can't be null" );
        driverDao.saveDriver( driver );
    }

    private void assignConductor( Conductor conductor ) {
        if ( conductor == null )
            throw new ProjectRuntimeException( "Conductor can't be null" );
        conductorDao.saveConductor( conductor );
    }

    private void removeDriver( Driver driver ) {
        if ( driver == null )
            throw new ProjectRuntimeException( "Driver can't be null" );
        //driver.setBusAssignee( null );
       assignDriver( driver );
    }

    private void removeConductor( Conductor conductor ) {
        if ( conductor == null )
            throw new ProjectRuntimeException( "Conductor can't be null" );
        //conductor.setBusAssignee( null );
        assignConductor( conductor );
    }

    public void assingDriverConductor ( Conductor conductor, Driver driver ) {
        /*if( conductor.getCompany() == null || driver.getCompany() == null )
            throw new ProjectRuntimeException( "Company can't be null" );*/
        assignConductor( conductor );
        assignDriver( driver );
    }

    public void removeDriverConductor ( Conductor conductor, Driver driver ) {
        /*if( conductor.getCompany() == null || driver.getCompany() == null )
            throw new ProjectRuntimeException( "Company can't be null" );*/
        removeConductor( conductor );
        removeDriver( driver );
    }

    public void saveBusAssignee ( BusAssignee busAssignee ) {
        if( busAssignee.getDriver() == null /*&& busAssignee.getConductor() == null */)
            throw new ProjectRuntimeException( "Bus assignee driver/conductor can't be null" );
        List<BusAssignee> busAssignees = new ArrayList<>(); //= busAssigneeDao.getBusAssigneeByBusId( busAssignee.getBus().getBusId() );
        for ( BusAssignee busAssignee1 : busAssignees ) {
            if ( busAssignee.getAssigneeId() == busAssignee1.getAssigneeId() ) {
                Driver driver = driverDao.getDriver( busAssignee.getDriver().getDriverId() );
                Conductor conductor;// = conductorDao.getConductor( busAssignee.getConductor().getConductorId() );
                busAssignee1.setTimeIn( busAssignee.getTimeIn() );
                busAssignee1.setTimeOut( busAssignee.getTimeOut() );
                busAssignee = busAssignee1;
                busAssignee.setDriver( driver );
                //busAssignee.setConductor( conductor );
            } else if ( busAssignee.getDriver().getDriverId() == busAssignee1.getDriver().getDriverId()
                /*|| busAssignee.getConductor().getConductorId() == busAssignee1.getConductor().getConductorId()*/ ) {
                /*throw new ProjectRuntimeException( String.format(
                                        "Driver/Conductor is already assigned to a bus with plate number {}",
                                        busAssignee1.getBus().getPlateNumber() ) );*/
            }
        }
        busAssigneeDao.saveAssignee( busAssignee );
    }

    public void removeBusAssignee ( Long busAssigneeId ) {
        busAssigneeDao.deleteBusAssignee( busAssigneeId );
    }

    public void saveCompanyBusStation( CompanyBusStations companyBusStation ) {
        BusStation busStation = busStationDao.getBusStation( companyBusStation.getBusStation().getStationName() );
        if( busStation == null ){
            busStationDao.saveBusStation( companyBusStation.getBusStation() );
            busStation = busStationDao.getBusStation( companyBusStation.getBusStation().getStationName() );
        }
        LOGGER.info("{}", busStation );
        companyBusStation.setBusStation( busStation );
        CompanyBusStations existing = companyBusStationsDao.isExist( companyBusStation );
        if( existing != null ) {
            companyBusStation.setCompanyStationId( existing.getCompanyStationId() );
        }
        companyBusStationsDao.saveCompanyBusStation( companyBusStation );
    }

    public void saveSourceDestination( CompanyBusSource companyBusSource ) {
        LOGGER.info( "{}", companyBusSource );
        SourceDestination sourceDestination = sourceDestinationDao.getBusSourceDestination( companyBusSource.getSourceDestination().getSource() );
        if( sourceDestination == null ) {
            sourceDestinationDao.saveSourceDestination( companyBusSource.getSourceDestination() );
            sourceDestination = sourceDestinationDao.getBusSourceDestination( companyBusSource.getSourceDestination().getSource() );
        }
        companyBusSource.setSourceDestination( sourceDestination );
        CompanyBusSource existing = companyBusSourceDao.isExist( companyBusSource );
        if( existing != null ) {
            companyBusSource.setId( existing.getId() );
        }
        companyBusSourceDao.saveCompanyBusSource( companyBusSource );
    }

    public void saveBusCustomSourceDestination( CompanyBusTrips companyBusTrips ) {
        BusTrips busTrip = busTripsDao.getBusTrip( companyBusTrips.getBusTrips() );
        if( busTrip == null ) {
            busTripsDao.saveBusTrip( companyBusTrips.getBusTrips() );
            busTrip = busTripsDao.getBusTrip( companyBusTrips.getBusTrips() );
        }
        companyBusTrips.setBusTrips( busTrip );
        CompanyBusTrips existing = companyBusTripsDao.isExist( companyBusTrips );
        if( existing != null ) {
            companyBusTrips.setCompanyBusTripId( existing.getCompanyBusTripId() );
        }
        companyBusTripsDao.saveCompanyBusTrips( companyBusTrips );
    }
}
