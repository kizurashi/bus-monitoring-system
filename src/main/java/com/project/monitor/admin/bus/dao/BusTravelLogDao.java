package com.project.monitor.admin.bus.dao;

import com.project.monitor.admin.bus.model.BusTravelLogs;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import java.util.List;

public interface BusTravelLogDao extends HibernateSessionFactory<BusTravelLogs> {

    public void saveBusTravelLog(BusTravelLogs busTravelLogs);

    public Page<List<BusTravelLogs>> getBusTravelLogs(String date, Pageable pageable);

    public BusTravelLogs getBusTravelLog(Long busId, Long assigneeId);

}