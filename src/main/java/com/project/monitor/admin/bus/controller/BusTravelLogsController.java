package com.project.monitor.admin.bus.controller;

import com.project.monitor.admin.bus.dao.BusStationDao;
import com.project.monitor.admin.bus.dao.BusTravelLogDao;
import com.project.monitor.admin.bus.model.BusStation;
import com.project.monitor.admin.bus.model.BusTravelLogs;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@RestController
@RequestMapping( ConstantConfig.API_PREFIX + "/travel/logs" )
public class BusTravelLogsController {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private BusTravelLogDao busTravelLogDao;

    @RequestMapping( value = "/{action}", method = RequestMethod.POST )
    public BusTravelLogs saveOrUpdateBusTravelLog( @RequestBody BusTravelLogs busTravelLogs,
                                          @PathVariable(name="action") String action  ) {
        TimeZone.setDefault(TimeZone.getTimeZone("Asia/Manila"));
        Date dt = new Date();
        System.out.println("Spring boot application running in UTC timezone :"+dt);   // It will print UTC timezone
        BusTravelLogs busTravelLogs1 = busTravelLogDao.getBusTravelLog(busTravelLogs.getBus().getBusId(), busTravelLogs.getBusAssignee().getAssigneeId());
        if( action == "arrival" && busTravelLogs1.getDepartureTime() == null ) {
            return busTravelLogs1;
        }
        if( busTravelLogs1.getTravelLogId() != null ) {
            busTravelLogs.setTravelLogId(busTravelLogs1.getTravelLogId());
            busTravelLogs.setArrivalTime( new Timestamp(dt.getTime()) );
            busTravelLogs.setDepartureTime( busTravelLogs1.getDepartureTime() );
        } else {
            busTravelLogs.setDepartureTime( new Timestamp(dt.getTime()) );
        }
        busTravelLogDao.saveBusTravelLog( busTravelLogs );
        return busTravelLogs;
    }

    @RequestMapping( value = "", method = RequestMethod.GET )
    public Page<List<BusTravelLogs>> busTravelLogs( @RequestParam( required = false, defaultValue="") String date,
                                                    @RequestParam( required = false, defaultValue="1") int page,
                                                    @RequestParam( required = false, defaultValue="10") int size) {
        return busTravelLogDao.getBusTravelLogs(date, Pageable.create(page, size));
    }

    @RequestMapping( value = "/{busId}/{assigneeId}", method = RequestMethod.GET )
    public BusTravelLogs busTravelLogs( @PathVariable Long busId,
                                        @PathVariable Long assigneeId ) {
        return busTravelLogDao.getBusTravelLog(busId,assigneeId);
    }
}


