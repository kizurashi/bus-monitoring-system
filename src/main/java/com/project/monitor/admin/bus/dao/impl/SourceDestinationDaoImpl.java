package com.project.monitor.admin.bus.dao.impl;

import com.project.monitor.admin.bus.dao.SourceDestinationDao;
import com.project.monitor.admin.bus.model.BusStation;
import com.project.monitor.admin.bus.model.BusTrips;
import com.project.monitor.admin.bus.model.SourceDestination;
import com.project.monitor.common.dao.GeoLocationDao;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

public class SourceDestinationDaoImpl extends HibernateSessionFactoryImpl<SourceDestination> implements SourceDestinationDao {

    private Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private GeoLocationDao geoLocationDao;

    @Override
    public void saveSourceDestination(SourceDestination busSourceDestination) {
        if( busSourceDestination.getLocation() != null ) {
            Long geoLocationId = geoLocationDao.saveGeoLocation( busSourceDestination.getLocation() );
            busSourceDestination.getLocation().setLocationId( geoLocationId );
        }
        if( busSourceDestination.getSourceId() != null )
            merge( busSourceDestination );
        else
            save( busSourceDestination );
    }

    @Override
    public void deleteBusSourceDestination(Long busSourceDestnationId) {
        delete( busSourceDestnationId );
    }

    @Override
    public Page<List<SourceDestination>> getBusSourceDestinations(Pageable pageable) {
        Query query = getSession().createQuery( String.format( "from %s",
                getActualParameterize().getSimpleName() ) )
                .setFirstResult(pageable.getOffset())
                .setMaxResults(pageable.getSize());
        return new DefaultPage<>(query.list(), pageable, getSourceDestinationsCount());
    }

    @Override
    public SourceDestination getBusSourceDestination(Long busSourceDestnationId) {
        return get( busSourceDestnationId );
    }

    @Override
    public SourceDestination getBusSourceDestination(String sourceName) {
        Criteria criteria =  getSession().createCriteria(SourceDestination.class)
                                            .add(Restrictions.eq("source", sourceName ) );
        return (SourceDestination)( criteria.list().isEmpty()? null : criteria.list().get(0) );
    }

    private Long getSourceDestinationsCount() {
        return (Long) getSession()
                .createCriteria( SourceDestination.class )
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }
}
