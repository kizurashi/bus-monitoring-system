package com.project.monitor.admin.bus.dao;

import com.project.monitor.admin.bus.model.BusStation;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import java.util.List;

public interface BusStationDao extends HibernateSessionFactory<BusStation> {

    public void saveBusStation(BusStation bus);

    public void deleteBusStation(Long busStationId);

    public Page<List<BusStation>> getBusStations(Pageable pageable);

    public BusStation getBusStation( Long busStationId );

    public BusStation getBusStation( String stationName );

}