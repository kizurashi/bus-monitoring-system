package com.project.monitor.admin.bus.model;

import com.project.monitor.common.model.GeoLocation;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table( name = "sourcedestination" )
public class SourceDestination implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long sourceId;

    @Column( name = "source", nullable = false )
    private String source;

    @OneToOne
    @JoinColumn( name = "locationId" )
    private GeoLocation location;

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public GeoLocation getLocation() {
        return location;
    }

    public void setLocation(GeoLocation location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "SourceDestination{" +
                "sourceId=" + sourceId +
                ", source='" + source + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SourceDestination that = (SourceDestination) o;
        return Objects.equals(sourceId, that.sourceId) &&
                Objects.equals(source, that.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sourceId, source);
    }
}
