package com.project.monitor.admin.bus.dao.impl;

import com.project.monitor.admin.bus.dao.BusTravelLogDao;
import com.project.monitor.admin.bus.model.BusTravelLogs;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class BusTravelLogDaoImpl extends HibernateSessionFactoryImpl<BusTravelLogs> implements BusTravelLogDao {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void saveBusTravelLog(BusTravelLogs busTravelLogs) {
        if( busTravelLogs.getTravelLogId() != null )
            merge(busTravelLogs);
        else
            save(busTravelLogs);
    }

    @Override
    public Page<List<BusTravelLogs>> getBusTravelLogs(String date, Pageable pageable) {
        Query query =  getSession().createQuery(String.format( "from %s WHERE departureTime LIKE '%s' ORDER BY arrivalTime DESC",
                getActualParameterize().getSimpleName(),
                date+"%" ) )
                .setFirstResult(pageable.getOffset())
                .setMaxResults(pageable.getSize());
        return new DefaultPage<>( query.list(), pageable , getTotalBusTravelLog(date) );
    }

    @Override
    public BusTravelLogs getBusTravelLog(Long busId, Long assigneeId) {
        LOGGER.info("{}, {}", busId, assigneeId);
        Query query =  getSession().createQuery(String.format( "from %s WHERE assigneeId = %d and busId = %d and arrivalTime is null",
                getActualParameterize().getSimpleName(),
                assigneeId,
                busId ) );
        return (BusTravelLogs)(!query.list().isEmpty()? query.list().get(0): new BusTravelLogs());
    }

    private Long getTotalBusTravelLog(String date){
        return  (Long)getSession()
                .createQuery(String.format( "select count(departureTime) from %s WHERE departureTime LIKE '%s' ORDER BY arrivalTime DESC",
                        getActualParameterize().getSimpleName(),
                        date+"%" ) )
                .uniqueResult();
    }

}
