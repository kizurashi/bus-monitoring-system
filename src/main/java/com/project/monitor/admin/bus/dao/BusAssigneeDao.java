package com.project.monitor.admin.bus.dao;

import com.project.monitor.admin.bus.model.BusAssignee;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import javax.transaction.Transactional;
import java.util.List;

public interface BusAssigneeDao extends HibernateSessionFactory<BusAssignee>{

    public void saveAssignee(BusAssignee busAssignee);

    //public void updateAssignee(BusAssignee busAssignee);

    public Page<List<BusAssignee>> getBusAssignees(Pageable pageable);

    public BusAssignee getBusAssignee(Long busAssigneeId);

    public void deleteBusAssignee(Long busAssigneeId);

    public List<BusAssignee> getBusAssigneeByBusId(Long busId);

    public Page<List<BusAssignee>> getBusAssignees(Long companyId, Pageable pageable);

    public BusAssignee validteBusSchedule(BusAssignee busAssignee);

}
