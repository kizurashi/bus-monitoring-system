package com.project.monitor.admin.bus.dao.impl;

import com.project.monitor.admin.bus.model.Bus;
import com.project.monitor.admin.bus.model.BusAssignedStations;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class BusAssignedStationsDaoImpl extends HibernateSessionFactoryImpl<BusAssignedStations> implements com.project.monitor.admin.bus.dao.BusAssignedStationsDao {

    @Override
    public void saveBusAssignedStation(BusAssignedStations busAssignedStations) {
        save( busAssignedStations );
    }

    @Override
    public void deleteBusAssignedStation(Long id) {
        delete( id );
    }

    @Override
    public Page<List<BusAssignedStations>> getBusAssignedStations(Long id, Pageable pageable) {
        Query query = getSession().createQuery( String.format( "from %s where id = %d",
                                                            getActualParameterize().getSimpleName(), id ) )
                                    .setFirstResult(pageable.getOffset())
                                    .setMaxResults(pageable.getPage());
        return new DefaultPage<>(query.list(), pageable, getBusAssignedStationsCount(id));
    }

    private Long getBusAssignedStationsCount( Long id ) {
        return (Long) getSession()
                .createCriteria( BusAssignedStations.class )
                .add(Restrictions.eq( "id", id ) )
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }
}
