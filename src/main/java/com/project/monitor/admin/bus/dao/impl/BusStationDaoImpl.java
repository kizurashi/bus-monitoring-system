package com.project.monitor.admin.bus.dao.impl;

import com.project.monitor.admin.bus.dao.BusStationDao;
import com.project.monitor.admin.bus.model.BusStation;
import com.project.monitor.common.dao.GeoLocationDao;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BusStationDaoImpl extends HibernateSessionFactoryImpl<BusStation> implements BusStationDao {

    private Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private GeoLocationDao geoLocationDao;

    @Override
    public void saveBusStation(BusStation busStation ) {
        if( busStation.getLocation() != null ) {
            Long geoLocationId = geoLocationDao.saveGeoLocation( busStation.getLocation() );
            busStation.getLocation().setLocationId( geoLocationId );
        }
        if( busStation.getStationId() != null ) {
            merge( busStation );
        } else {
            save( busStation );
        }
    }

    @Override
    public void deleteBusStation(Long busStationId) {
        delete( busStationId );
    }

    @Override
    public Page<List<BusStation>> getBusStations(Pageable pageable) {
        Query query = getSession().createQuery( String.format( "from %s",
                                                        getActualParameterize().getSimpleName() ) )
                                    .setFirstResult(pageable.getOffset())
                                    .setMaxResults(pageable.getSize());
        return new DefaultPage<>(query.list(), pageable, getBusStationCount());
    }

    @Override
    public BusStation getBusStation(Long busStationId) {
        return get( busStationId );
    }

    @Override
    public BusStation getBusStation(String stationName) {
        Criteria criteria =  getSession().createCriteria(BusStation.class)
                                    .add(Restrictions.eq("stationName", stationName ) );
        return (BusStation)( criteria.list().isEmpty()? null : criteria.list().get(0) );
    }

    private Long getBusStationCount() {
       return (Long) getSession().createCriteria(BusStation.class)
                .setProjection(Projections.rowCount())
                .uniqueResult();
    }
}
