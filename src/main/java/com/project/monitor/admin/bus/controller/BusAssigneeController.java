package com.project.monitor.admin.bus.controller;

import com.project.monitor.admin.bus.dao.BusAssigneeDao;
import com.project.monitor.admin.bus.model.BusAssignee;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ConstantConfig.API_PREFIX + ConstantConfig.BUS + "-assignee" )
public class BusAssigneeController {

    @Autowired
    private BusAssigneeDao busAssigneeDao;

    @RequestMapping( value = "/{companyId}", method = RequestMethod.GET )
    public Page<List<BusAssignee>> getBusAssignees(@PathVariable("companyId") Long companyId,
                                                   @RequestParam( required = false, defaultValue="1") int page,
                                                   @RequestParam( required = false, defaultValue="10") int size ) {
        return busAssigneeDao.getBusAssignees(Pageable.create(page, size));
    }

    @RequestMapping( value = "/{busAssigneeId}", method = RequestMethod.DELETE )
    public void deleteBusAssignee( @PathVariable("busAssigneeId") Long busAssigneeId ) {
        busAssigneeDao.deleteBusAssignee( busAssigneeId );
    }

}
