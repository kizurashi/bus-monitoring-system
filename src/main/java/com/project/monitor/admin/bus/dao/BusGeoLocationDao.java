package com.project.monitor.admin.bus.dao;


import com.project.monitor.common.model.GeoLocation;

public interface BusGeoLocationDao {

    public void saveBusGeoLocation( GeoLocation busGeoLocation );

    public GeoLocation getBusGeoLocation(Long busId );

    public void deleteBusAssignee( Long busId );

}
