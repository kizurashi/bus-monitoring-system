package com.project.monitor.admin.bus.dao;

import com.project.monitor.admin.bus.model.BusTrips;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import java.util.List;

public interface BusTripsDao extends HibernateSessionFactory<BusTrips> {

    public void saveBusTrip( BusTrips busTrip );

    public void deleteBusTrip( Long busTripId );

    public Page<List<BusTrips>> getBusTrips(Pageable pageable);

    public BusTrips getBusTrip( Long busTripId );

    public BusTrips getBusTrip( BusTrips busTrips );

}
