package com.project.monitor.admin.bus.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project.monitor.activity.model.Activity;
import com.project.monitor.admin.company.model.CompanyBusTrips;
import com.project.monitor.common.model.GeoLocation;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table( name = "bustravellogs" )
public final class BusTravelLogs implements Serializable{

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long travelLogId;

    @Column( name = "isSourceToDestination", nullable = false, unique = true, length = 20)
    private int isSourceToDestination;

    @ManyToOne
    @JoinColumn( name = "busId" )
    private Bus bus;

    @ManyToOne
    @JoinColumn( name = "assigneeId" )
    private BusAssignee busAssignee;

    @JsonIgnoreProperties( { "bus" } )
    @OneToOne
    @JoinColumn( name = "companyBusTripId" )
    private CompanyBusTrips busTrips;

    private Timestamp arrivalTime;

    private Timestamp departureTime;

    public Long getTravelLogId() {
        return travelLogId;
    }

    public void setTravelLogId(Long travelLogId) {
        this.travelLogId = travelLogId;
    }

    public int getIsSourceToDestination() {
        return isSourceToDestination;
    }

    public void setIsSourceToDestination(int isSourceToDestination) {
        this.isSourceToDestination = isSourceToDestination;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    public CompanyBusTrips getBusTrips() {
        return busTrips;
    }

    public void setBusTrips(CompanyBusTrips busTrips) {
        this.busTrips = busTrips;
    }

    public Timestamp getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Timestamp arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Timestamp getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Timestamp departureTime) {
        this.departureTime = departureTime;
    }

    public BusAssignee getBusAssignee() {
        return busAssignee;
    }

    public void setBusAssignee(BusAssignee busAssignee) {
        this.busAssignee = busAssignee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BusTravelLogs that = (BusTravelLogs) o;
        return Objects.equals(travelLogId, that.travelLogId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(travelLogId);
    }

    @Override
    public String toString() {
        return "BusTravelLogs{" +
                "travelLogId=" + travelLogId +
                ", isSourceToDestination=" + isSourceToDestination +
                ", bus=" + bus +
                ", busTrips=" + busTrips +
                ", arrivalTime=" + arrivalTime +
                ", departureTime=" + departureTime +
                '}';
    }
}
