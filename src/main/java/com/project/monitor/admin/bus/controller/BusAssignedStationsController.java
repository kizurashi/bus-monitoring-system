package com.project.monitor.admin.bus.controller;

import com.project.monitor.admin.bus.dao.BusAssignedStationsDao;
import com.project.monitor.admin.bus.model.BusAssignedStations;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ConstantConfig.API_PREFIX + ConstantConfig.BUS + "-assignee-stations" )
public class BusAssignedStationsController {

    @Autowired
    private BusAssignedStationsDao busAssignedStationsDao;

    @RequestMapping( value = "/{id}", method = RequestMethod.GET )
    public Page<List<BusAssignedStations>> getBusAssigneedStations(@PathVariable("id") Long id,
                                                                   @RequestParam( required = false, defaultValue = "0") int page,
                                                                   @RequestParam( required = false, defaultValue="10") int size) {
        return busAssignedStationsDao.getBusAssignedStations( id, Pageable.create(page, size));
    }
}
