package com.project.monitor.admin.bus.dao;

import com.project.monitor.admin.bus.model.SourceDestination;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import java.util.List;

public interface SourceDestinationDao extends HibernateSessionFactory<SourceDestination> {

    public void saveSourceDestination(SourceDestination busSourceDestination);

    public void deleteBusSourceDestination(Long busSourceDestnationId);

    public Page<List<SourceDestination>> getBusSourceDestinations(Pageable pageable);

    public SourceDestination getBusSourceDestination(Long busSourceDestnationId );

    public SourceDestination getBusSourceDestination( String sourceName );

}