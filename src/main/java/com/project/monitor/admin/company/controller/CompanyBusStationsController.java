package com.project.monitor.admin.company.controller;

import com.project.monitor.admin.company.dao.CompanyBusStationsDao;
import com.project.monitor.admin.company.model.CompanyBusStations;
import com.project.monitor.admin.company.model.CompanyBusTrips;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ConstantConfig.API_PREFIX + ConstantConfig.COMPANY  )
public class CompanyBusStationsController {

    @Autowired
    private CompanyBusStationsDao companyBusStationsDao;

    @RequestMapping( value = "/{companyId}/bus-stations", method = RequestMethod.GET)
    public Page<List<CompanyBusStations>> getCompanyBusStations(@RequestParam( required = false, defaultValue="1") int page,
                                                                @RequestParam( required = false, defaultValue="10") int size,
                                                                @PathVariable( "companyId" ) Long companyId ) {
        return companyBusStationsDao.getCompanyBusStations( companyId, Pageable.create(page,size));
    }


    @RequestMapping( value = "/bus-station/{companyBusStationId}", method = RequestMethod.DELETE)
    public void deleteCompanyBusStation( @PathVariable( "companyBusStationId" ) Long companyBusStationId ) {
        companyBusStationsDao.deleteCompanyBusStation( companyBusStationId );
    }

}
