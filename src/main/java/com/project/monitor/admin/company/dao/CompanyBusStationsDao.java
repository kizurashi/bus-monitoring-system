package com.project.monitor.admin.company.dao;

import com.project.monitor.admin.company.model.CompanyBusStations;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import java.util.List;

public interface CompanyBusStationsDao extends HibernateSessionFactory<CompanyBusStations> {

    public void saveCompanyBusStation( CompanyBusStations companyBusStations );

    public void deleteCompanyBusStation( Long companyBusStationId );

    public Page<List<CompanyBusStations>> getCompanyBusStations(Long companyId, Pageable pageable);

    public CompanyBusStations getCompanyBusStation(Long companyBusStationId );

    public CompanyBusStations isExist( CompanyBusStations companyBusStations );

}
