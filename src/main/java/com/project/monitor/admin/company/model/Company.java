package com.project.monitor.admin.company.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project.monitor.permission.model.User;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.*;

@Transactional
@Entity
@Table( name = "company" )
public final class Company implements Serializable{

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long companyId;

    @Column( name = "companyName", unique = true, nullable = false, length = 60 )
    private String companyName;

//    private Address companyAddress;

    @Column( name = "contactNo", unique = true, nullable = false, length = 60 )
    private String contactNo;

    @Column( name = "emailAddress", unique = true, nullable = false, length = 60 )
    private String emailAddress;

    @OneToOne
    @JoinColumn( name = "userId" )
    private User user;

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public User getUser() {
        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Company company = (Company) o;
        return Objects.equals(companyId, company.companyId) &&
                Objects.equals(companyName, company.companyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyId);
    }

    @Override
    public String toString() {
        return " Company[ companyId = " + companyId
                + ", companyName = " + companyName
                + " ] ";
    }

}
