package com.project.monitor.admin.company.controller;

import com.project.monitor.admin.company.dao.CompanyBusTripsDao;
import com.project.monitor.admin.company.model.CompanyBusTrips;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ConstantConfig.API_PREFIX + ConstantConfig.COMPANY  )
public class CompanyBusTripsController {

    private Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private CompanyBusTripsDao companyBusTripsDao;

    @RequestMapping( value = "/{companyId}/bus-trips", method = RequestMethod.GET)
    public Page<List<CompanyBusTrips>> getCompanyBusTrips(@RequestParam( required = false, defaultValue="1") int page,
                                                          @RequestParam( required = false, defaultValue="10") int size,
                                                          @PathVariable( "companyId" ) Long companyId ) {
        return companyBusTripsDao.getCompanyBusTrips( companyId, Pageable.create(page,size));
    }

    @RequestMapping( value = "/bus-trip/{companyBusTripId}", method = RequestMethod.GET)
    public CompanyBusTrips getCompanyBusStation( @PathVariable( "companyBusTripId" ) Long companyBusTripId ) {
        return companyBusTripsDao.getCompanyBusTrip( companyBusTripId );
    }

    @RequestMapping( value = "/bus-trip/{companyBusTripId}", method = RequestMethod.DELETE)
    public void deleteCompanyBusStation( @PathVariable( "companyBusTripId" ) Long companyBusTripId ) {
        companyBusTripsDao.deleteCompanyBusTrips( companyBusTripId );
    }

}
