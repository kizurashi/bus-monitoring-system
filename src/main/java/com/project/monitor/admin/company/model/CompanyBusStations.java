package com.project.monitor.admin.company.model;

import com.project.monitor.admin.bus.model.BusStation;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table( name = "companybusstations" )
public class CompanyBusStations {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long companyStationId;

    @ManyToOne
    @JoinColumn( name = "stationId" )
    private BusStation busStation;

    @ManyToOne
    @JoinColumn( name = "companyId" )
    private Company company;

    public Long getCompanyStationId() {
        return companyStationId;
    }

    public void setCompanyStationId(Long companyStationId) {
        this.companyStationId = companyStationId;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public BusStation getBusStation() {
        return busStation;
    }

    public void setBusStation(BusStation busStation) {
        this.busStation = busStation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyBusStations that = (CompanyBusStations) o;
        return Objects.equals(companyStationId, that.companyStationId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(companyStationId);
    }

    @Override
    public String toString() {
        return "CompanyBusStations{" +
                "companyStationId=" + companyStationId +
                ", busStation=" + busStation +
                ", company=" + company +
                '}';
    }
}
