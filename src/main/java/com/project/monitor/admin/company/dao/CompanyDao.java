package com.project.monitor.admin.company.dao;

import com.project.monitor.admin.company.model.Company;
import com.project.monitor.exception.ProjectRuntimeException;
import com.project.monitor.hibernate.HibernateSessionFactory;

import java.util.List;

public interface CompanyDao extends HibernateSessionFactory<Company> {

    public void updateCompany(Company company) throws Exception;

    public void deleteCompany(Long companyId) throws ProjectRuntimeException;

    public List<Company> getCompanies();

    public Company getCompany(Long companyId) throws ProjectRuntimeException;

    public void saveCompany(Company company);

    public Company getCompanyByUserId( Long userId );

}
