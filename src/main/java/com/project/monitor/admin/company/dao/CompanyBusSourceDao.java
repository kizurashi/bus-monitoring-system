package com.project.monitor.admin.company.dao;

import com.project.monitor.admin.company.model.CompanyBusSource;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import java.util.List;

public interface CompanyBusSourceDao extends HibernateSessionFactory<CompanyBusSource> {

    public void saveCompanyBusSource( CompanyBusSource companyBusSource );

    public void deletecompanyBusSource( Long id );

    public Page<List<CompanyBusSource>> getCompanyBusSources(Long companyId, Pageable pageable);

    public CompanyBusSource getCompanyBusSource( Long id );

    public CompanyBusSource isExist( CompanyBusSource companyBusSource );

}

