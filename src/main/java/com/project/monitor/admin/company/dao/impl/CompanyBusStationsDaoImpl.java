package com.project.monitor.admin.company.dao.impl;

import com.project.monitor.admin.company.dao.CompanyBusStationsDao;
import com.project.monitor.admin.company.model.CompanyBusSource;
import com.project.monitor.admin.company.model.CompanyBusStations;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class CompanyBusStationsDaoImpl extends HibernateSessionFactoryImpl<CompanyBusStations> implements CompanyBusStationsDao {

    @Override
    public void saveCompanyBusStation(CompanyBusStations companyBusStations) {
        if( companyBusStations.getCompanyStationId() != null ){
            merge(companyBusStations );
        } else {
            save( companyBusStations );
        }
    }

    @Override
    public void deleteCompanyBusStation(Long companyBusStationId) {
        delete( companyBusStationId );
    }

    @Override
    public Page<List<CompanyBusStations>> getCompanyBusStations(Long companyId, Pageable pageable) {
        Query query = getSession().createQuery( String.format("from %s where companyId = %d",
                                                    getActualParameterize().getSimpleName(),
                                                    companyId ) )
                                    .setFirstResult( pageable.getOffset() )
                                    .setMaxResults( pageable.getPage() );
        return new DefaultPage<>( query.list(), pageable, getCompanyBusStationsCount( companyId ) );
    }

    @Override
    public CompanyBusStations getCompanyBusStation(Long companyBusStationId) {
        return get( companyBusStationId );
    }

    @Override
    public CompanyBusStations isExist(CompanyBusStations companyBusStations) {
        Criteria criteria = getSession().createCriteria( CompanyBusStations.class, "companyBusStations" )
                            .add(Restrictions.and(
                                    Restrictions.eq("companyBusStations.company.companyId", companyBusStations.getCompany().getCompanyId() ),
                                    Restrictions.eq("companyBusStations.busStation.stationId", companyBusStations.getBusStation().getStationId() )
                            ));
        return (CompanyBusStations) (criteria.list().isEmpty()? null: criteria.list().get(0));
    }


    private Long getCompanyBusStationsCount( Long companyId ) {
        return (Long) getSession()
                .createCriteria( CompanyBusStations.class )
                .add(Restrictions.eq( "company.companyId", companyId ) )
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }
}
