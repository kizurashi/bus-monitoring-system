package com.project.monitor.admin.company.dao;

import com.project.monitor.admin.company.model.CompanyBusTrips;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import java.util.List;

public interface CompanyBusTripsDao extends HibernateSessionFactory<CompanyBusTrips> {

    public void saveCompanyBusTrips( CompanyBusTrips companyBusTrips );

    public void deleteCompanyBusTrips( Long companyBusTripId );

    public Page<List<CompanyBusTrips>> getCompanyBusTrips(Long companyId, Pageable pageable);

    public CompanyBusTrips getCompanyBusTrip( Long companyBusTripId );

    public CompanyBusTrips isExist( CompanyBusTrips companyBusTrips );

}
