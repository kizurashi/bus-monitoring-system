package com.project.monitor.admin.company.controller;

import com.project.monitor.admin.company.dao.CompanyDao;
import com.project.monitor.admin.company.model.Company;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.exception.company.CompanyNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( value = ConstantConfig.API_PREFIX + ConstantConfig.COMPANY )
public class CompanyController {

    @Autowired
    private CompanyDao companyDao;

    @RequestMapping( value = "", method = RequestMethod.GET )
    public List<Company> getCompanies() {
        return companyDao.getCompanies();
    }

    @RequestMapping( value = "/{companyId}", method = RequestMethod.GET )
    public ResponseEntity<Company> getCompany(@PathVariable( value = "companyId" ) Long companyId ) throws CompanyNotFoundException {
        return ResponseEntity.ok( companyDao.getCompany( companyId ) );
    }

    @RequestMapping( value = "/{companyId}", method = RequestMethod.DELETE )
    public void deleteCompany( @PathVariable( "companyId" ) Long companyId ) throws CompanyNotFoundException{
            companyDao.deleteCompany( companyId );
    }

    @RequestMapping( value = "", method = RequestMethod.PUT )
    public void updateCompany(@RequestBody Company company ) throws Exception {
        companyDao.updateCompany( company );
    }

    @RequestMapping( value = "", method = RequestMethod.POST )
    public void saveCompany(@RequestBody Company company ) {
        companyDao.saveCompany( company );
    }

}
