package com.project.monitor.admin.company.dao.impl;

import com.project.monitor.admin.bus.model.BusTrips;
import com.project.monitor.admin.company.dao.CompanyBusTripsDao;
import com.project.monitor.admin.company.model.CompanyBusStations;
import com.project.monitor.admin.company.model.CompanyBusTrips;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class CompanyBusTripsDaoImpl extends HibernateSessionFactoryImpl<CompanyBusTrips> implements CompanyBusTripsDao {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void saveCompanyBusTrips(CompanyBusTrips companyBusTrips) {
        LOGGER.info( "{}", companyBusTrips );
        if( companyBusTrips.getCompanyBusTripId() != null ) {
            merge( companyBusTrips );
        } else {
            save( companyBusTrips );
        }
    }

    @Override
    public void deleteCompanyBusTrips(Long companyBusTripId) {
        delete( companyBusTripId );
    }

    @Override
    public Page<List<CompanyBusTrips>> getCompanyBusTrips(Long companyId, Pageable pageable) {
        Query query = getSession().createQuery( String.format("from %s where companyId = %d",
                                                    getActualParameterize().getSimpleName(),
                                                    companyId ) )
                                    .setFirstResult( pageable.getOffset() )
                                    .setMaxResults( pageable.getPage() );
        return new DefaultPage<>(query.list(), pageable, getCompanyBusTripsCount( companyId ) );
    }

    @Override
    public CompanyBusTrips getCompanyBusTrip(Long companyBusTripId) {
        return get( companyBusTripId );
    }

    @Override
    public CompanyBusTrips isExist(CompanyBusTrips companyBusTrips) {
        Criteria criteria =  getSession().createCriteria(CompanyBusTrips.class, "companyBusTrips")
                .createAlias( "companyBusTrips.busTrips.source", "origin")
                .createAlias( "companyBusTrips.busTrips.destination", "destination")
                .add( Restrictions.and(
                            Restrictions.eq( "origin.source", companyBusTrips.getBusTrips().getSource().getSource() ),
                            Restrictions.eq( "destination.source", companyBusTrips.getBusTrips().getDestination().getSource() )
                      )
                );
        return (CompanyBusTrips)( criteria.list().isEmpty()? null : criteria.list().get(0) );
    }

    private Long getCompanyBusTripsCount( Long companyId ) {
        return (Long) getSession()
                .createCriteria( CompanyBusStations.class )
                .add(Restrictions.eq( "company.companyId", companyId ) )
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }

}
