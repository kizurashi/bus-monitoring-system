package com.project.monitor.admin.company.dao.impl;

import com.project.monitor.admin.company.dao.CompanyDao;
import com.project.monitor.admin.company.model.Company;
import com.project.monitor.exception.ProjectRuntimeException;
import com.project.monitor.exception.company.CompanyNotFoundException;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.permission.dao.UserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class CompanyDaoImpl extends HibernateSessionFactoryImpl<Company> implements CompanyDao {

    Logger logger = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private UserDao userDao;

    @Override
    public void saveCompany( Company company ) {
        if( company.getUser() != null ) {
            if( company.getUser().getId() == null ) {
                Long userId = userDao.saveUser( company.getUser() );
                company.getUser().setId( userId );
            }
        }
        save( company );
    }

    @Override
    public void updateCompany( Company company ) throws CompanyNotFoundException{ }

    @Override
    public void deleteCompany( Long companyId ) throws ProjectRuntimeException {
        try {
            delete( companyId );
        } catch ( IllegalArgumentException ex ) {
            throw new CompanyNotFoundException( String.format(
                    "Company with %d not found!", companyId ) );
        }
    }

    @Override
    public List<Company> getCompanies() {
        return getAll();
    }

    @Override
    public Company getCompany( Long companyId ) throws ProjectRuntimeException {
        Company company =  get( companyId );
        if( company == null )
            throw new CompanyNotFoundException( String.format(
                    "Company with %d not found!", companyId ) );
        return company;
    }

    @Override
    public Company getCompanyByUserId( Long userId ) {
        List<Company> companies = getCompanies();
        return companies.stream()
                .filter( c -> c.getUser().getId().equals( userId ) )
                .collect( Collectors.toList() )
                .get(0);
    }

}
