package com.project.monitor.admin.company.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project.monitor.admin.bus.model.Bus;
import com.project.monitor.admin.bus.model.BusAssignedStations;
import com.project.monitor.admin.bus.model.BusStation;
import com.project.monitor.admin.bus.model.BusTrips;

import javax.persistence.*;
import java.util.*;

@Entity
@Table( name = "companybustrips" )
public class CompanyBusTrips {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long companyBusTripId;

    @ManyToOne
    @JoinColumn( name =  "bustripId" )
    private BusTrips busTrips;

    @ManyToOne
    @JoinColumn( name =  "companyId" )
    private Company company;

    @OneToMany( fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.PERSIST } )
    @JoinColumn(name = "companyBusTripId")
//    @JoinTable(name = "busassigneestations",
//            joinColumns = { @JoinColumn(name = "companyBusTripId") },
//            inverseJoinColumns = { @JoinColumn(name = "stationId") })
    private List<BusAssignedStations> companyBusStations = new ArrayList<>();

    public Long getCompanyBusTripId() {
        return companyBusTripId;
    }

    public void setCompanyBusTripId(Long companyBusTripId) {
        this.companyBusTripId = companyBusTripId;
    }

    public BusTrips getBusTrips() {
        return busTrips;
    }

    public void setBusTrips(BusTrips busTrips) {
        this.busTrips = busTrips;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<BusAssignedStations> getCompanyBusStations() {
        return companyBusStations;
    }

    public void setCompanyBusStations(List<BusAssignedStations> companyBusStations) {
        this.companyBusStations = companyBusStations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyBusTrips that = (CompanyBusTrips) o;
        return Objects.equals(companyBusTripId, that.companyBusTripId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyBusTripId);
    }

    @Override
    public String toString() {
        return "CompanyBusTrips{" +
                "companyBusTripId=" + companyBusTripId +
                ", busTrips=" + busTrips +
                ", company=" + company +
                ", busAssignedStations=" + companyBusStations +
                '}';
    }
}
