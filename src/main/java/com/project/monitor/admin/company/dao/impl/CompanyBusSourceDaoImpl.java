package com.project.monitor.admin.company.dao.impl;

import com.project.monitor.admin.bus.model.Bus;
import com.project.monitor.admin.bus.model.SourceDestination;
import com.project.monitor.admin.company.dao.CompanyBusSourceDao;
import com.project.monitor.admin.company.model.CompanyBusSource;
import com.project.monitor.admin.company.model.CompanyBusStations;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class CompanyBusSourceDaoImpl extends HibernateSessionFactoryImpl<CompanyBusSource> implements CompanyBusSourceDao {

    @Override
    public void saveCompanyBusSource(CompanyBusSource companyBusSource) {
        if( companyBusSource.getId() != null ){
            merge( companyBusSource );
        } else {
            save( companyBusSource );
        }
    }

    @Override
    public void deletecompanyBusSource(Long id) {
        delete( id );
    }

    @Override
    public Page<List<CompanyBusSource>> getCompanyBusSources(Long companyId, Pageable pageable) {
        Query query = getSession().createQuery( String.format("from %s where companyId = %d",
                                                    getActualParameterize().getSimpleName(),
                                                    companyId ) )
                                    .setFirstResult( pageable.getOffset() )
                                    .setMaxResults( pageable.getPage() );
        return new DefaultPage<>(query.list(), pageable, getCompanyBusSourceCount( companyId ) );
    }

    @Override
    public CompanyBusSource getCompanyBusSource(Long id) {
        Query query = getSession().createQuery( String.format("from %s where id = %d",getActualParameterize().getSimpleName(), id ) );
        return (CompanyBusSource) (query.list().isEmpty()? null: query.list().get(0));

    }

    @Override
    public CompanyBusSource isExist( CompanyBusSource companyBusSource ) {
        Criteria criteria = getSession().createCriteria( CompanyBusSource.class, "companyBusSource" )
                .add(Restrictions.and(
                        Restrictions.eq("companyBusSource.company.companyId", companyBusSource.getCompany().getCompanyId() ),
                        Restrictions.eq("companyBusSource.sourceDestination.sourceId", companyBusSource.getSourceDestination().getSourceId() )
                ));
        return (CompanyBusSource) (criteria.list().isEmpty()? null: criteria.list().get(0));
    }

    private Long getCompanyBusSourceCount( Long companyId ) {
        return (Long) getSession()
                .createCriteria( CompanyBusSource.class )
                .add(Restrictions.eq( "company.companyId", companyId ) )
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }

}
