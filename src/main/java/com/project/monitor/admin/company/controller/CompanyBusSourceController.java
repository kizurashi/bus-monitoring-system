package com.project.monitor.admin.company.controller;

import com.project.monitor.admin.company.dao.CompanyBusSourceDao;
import com.project.monitor.admin.company.dao.CompanyBusStationsDao;
import com.project.monitor.admin.company.model.CompanyBusSource;
import com.project.monitor.admin.company.model.CompanyBusStations;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ConstantConfig.API_PREFIX + ConstantConfig.COMPANY  )
public class CompanyBusSourceController {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private CompanyBusSourceDao companyBusSourceDao;

    @RequestMapping( value = "/{companyId}/bus-source", method = RequestMethod.GET)
    public Page<List<CompanyBusSource>> getCompanyBusSources(@RequestParam( required = false, defaultValue="1") int page,
                                                             @RequestParam( required = false, defaultValue="10") int size,
                                                             @PathVariable( "companyId" ) Long companyId ) {
        LOGGER.info( "{}", companyId );
        return companyBusSourceDao.getCompanyBusSources( companyId, Pageable.create(page,size));
    }

    @RequestMapping( value = "/bus-source/{id}", method = RequestMethod.DELETE)
    public void deleteCompanyBusSource( @PathVariable( "id" ) Long id ) {
        companyBusSourceDao.deletecompanyBusSource( id );
    }

}
