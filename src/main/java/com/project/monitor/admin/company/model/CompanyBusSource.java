package com.project.monitor.admin.company.model;

import com.project.monitor.admin.bus.model.SourceDestination;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table( name = "companybussource" )
public class CompanyBusSource {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @ManyToOne( optional = false     )
    @JoinColumn( name = "companyId" )
    private Company company;

    @ManyToOne( optional = false )
    @JoinColumn( name = "sourceId" )
    private SourceDestination sourceDestination;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public SourceDestination getSourceDestination() {
        return sourceDestination;
    }

    public void setSourceDestination(SourceDestination sourceDestination) {
        this.sourceDestination = sourceDestination;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyBusSource that = (CompanyBusSource) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "CompanyBusSource{" +
                "id=" + id +
                ", company=" + company +
                ", sourceDestination=" + sourceDestination +
                '}';
    }
}
