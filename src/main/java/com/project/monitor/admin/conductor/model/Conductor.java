package com.project.monitor.admin.conductor.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project.monitor.admin.bus.model.BusAssignee;
import com.project.monitor.permission.model.User;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.Objects;

@Transactional
@Entity
@Table( name = "conductor" )
public final class Conductor implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private Long conductorId;

    @Column( name = "firstName", nullable = false, length = 60)
    private String firstName;

    @Column( name = "lastName", nullable = false, length = 60)
    private String lastName;

    @Column( name = "age", nullable = false, length = 3)
    private int age;

    @Column( name = "contactNo", nullable = false, length = 11)
    private String contactNo;

    @JsonIgnoreProperties( { "driver", "conductor" } )
    @OneToOne( fetch = FetchType.LAZY, mappedBy = "conductor", cascade = {CascadeType.MERGE, CascadeType.PERSIST} )
    private BusAssignee busAssignee;

    private Long companyId;

    @ManyToOne( cascade = {CascadeType.REMOVE})
    @JoinColumn( name = "userId" )
    private User user;

    public void setConductorId(Long conductorId) {
        this.conductorId = conductorId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getConductorId(){
        return conductorId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

/*    public Address getAddress() {
        return address;
    }*/

    public String getContactNo() {
        return contactNo;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public BusAssignee getBusAssignee() {
        return busAssignee;
    }

    public void setBusAssignee(BusAssignee busAssignee) {
        this.busAssignee = busAssignee;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Conductor{" +
                "id='" + conductorId + '\'' +
                ",firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", contactNo='" + contactNo + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Conductor driver = (Conductor) o;
        return Objects.equals(firstName, driver.firstName) &&
                Objects.equals(lastName, driver.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(conductorId);
    }

}
