package com.project.monitor.admin.conductor.dao;

import com.project.monitor.admin.conductor.model.Conductor;
import com.project.monitor.admin.driver.model.Driver;
import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import java.util.List;
import java.util.Set;

public interface ConductorDao extends HibernateSessionFactory<Conductor> {

    public void saveConductor(Conductor conductor);

    public void updateConductor(Conductor conductor) throws Exception;

    public void deleteConductor(Long conductorId);

    public Page<Set<Conductor>> getConductors(Pageable pageable);

    public Conductor getConductor(Long conductorId);

    public Page<Set<Conductor>> getConductorsByCompanyId(Long companyId, String conductorName, Pageable pageable);

    public List<Conductor> getUnAssigneedConductors(Long companyId );

    public Long getConductorCountByCompany(Long companyId);

    public Conductor getConductorByUsername( String username );

}
