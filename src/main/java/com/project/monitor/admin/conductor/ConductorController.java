package com.project.monitor.admin.conductor;

import com.project.monitor.admin.conductor.dao.ConductorDao;
import com.project.monitor.admin.conductor.model.Conductor;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping( value = ConstantConfig.API_PREFIX + ConstantConfig.CONDUCTOR )
public class ConductorController {

    @Autowired
    ConductorDao conductorDao;

    @RequestMapping( value = "", method = RequestMethod.GET )
    public Page<Set<Conductor>> getConductors( @RequestParam( required = false, defaultValue="") String conductorName,
                                               @RequestParam( required = false, defaultValue="") Long companyId,
                                               @RequestParam( required = false, defaultValue="1") int page,
                                               @RequestParam( required = false, defaultValue="10") int size) {
        return conductorDao.getConductorsByCompanyId(companyId, conductorName, Pageable.create(page,size));
    }

    @RequestMapping( value = "/{conductorId}", method = RequestMethod.GET )
    public Conductor getConductor( @PathVariable( value = "conductorId" ) Long conductorId ) {
        return conductorDao.getConductor( conductorId );
    }

    @RequestMapping( value = "/{conductorId}", method = RequestMethod.DELETE )
    public void deleteConductor( @PathVariable( "conductorId" ) Long companyId ) {
        conductorDao.deleteConductor( companyId );
    }

    @RequestMapping( value = "", method = RequestMethod.PUT )
    public void updateConductor( @RequestBody Conductor conductor ) throws Exception {
        conductorDao.updateConductor( conductor );
    }

    @RequestMapping( value = "", method = RequestMethod.POST )
    public void saveConductor( @RequestBody Conductor conductor ) {
        conductorDao.saveConductor( conductor );
    }
}
