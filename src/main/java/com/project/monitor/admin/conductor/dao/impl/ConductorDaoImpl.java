package com.project.monitor.admin.conductor.dao.impl;

import com.project.monitor.admin.conductor.model.Conductor;
import com.project.monitor.admin.conductor.dao.ConductorDao;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ConductorDaoImpl extends HibernateSessionFactoryImpl<Conductor> implements ConductorDao {

    @Override
    public void saveConductor( Conductor conductor ) {
        save( conductor );
    }

    @Override
    public void updateConductor( Conductor conductor ) throws Exception {
        save( conductor );
    }

    @Override
    public void deleteConductor( Long conductorId ) {
        delete( conductorId );
    }

    @Override
    public Page<Set<Conductor>> getConductors(Pageable pageable ) {
        Query query = getSession().createQuery( String.format("from %s",
                getActualParameterize().getSimpleName() ) )
                .setFirstResult(pageable.getOffset())
                .setMaxResults( pageable.getSize() );
        return new DefaultPage<>(new HashSet(query.list()), pageable, getConductorsCount());
    }

    @Override
    public Conductor getConductor( Long conductorId ) {
        return get( conductorId );
    }

    @Override
    public Page<Set<Conductor>> getConductorsByCompanyId(Long companyId, String conductorName, Pageable pageable) {
        Disjunction disjunction = Restrictions.disjunction();
        if( conductorName != null && conductorName != "" ) {
            String[] name = conductorName.split(" ");
            disjunction.add(Restrictions.disjunction(
                    Restrictions.like("firstName", name[0], MatchMode.ANYWHERE),
                    Restrictions.like("lastName", name[0], MatchMode.ANYWHERE)) );
            if( name.length >= 2 ) {
                disjunction.add(Restrictions.disjunction(
                                                Restrictions.like("firstName", name[1], MatchMode.ANYWHERE),
                                                Restrictions.like("lastName", name[1], MatchMode.ANYWHERE)) );
            }
        }
        Criteria criteria = getSession().createCriteria(Conductor.class)
                                        .add(disjunction)
                                        .setFirstResult( pageable.getOffset() )
                                        .setMaxResults( pageable.getSize() );
        return new DefaultPage<>( new HashSet(criteria.list()), pageable, getConductorCountByCompany(companyId) );
    }
    @Override
    public List<Conductor> getUnAssigneedConductors( Long companyId ) {
        List<Conductor> conductors =  getSession().createCriteria(Conductor.class, "conductor")
                .createAlias("conductor.busAssignee", "busAssignee", JoinType.LEFT_OUTER_JOIN)
                .add( Restrictions.conjunction(
                        Restrictions.isNull("busAssignee.assigneeId"),
                        Restrictions.eq("companyId", companyId ) ) )
                .list();
        return new ArrayList<>( new HashSet<>( conductors ) );
    }

    @Override
    public Long getConductorCountByCompany(Long companyId) {
        return (Long) getSession()
                .createCriteria( Conductor.class )
                .add(Restrictions.eq("companyId", companyId))
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }

    @Override
    public Conductor getConductorByUsername(String username) {
         Criteria criteria = getSession().createCriteria(Conductor.class, "conductor")
                .createAlias("conductor.user", "user", JoinType.LEFT_OUTER_JOIN)
                .add( Restrictions.eq("user.username", username ) );
       // Query query = getSession().createQuery( String.format( "from %s WHERE user.username = %s", getActualParameterize().getSimpleName(), username));
        return (Conductor)( criteria.list().size() != 0? criteria.list().get(0) : new Conductor());
    }

    public Long getConductorsCount() {
        return (Long) getSession()
                .createCriteria( Conductor.class )
                .setProjection( Projections.rowCount() )
                .uniqueResult();
    }

}


