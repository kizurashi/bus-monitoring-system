package com.project.monitor.exception;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.*;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping({"${server.error.path:${error.path:/error}}"})
public class CustomErrorController implements ErrorController {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    private ErrorAttributes errorAttributes;


    @Value( "${error.path:/error}" )
    private String errorPath;

    public CustomErrorController( ErrorAttributes errorAttributes ) {
        Assert.notNull(errorAttributes, "ErrorAttributes must not be null");
        this.errorAttributes = errorAttributes;
    }

    @RequestMapping( produces = {"text/html"} )
    public ModelAndView errorHtml(HttpServletRequest request, HttpServletResponse response) {
        HttpStatus status = getStatus(request);
        Map<String, Object> body = getErrorAttributes(request, getTraceParameter( request ) );
        body.replace( "status", status.value() );
        ModelAndView modelView =  new ModelAndView("/error", body, status );
        return modelView;
    }

    @RequestMapping
    @ResponseBody
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        Map<String, Object> body = getErrorAttributes(request, getTraceParameter( request ) );
        HttpStatus status = getStatus(request);
        return new ResponseEntity<>(body, status);
    }

    protected Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace ) {
        RequestAttributes requestAttributes = new ServletRequestAttributes( request );
        return errorAttributes.getErrorAttributes( requestAttributes, includeStackTrace );
    }

    protected boolean getTraceParameter( HttpServletRequest req ) {
        String paramter = req.getParameter( "trace" );
        if( paramter == null ) {
            return false;
        }
        return !"false".equals(paramter.toLowerCase());
    }


    protected HttpStatus getStatus( HttpServletRequest req ) {
        Integer statusCode = (Integer) req.getAttribute("javax.servlet.error.status_code" );
        if ( statusCode != null ) {
            return HttpStatus.valueOf( statusCode );
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @Override
    public String getErrorPath() {
        return errorPath;
    }

}