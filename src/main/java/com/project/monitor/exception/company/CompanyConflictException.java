package com.project.monitor.exception.company;

import com.project.monitor.exception.ProjectRuntimeException;

public class CompanyConflictException extends ProjectRuntimeException {

    public CompanyConflictException(String message) {
        super(message);
    }
}
