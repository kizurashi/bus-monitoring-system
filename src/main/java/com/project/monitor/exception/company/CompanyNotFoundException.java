package com.project.monitor.exception.company;

import com.project.monitor.exception.ProjectRuntimeException;

public class CompanyNotFoundException extends ProjectRuntimeException{

    private static final long serialVersionUID = -8616277182754417408L;

    public CompanyNotFoundException( String message ) {
        super( message );
    }

}
