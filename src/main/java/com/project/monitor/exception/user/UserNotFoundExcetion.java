package com.project.monitor.exception.user;

import com.project.monitor.exception.ProjectRuntimeException;

public class UserNotFoundExcetion extends ProjectRuntimeException {

    private static final long serialVersionUID = -8616277182754417409L;

    public UserNotFoundExcetion(String message) {
        super(message);
    }

}
