package com.project.monitor.hibernate.impl;

import com.project.monitor.hibernate.HibernateSessionFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.PersistenceException;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@Transactional
public abstract class HibernateSessionFactoryImpl<T> implements HibernateSessionFactory<T> {

    Logger logger = LoggerFactory.getLogger( this.getClass() );

    private static SessionFactory sessionFactory;

    public void setSessionFactory( SessionFactory sessionFactory ) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public Session getSession() {
        return getSessionFactory().getCurrentSession();
    }

    public Class getActualParameterize(){
        return (Class)((ParameterizedType)this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public List<T> getAll() {
        Query query = getSession().createQuery( String.format( "from %s", getActualParameterize().getSimpleName() ) );
        try {
            return query.list();
        } catch ( PersistenceException e ) {
            throw new PersistenceException( e );
        }
    }

    @Override
    public T get(Serializable var) {
        return (T)getSession().get( getActualParameterize(), var  );
    }

    @Override
    public void save(T var) {
        getSession().saveOrUpdate( var );
    }

    @Override
    public void merge(T var) {
        getSession().merge( var );
    }

    @Override
    public void flush( ) {
        getSession().flush();
    }

    @Override
    public void delete(T var) {
        getSession().delete( var );
    }

    @Override
    public void delete( Serializable t ) {
        logger.info("{}", t);
        getSession().delete( getSession().get( getActualParameterize(), t ) );
    }

    @Override
    public Criteria createCriteria() {
        return null;
    }

}
