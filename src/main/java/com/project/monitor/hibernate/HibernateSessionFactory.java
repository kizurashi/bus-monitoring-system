package com.project.monitor.hibernate;

import org.hibernate.Criteria;

import java.io.Serializable;
import java.util.List;

public interface HibernateSessionFactory<T> {

    public List<T> getAll();

    public T get(Serializable var);

    public void save(T var);

    public void merge(T var);

    public void flush( );

    public void delete(T var);

    public void delete(Serializable t);

    Criteria createCriteria();

}
