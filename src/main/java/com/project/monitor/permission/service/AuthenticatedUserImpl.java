package com.project.monitor.permission.service;

import com.project.monitor.admin.conductor.dao.ConductorDao;
import com.project.monitor.admin.conductor.model.Conductor;
import com.project.monitor.bean.BeanUtils;
import com.project.monitor.admin.company.dao.CompanyDao;
import com.project.monitor.admin.company.model.Company;
import com.project.monitor.exception.user.UserNotFoundExcetion;
import com.project.monitor.permission.dao.UserDao;
import com.project.monitor.permission.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import javax.transaction.Transactional;

public class AuthenticatedUserImpl  {

    private final static Logger LOGGER = LoggerFactory.getLogger( AuthenticatedUserImpl.class );

    @Autowired
    private static CompanyDao companyDao = BeanUtils.getBean( CompanyDao.class );

    @Autowired
    private static UserDao userDao = BeanUtils.getBean( UserDao.class );

    @Autowired
    private static ConductorDao conductorDao = BeanUtils.getBean( ConductorDao.class );

    public Authentication getAuthentication() {
        return null;
    }

    public static User getUserDetails( String username ) {
        if( username == null )
            throw new UserNotFoundExcetion( String.format( "User not found %s", username ) );
        return userDao.getUserByUsername( username );
    }


    public static Company getCompanyDetails( Long userId ) {
        if( userId == null )
            throw new UserNotFoundExcetion( String.format( "User not found %d", userId ) );
        return companyDao.getCompanyByUserId( userId );
    }

    public static Conductor getConductorDetails( String username ) {
        if( username == null )
            throw new UserNotFoundExcetion( String.format( "User not found %d", username ) );
        return conductorDao.getConductorByUsername( username );
    }
}
