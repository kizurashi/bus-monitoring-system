package com.project.monitor.permission.service;

import com.project.monitor.admin.company.model.Company;
import com.project.monitor.permission.model.User;
import org.springframework.security.core.Authentication;

public interface AuthenticatedUser {

    public Authentication getAuthentication();

    public User getUserDetails(String username);

    public Company getCompanyDetails(Long userId);

}
