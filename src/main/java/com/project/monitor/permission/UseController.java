package com.project.monitor.permission;

import com.mysql.jdbc.log.LogFactory;
import com.project.monitor.ProjectMonitorApplication;
import com.project.monitor.exception.ProjectRuntimeException;
import com.project.monitor.permission.dao.UserDao;
import com.project.monitor.permission.model.User;
import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by WKS on 2/17/2018.
 */
@RestController
@RequestMapping( "users" )
public class UseController {

    private Logger LOGGER = org.slf4j.LoggerFactory.getLogger( this.getClass() );
    @Autowired
    public UserDao userDao;

    @RequestMapping( value = "", method = RequestMethod.GET )
    public List<User> getUsers() {
        return userDao.getUsers();
    }

    @RequestMapping( value = "/{userId}", method = RequestMethod.GET )
    public User getUserById( @PathVariable("userId") Integer userId ) {
        return userDao.getUser( userId );
    }

    @RequestMapping( value = "/username/{username}", method = RequestMethod.GET )
    public User getUserByUsername( @PathVariable("username") String username ) {
        return userDao.getUserByUsername( username );
    }

    @RequestMapping( value = "/", method = RequestMethod.POST )
    public void saveUser( @RequestBody User user ) {
        userDao.saveUser( user );
    }

    @RequestMapping( value = "/{userId}", method = RequestMethod.DELETE )
    public void deleteUser( @PathVariable("userId") Integer userId ) {
        userDao.deleteUser( userId );
    }

    @RequestMapping( value = "/change-password/{username}", method = RequestMethod.POST )
    public void changeUserPassword(@PathVariable("username") String username,
                                   @RequestParam( "currentPassword" ) @NotBlank String currentPassword,
                                   @RequestParam( "password" ) @NotBlank String password ) {
        LOGGER.info( "{}", userDao.comparePassword( username, currentPassword ) );
        if( !userDao.comparePassword( username, currentPassword ) )
            throw new ProjectRuntimeException("Mismatch current password");
        userDao.changeUserPassword( username, password );
    }

}
