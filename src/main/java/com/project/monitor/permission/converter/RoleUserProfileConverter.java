package com.project.monitor.permission.converter;

import com.project.monitor.permission.dao.UserRoleDao;
import com.project.monitor.permission.model.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RoleUserProfileConverter implements Converter<Object, UserRole> {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    UserRoleDao userRoleDao;

    @Override
    public UserRole convert(Object o) {
        Integer userId = (Integer)o;
        UserRole userRole =  userRoleDao.getUserRole( userId );
        return userRole;
    }

}

