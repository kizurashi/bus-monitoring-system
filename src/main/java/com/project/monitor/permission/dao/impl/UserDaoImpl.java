package com.project.monitor.permission.dao.impl;

import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.permission.dao.UserDao;
import com.project.monitor.permission.model.User;
import com.project.monitor.permission.dao.UserRoleDao;
import org.hibernate.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
public class UserDaoImpl extends HibernateSessionFactoryImpl<User> implements UserDao {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Override
    public Long saveUser(User user) {
        user.setPassword(passwordEncoder().encode(user.getPassword()));
        if( user.getId() == null ) {
            return (Long) getSession().save( user );
        } else {
            getSession().update( user );
            return user.getId();
        }
    }

    @Override
    public void deleteUser(Integer userId) {
        delete( userId );
    }

    @Override
    public User getUser(Integer userId) {
        return (User) get( userId );
    }

    @Override
    public User getUserByUsername(String username) {
        Query query = getSession().createQuery(
                String.format( "from %s where username = :username ", getActualParameterize().getSimpleName() ) );
        query.setParameter( "username", username );
        User user = !query.list().isEmpty()? (User)query.list().get( 0 ) : null;
        return user;
    }

    @Override
    public List<User> getUsers() {
        return getAll();
    }

    @Override
    public Optional<User> getUserCurrentlyLoggedIn() {
        User user = getUserByUsername( getPrincipal() );
        if ( user == null )
            return Optional.empty();
        return Optional.of(user);
    }

    @Override
    public void changeUserPassword(String username, String password) {
        String query = String.format("update User set password = :password where username = :username");
        getSession().createQuery( query )
                .setString("password", passwordEncoder().encode(password))
                .setString("username", username ).executeUpdate();
    }

    @Override
    public boolean comparePassword( String username, String currentPassword ) {
        return passwordEncoder().matches( currentPassword, getUserByUsername( username ).getPassword() );
    }

    /**
     * This method returns the principal[permission-name] of logged-in permission.
     */
    private String getPrincipal(){
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
