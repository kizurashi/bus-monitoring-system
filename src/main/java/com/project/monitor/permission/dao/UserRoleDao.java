package com.project.monitor.permission.dao;

import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.permission.model.UserRole;

import java.util.List;

/**
 * Created by Kizurashi on 2/12/2018.
 */
public interface UserRoleDao extends HibernateSessionFactory<UserRole> {

    void saveUserRole(UserRole role);

    void updateUserRole(Integer roleId, UserRole role);

    void deleteUserRole(Integer roleId);

    UserRole getUserRole(Integer roleId);

    List<UserRole> getUserRoles();

    UserRole findRole(String role);

}
