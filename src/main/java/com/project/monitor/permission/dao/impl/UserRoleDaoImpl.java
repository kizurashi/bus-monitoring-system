package com.project.monitor.permission.dao.impl;

import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.permission.dao.UserRoleDao;
import com.project.monitor.permission.model.UserRole;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Kizurashi on 2/12/2018.
 */
@Transactional
public class UserRoleDaoImpl extends HibernateSessionFactoryImpl<UserRole> implements UserRoleDao {

    Logger logger = LoggerFactory.getLogger( this.getClass() );

    @Override
    public void saveUserRole(UserRole role) {
        super.save( role );
    }

    @Override
    public void updateUserRole(Integer roleId, UserRole role) {}

    @Override
    public void deleteUserRole(Integer roleId) {
        super.delete( roleId );
    }

    @Override
    public UserRole getUserRole(Integer roleId) {
        return super.get( roleId );
    }

    @Override
    public List<UserRole> getUserRoles() {
        return super.getAll();
    }

    @Override
    public UserRole findRole(String role) {
        Criteria criteria = getSession().createCriteria(UserRole.class)
                                    .add(Restrictions.eq("role", role ) );
        return !criteria.list().isEmpty()? (UserRole)criteria.list().get( 0 ) : null;
    }
}
