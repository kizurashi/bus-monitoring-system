package com.project.monitor.permission.dao;


import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.permission.model.User;

import java.util.List;
import java.util.Optional;

/**
 * Created by Kizurashi on 2/12/2018.
 */
public interface UserDao extends HibernateSessionFactory<User> {

    Long saveUser(User user);

    void deleteUser(Integer userId);

    User getUser(Integer userId);

    User getUserByUsername(String username);

    List<User> getUsers();

    Optional<User> getUserCurrentlyLoggedIn();

    void changeUserPassword(String username, String password);

    public boolean comparePassword(String username, String currentPassword);

}
