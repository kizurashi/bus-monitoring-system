package com.project.monitor.page;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class DefaultPage<T> implements Page<T> {

    private int currentPage;

    private long totalPage;

    private T contents;

    private int limit;

    public DefaultPage(T contents, Pageable pageable, long total ) {
        this.contents = contents;
        this.currentPage = pageable.getPage() + 1;
        this.limit = pageable.getSize();
        this.totalPage = total;
    }

    @Override
    public int getCurrentPage() {
        return currentPage;
    }

    @Override
    public int getLimit() {
        return limit;
    }

    @Override
    public long getTotalPage() {
        return getLimit() == 0 ? 1 : (int) (Math.ceil( (double) totalPage / (double) getLimit()) );
    }

    @Override
    public T getContents() {
        return contents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultPage<?> that = (DefaultPage<?>) o;
        return currentPage == that.currentPage &&
                totalPage == that.totalPage &&
                Objects.equals(contents, that.contents);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currentPage, totalPage, contents);
    }

    @Override
    public String toString() {
        return "DefaultPage{" +
                "currentPage=" + getCurrentPage() +
                ", totalPage=" + getTotalPage() +
                ", contents=" + contents +
                ", limit=" + limit +
                '}';
    }
}
