package com.project.monitor.page;

import java.util.List;
import java.util.Objects;

public class PageRequest implements Pageable{

    private int page;
    private int size;

    public PageRequest(int page, int size) {
        this.page = page - 1;
        this.size = size;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PageRequest that = (PageRequest) o;
        return page == that.page && size == that.size;
    }

    @Override
    public int hashCode() {
        return Objects.hash(page, size);
    }

    @Override
    public String toString() {
        return "PageRequest{" +
                "page=" + (page + 1) +
                ", size=" + (size / page) +
                '}';
    }
}
