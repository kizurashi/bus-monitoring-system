package com.project.monitor.page;

public interface Pageable {

    int getSize();

    int getPage();

    default int getOffset() {
        return getSize() * getPage();
    }

    static Pageable create(int page, int size){
        return new PageRequest( page, size );
    }

}
