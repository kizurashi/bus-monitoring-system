package com.project.monitor.page;

import java.util.Collection;
import java.util.List;

public interface Page<T> {

    public int getCurrentPage();

    public int getLimit();

    public long getTotalPage();

    public T getContents();

}
