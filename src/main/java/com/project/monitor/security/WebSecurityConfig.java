package com.project.monitor.security;

import com.project.monitor.activity.model.Activity;
import com.project.monitor.admin.conductor.model.Conductor;
import com.project.monitor.permission.service.AuthenticatedUserImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    @Qualifier("customUserDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    private static SimpMessagingTemplate template;

    public void configure( WebSecurity web ) {
        web.ignoring().antMatchers(
                "/static/**",
                "/api/v1/bus/reverse-bus-origin/**",
                "/api/v1/bus/reserved/slot/**" );
    }

    @Autowired
    public void configureGlobalSecurity( AuthenticationManagerBuilder auth ) throws Exception {
        auth.userDetailsService( userDetailsService );
        auth.authenticationProvider( authenticationProvider() );
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService( userDetailsService );
        authenticationProvider.setPasswordEncoder( passwordEncoder() );
        return authenticationProvider;
    }

    static class SuccessfulHandler implements AuthenticationSuccessHandler {

        private Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

        @Override

        public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
            Collection<? extends GrantedAuthority> auth = authentication.getAuthorities();
            List<String> role = auth.stream().map( GrantedAuthority::getAuthority ).collect( Collectors.toList() );
            if ( role.contains( "ROLE_ADMIN" ) ) {
                httpServletResponse.sendRedirect("/admin");
            } else if ( role.contains( "ROLE_CONDUCTOR" ) ) {
                User principal = (User) authentication.getPrincipal();
                Conductor conductor = AuthenticatedUserImpl.getConductorDetails( principal.getUsername() );

                if( conductor.getBusAssignee() == null ) {
                    SecurityContextHolder.clearContext();
                    httpServletResponse.sendRedirect("/login?error=No bus is assigned to this user");
                }else {
//                    Activity activity = new Activity();
//                    activity.setAction("Logged In");
//                    activity.setMessage("Successfully logged in");
//                    activity.setTimestamp( new Timestamp( System.currentTimeMillis() ) );
//                    LOGGER.info("{}",conductor.getBusAssignee().getBus().getBusId());
                    LOGGER.info("{}",template);
//                    simpMessagingTemplate.convertAndSend("/post/activity/"+conductor.getBusAssignee().getBus().getBusId(), activity);
                    httpServletResponse.sendRedirect("/bus-client");
                }
            }
        }
    }

    @Configuration
    @Order(2)
    static class AdminSecuritConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure( HttpSecurity http ) throws Exception {
            http
                    .antMatcher("/**")
                    .authorizeRequests()
                    .antMatchers(
                            "/tracking/**",
                                        "/bus-client/**")
                    .access("hasRole('ROLE_CONDUCTOR')")
                    .antMatchers(
                            "/admin/**")
                    .access( "hasRole('ROLE_ADMIN')" )
                    .and()
                    .formLogin()
                    .loginPage("/login")
                    .loginProcessingUrl("/login")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .successHandler(new SuccessfulHandler())
                    .permitAll()
                    .and()
                    .logout()
                    .logoutUrl("/logout")
                    .permitAll()
                    .and()
                    .csrf().disable()
                    .exceptionHandling()
                    .accessDeniedHandler( new AccessDeniedHandlerImpl() );
        }
    }

    @Configuration
    @Order(1)
    static class BusClientSecurityConfig extends WebSecurityConfigurerAdapter {

        @Override
        public void configure( HttpSecurity http ) throws Exception {
            http.antMatcher("/api/v1/**")
                    .authorizeRequests()
                    .antMatchers( "/api/v1/**")
                    .access("hasRole('ROLE_CONDUCTOR') OR hasRole('ROLE_ADMIN')")
                    .anyRequest()
                    .authenticated()
                    .and()
                    .httpBasic().and()
                    .csrf().disable();
        }
    }

    @Configuration
    @Order(3)
    static class PassengerSecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        public void configure( HttpSecurity http ) throws Exception {
            http.antMatcher("/api/bus-search").authorizeRequests().antMatchers("/**").permitAll()
                    .and().antMatcher("/bus-trips").authorizeRequests().antMatchers("/**").permitAll().and()
                    .csrf().disable();
        }
    }

}