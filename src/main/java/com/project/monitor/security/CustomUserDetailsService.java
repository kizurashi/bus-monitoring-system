package com.project.monitor.security;

import java.util.ArrayList;
import java.util.List;

import com.project.monitor.permission.dao.UserDao;
import com.project.monitor.permission.model.User;
import com.project.monitor.permission.model.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    private final Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    UserDao userDao;

    @Transactional( readOnly=true )
    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException {
        User user = userDao.getUserByUsername( username );
        if (  user == null ) {
            LOGGER.info( "Username {} not found!", username );
            throw new UsernameNotFoundException( String.format( "Username %s not found!", username ) );
        }
        return new org.springframework.security.core.userdetails.User(
                                                                        user.getUsername(),
                                                                        user.getPassword(),
                                                                        true,
                                                                        true,
                                                                        true,
                                                                        true,
                                                                        getGrantedAuthorities( user ) );
    }


    private List<GrantedAuthority> getGrantedAuthorities( User user ){
        List<GrantedAuthority> authorities = new ArrayList<>();
        for( UserRole userRole : user.getUserRoles() ) {
            authorities.add( new SimpleGrantedAuthority("ROLE_"+ userRole.getRole() ) );
        }
        return authorities;
    }

}
