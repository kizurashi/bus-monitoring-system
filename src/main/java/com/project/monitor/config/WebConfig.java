package com.project.monitor.config;

import com.project.monitor.permission.converter.RoleUserProfileConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.resource.PathResourceResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {

    @Autowired
    RoleUserProfileConverter roleUserProfileConverter;

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler("/static/**" )
                .addResourceLocations( "/static/" )

                .setCachePeriod(3600)
                .resourceChain(true )
                .addResolver( new PathResourceResolver() );
    }

    @Bean
    public ViewResolver getViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/view");
        resolver.setSuffix(".jsp");
        resolver.setViewClass( JstlView.class );
        return resolver;
    }

    @Override
    public void configureDefaultServletHandling( DefaultServletHandlerConfigurer configurer ) {
        configurer.enable();
    }

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename( "messages" );
        return messageSource;
    }

    @Override
    public void addFormatters( FormatterRegistry registry ) {
        registry.addConverter( roleUserProfileConverter );
    }

    @Override
    public void addViewControllers( ViewControllerRegistry controller ) {
        controller.addViewController( "/error" ).setViewName( "/error" );
    }
}
