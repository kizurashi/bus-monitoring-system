package com.project.monitor.config;

public class ConstantConfig {

    private ConstantConfig() { }

    public final static String API_VERSION = "/v1";
    public final static String API_PREFIX = "/api" + API_VERSION;
    public final static String COMPANY = "/company";
    public final static String BUS = "/bus";
    public final static String CONDUCTOR = "/conductor";
    public final static String DRIVER = "/driver";
    public final static String ACTIVITY = "/activity";
    public final static String BUS_STATION_API = "/bus-station";
    public final static String BUS_SOURCE = "/bus-source";
    public final static String BUS_CUSTOM_SOURCE = "/bus-custom-source";

    private final static String ADMIN_PAGE = "/admin";
    public final static String LOGIN_PAGE = ADMIN_PAGE + "/login";
    public final static String DASHBOARD_PAGE = ADMIN_PAGE + "/dashboard";

    //Company web view
    private final static String COMPANY_PAGE = ADMIN_PAGE + COMPANY;
    public final static String COMPANY_PROFILE = COMPANY_PAGE + "/profile";
    public final static String UPDATE_COMPANY_PROFILE = COMPANY_PAGE + "/update-profile";
    public final static String REGISTER_COMPANY = COMPANY_PAGE + "/registration-form";

    //Bus Admin view
    private final static String BUS_PAGE = ADMIN_PAGE + BUS;
    public final static String BUS_LIST_PAGE = BUS_PAGE + BUS;
    public final static String ADD_BUS_PAGE = BUS_PAGE + "/add-bus";
    public final static String BUS_ASSIGN_CONDUCTOR_DRIVER = BUS_PAGE + "/assign-conductor-driver";
    public final static String ASSIGN_BUS_STATION = BUS_PAGE + "/assign-bus-station";
    public final static String BUS_PROFILE = BUS_PAGE + "/bus-profile";
    public final static String BUS_STATION = BUS_PAGE + "/bus-station";
    public final static String BUS_CUSTSOM_SOURCE_DESTINATION = BUS_PAGE + "/bus-trips";
    public final static String BUS_SOURCE_DESTINATION = BUS_PAGE + "/bus-source-or-destination";

    //Bus tracking view
    private final static String BUS_TRACKING_PAGE = ADMIN_PAGE + "/bus";
    public final static String BUS_TRACKING_INDEX = BUS_TRACKING_PAGE + "/bus-locator";

    //Conductor view
    private final static String CONDUCTOR_PAGE = ADMIN_PAGE + CONDUCTOR;
    public final static String ADD_CONDUCTOR_PAGE = CONDUCTOR_PAGE + "/add-conductor";
    public final static String CONDUCTOR_LIST_PAGE = CONDUCTOR_PAGE + "/conductor";

    //User

    //Driver
    private final static String DRIVER_PAGE = ADMIN_PAGE + DRIVER;
    public final static String DRIVER_INDEX = DRIVER_PAGE + DRIVER;
    public final static String ADD_DRIVER_PAGE = DRIVER_PAGE + "/add-driver";

    //Activity view
    private final static String BUS_ACTIVITY_PAGE = ADMIN_PAGE + ACTIVITY ;
    public final static String BUS_ACTIVITY_LIST = BUS_ACTIVITY_PAGE + "/activities";
    public final static String BUS_ADD_ACTIVITY = BUS_ACTIVITY_PAGE + "/add-activity";

}
