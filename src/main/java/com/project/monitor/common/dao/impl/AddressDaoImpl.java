package com.project.monitor.common.dao.impl;

import com.project.monitor.common.dao.AddressDao;
import com.project.monitor.common.model.Address;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;

public class AddressDaoImpl extends HibernateSessionFactoryImpl<Address> implements AddressDao {
}
