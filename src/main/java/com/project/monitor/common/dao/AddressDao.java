package com.project.monitor.common.dao;

import com.project.monitor.common.model.Address;
import com.project.monitor.hibernate.HibernateSessionFactory;

public interface AddressDao extends HibernateSessionFactory<Address> {
}
