package com.project.monitor.common.dao;

import com.project.monitor.hibernate.HibernateSessionFactory;
import com.project.monitor.common.model.GeoLocation;

public interface GeoLocationDao extends HibernateSessionFactory<GeoLocation> {

    public Long saveGeoLocation( GeoLocation geoLocation );

}

