package com.project.monitor.common.dao.impl;

import com.project.monitor.common.dao.GeoLocationDao;
import com.project.monitor.common.model.GeoLocation;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;

public class GeoLocationDaoImpl extends HibernateSessionFactoryImpl<GeoLocation> implements GeoLocationDao {

    @Override
    public Long saveGeoLocation(GeoLocation geoLocation) {
        if( geoLocation.getLocationId() == null ) {
            return (Long) getSession().save( geoLocation );
        } else {
            getSession().merge( geoLocation );
            return geoLocation.getLocationId();
        }
    }

}
