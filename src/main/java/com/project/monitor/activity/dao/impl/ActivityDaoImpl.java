package com.project.monitor.activity.dao.impl;

import com.project.monitor.activity.dao.ActivityDao;
import com.project.monitor.activity.model.Activity;
import com.project.monitor.common.dao.GeoLocationDao;
import com.project.monitor.hibernate.impl.HibernateSessionFactoryImpl;
import com.project.monitor.page.DefaultPage;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class ActivityDaoImpl extends HibernateSessionFactoryImpl<Activity> implements ActivityDao {

    private Logger LOGGER = LoggerFactory.getLogger( this.getClass() );

    @Autowired
    private GeoLocationDao geoLocationDao;

    @Override
    public void saveActivity(Activity activity) {
        if( activity.getLocation() != null ) {
            Long geoLocationId = geoLocationDao.saveGeoLocation( activity.getLocation() );
            activity.getLocation().setLocationId( geoLocationId );
        }
        save( activity );
    }

    @Override
    public Activity getActivity(Long activityId) {
        return get( activityId );
    }

    @Override
    public Page<List<Activity>> getActivities(Long companyId, Pageable pageable) {
        Long total = getTotalActivities(companyId);
        Query query = getSession().createQuery( String.format(
                "from %s where companyId = %d",
                getActualParameterize().getSimpleName(),
                companyId ) ).setFirstResult(pageable.getOffset()).setMaxResults(pageable.getSize());
        LOGGER.info("{}", query.list());
        return new DefaultPage<>(query.list(), pageable, total );
    }

    @Override
    public void deleteActivty( Long activityId ) {
        delete( activityId );
    }

    @Override
    public Page<Map<String, List<Activity>>> getActivityByCompany(Long companyId, String dateString, Pageable pageable) {
        return getActivityies( "companyId", companyId, dateString, pageable);
    }

    @Override
    public Page<Map<String, List<Activity>>> getActivityByBus(Long busId, String dateString, Pageable pageable) {
        return getActivityies( "busId", busId, dateString, Pageable.create(0,10));
    }

    @Override
    public Page<Map<String, List<Activity>>> getActivityPreviewByCompany(Long companyId) {
        return getActivityies( "companyId", companyId,"", Pageable.create(0,10) );
    }

    @Override
    public Long getTotalActivities(Long companyId) {
        return (Long) getSession().createCriteria( Activity.class )
                .setProjection(Projections.rowCount()).list().get(0);
    }

    private Page<Map<String, List<Activity>>> getActivityies(String property, Long companyId, String dateString, Pageable pageable ) {
        Query query = getSession().createQuery( String.format( "from %s WHERE %s = %d AND timestamp LIKE '%s' ORDER BY timestamp DESC",
                getActualParameterize().getSimpleName(),
                property,
                companyId,
                dateString+"%" ) ).setFirstResult(pageable.getOffset()).setMaxResults(pageable.getSize());
        List<Activity> activities = query.list();
        Map<String, List<Activity>> sortedActivities = new TreeMap<>( Comparator.reverseOrder() );
        Map<String, List<Activity>> act = activities.stream()
                .sorted( Comparator.comparing( Activity::getTimestamp ) )
                .collect( Collectors.groupingBy( Activity::getFormattedDate ) );
        sortedActivities.putAll( act );
        return new DefaultPage<>( sortedActivities, pageable, getTotalActivities( companyId ) );
    }

    @Override
    public Activity getBusLastActivity(Long busId) {
        Query query = getSession().createQuery( String.format(
                "from %s where busId = %d AND timestamp LIKE '%s'",
                getActualParameterize().getSimpleName(),
                busId,
                new SimpleDateFormat("yyyy-MM-dd").format(new Date())+"%"));
        return (Activity)(!query.list().isEmpty()? query.list().get(0): new Activity());
    }
}
