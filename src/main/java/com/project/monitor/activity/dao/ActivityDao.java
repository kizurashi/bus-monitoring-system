package com.project.monitor.activity.dao;

import com.project.monitor.activity.model.Activity;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;

import java.util.List;
import java.util.Map;

public interface ActivityDao {

    public void saveActivity( Activity activity );

    public Activity getActivity( Long activityId );

    public Activity getBusLastActivity( Long busId );

    public Page<List<Activity>> getActivities( Long companyId, Pageable pageable);

    public void deleteActivty( Long activityId );

    public Page<Map<String, List<Activity>>> getActivityByCompany( Long companyId, String dateString, Pageable pageable );

    public Page<Map<String, List<Activity>>> getActivityByBus( Long busId, String dateString, Pageable pageable );

    public Page<Map<String, List<Activity>>> getActivityPreviewByCompany(Long companyId);

    public Long getTotalActivities( Long companyId );
}
