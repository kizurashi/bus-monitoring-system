package com.project.monitor.activity.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project.monitor.admin.bus.model.Bus;
import com.project.monitor.common.model.GeoLocation;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

@Transactional
@Entity
@Table( name = "activitylog" )
public class Activity implements Serializable, Comparator<Activity> {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long activityId;

    @Column( name = "action", nullable = false )
    private String action;

    @Column( name = "message", nullable = false )
    private String message;

    private Timestamp timestamp;

    @OneToOne
    @JoinColumn( name = "locationId" )
    private GeoLocation location;

    @ManyToOne
    @JoinColumn( name = "busId" )
    private Bus bus;

    private Long companyId;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GeoLocation getLocation() {
        return location;
    }

    public void setLocation(GeoLocation location) {
        this.location = location;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }


    @JsonIgnore
    public String getFormattedDate() {
        Date date = new Date();
        date.setTime(getTimestamp().getTime());
        return new SimpleDateFormat( "yyyy-MM-dd" ).format( date );
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    @Override
    public int compare(Activity o1, Activity o2) {
        return 0;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Activity activity = (Activity) o;
        return Objects.equals(activityId, activity.activityId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(activityId);
    }

    @Override
    public String toString() {
        return "Activity{" +
                "activityId=" + activityId +
                ", action='" + action + '\'' +
                ", message='" + message + '\'' +
                ", timestamp=" + timestamp +
                ", companyId=" + companyId +
                ", bus=" + bus +
                '}';
    }
}
