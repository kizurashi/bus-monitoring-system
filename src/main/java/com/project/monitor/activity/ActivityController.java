package com.project.monitor.activity;

import com.project.monitor.activity.dao.ActivityDao;
import com.project.monitor.activity.dao.impl.ActivityDaoImpl;
import com.project.monitor.activity.model.Activity;
import com.project.monitor.config.ConstantConfig;
import com.project.monitor.page.Page;
import com.project.monitor.page.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping( ConstantConfig.API_PREFIX + "/activity" )
public class ActivityController {

    @Autowired
    private ActivityDao activityDao;

    @RequestMapping( value = "", method = RequestMethod.POST )
    public void saveActivity( @RequestBody Activity activity) {
        activityDao.saveActivity(activity);
    }

    @RequestMapping( value = "/{activityId}", method = RequestMethod.GET )
    public Activity getActivity( @PathVariable( "activityId" ) Long activityId ) {
        return activityDao.getActivity(activityId);
    }

    @RequestMapping( value = "", method = RequestMethod.GET )
    public Page<Map<String, List<Activity>>> getActivities(
                                                           @RequestParam Long companyId,
                                                           @RequestParam( required = false, defaultValue = "") String date,
                                                           @RequestParam( required = false, defaultValue="1") int page,
                                                           @RequestParam( required = false, defaultValue="10") int size ) {
        return activityDao.getActivityByCompany( companyId, date, Pageable.create( page, size ) );
    }

    @RequestMapping( value = "/{activityId}", method = RequestMethod.DELETE )
    public void deleteActivty( @PathVariable( "activityId" ) Long activityId ) {
        activityDao.deleteActivty( activityId );
    }

    @RequestMapping( value = "/busLastActivity/{busId}", method = RequestMethod.GET )
    public Activity getBusLastActivity( @PathVariable( "busId" ) Long busId ) {
        return activityDao.getBusLastActivity( busId );
    }

}
