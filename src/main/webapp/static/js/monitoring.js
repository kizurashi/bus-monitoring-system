var busStationCache = new Array();
var index = 0;
var updateBusStationList = function () {
    var html = "";
    $.each( busStationCache, function( index, value ){
        var count = index + 1;
        html += '<tr>' +
                  '<td>' +
                      '<input type="hidden" name="companyBusStations['+index+'].id" value="'+value.id+'" type="text">' +
                      '<input type="hidden" name="companyBusStations['+index+'].companyBusTripId" value="'+value.busTripId+'" type="text">' +
                      '<input type="hidden" name="companyBusStations['+index+'].priority" value="'+value.priority+'" type="text">' +
                      '<input type="hidden" name="companyBusStations['+index+'].busStation.stationId" value="'+value.stationId+'" type="text">' +
                      count
                  +'</td>' +
                  '<td>' +
                      '<input type="hidden" name="companyBusStations['+index+'].busStation.stationName" value="'+value.name+'" type="text">' +
                      '<input type="hidden" name="companyBusStations['+index+'].busStation.location.locationId" value="'+value.locationId+'" type="text">' +
                      value.name
                  + '</td>' +
                  '<td>' +
                        '<button type="button" class="btn btn-danger removeBusStation" data-id="'+value.id+'" ><span class="fa fa-trash"></span></button>' +
                   '</td>' +

                '</tr>';
    });
    $('.busStationList').html( html );
}

var addBuSStationCache = function( companyId, stationId, stationName, locationId, lat, lng, busTripId, id, priority) {
    if( busStationCache.filter( station => station.stationId == stationId ).length == 0 ) {
        busStationCache.push( { companyId: companyId, stationId: stationId, name: stationName, locationId: locationId, lat: lat, lng: lng, busTripId: busTripId, id: id, priority: priority });
    }
}

$(document).ready(function(){

    //UI
    $('[data-toggle="offcanvas"]').click(function(){
       $("#navigation").toggleClass("hidden-xs");
    });

    $('#assign-bus-station').on( 'click', function() {
        var stationName = $('.bus-station').find(':selected').data('name');
        var companyStationId = $('.bus-station').find(':selected').data('companystationid');
        var locationId = $('.bus-station').find(':selected').data('locationid');
        var companyId = $('.bus-station').find(':selected').data('companyid');
        var lat = $('.bus-station').find(':selected').data('lat');
        var lng = $('.bus-station').find(':selected').data('lng');
        var busTripId = $('.bus-station').find(':selected').data('busTripId');
        var priority = busStationCache.length;
        var id = $('.bus-station').find(':selected').data('id');

        var stationId =  $('.bus-station').val();
        if( stationName != undefined &&  busStationCache.find(function(e){ return e.stationId == stationId }) ===  undefined ) {
            //TODO: add as waypoints
            addBuSStationCache( companyId, stationId, stationName, locationId, lat, lng, busTripId, id, priority );
            updateBusStationList();
            calculateAndDisplayRoute(directionsService, directionsDisplay);  
        }
    });

    //HTTP Operation(s)
    $('.delete-domain').on( 'click', function() {
        $('.name').text( $(this).data('name') );
        $('.delete-object').text( $(this).data('object').replace( /(\/|-)/g, ' ') );
        $('#object').val( $(this).data('object') );
        $('#id').val( $(this).data('id') );
        $('#object-name').val( $(this).data('name') );
    });

    $('.update-custom-source').on( 'click', function() {
        $('#companyBusTripId').val( $(this).data('id') );
        $('#sourceId').val( $(this).data('source') );
        $('#destinationId').val( $(this).data('destination') );
    });

    $('.update-domain-station').on( 'click', function() {
      $('#stationId').val( $(this).data('id') );
      $('#stationName').val( $(this).data('name') );
      $('.change-me-please').text("Update Bus Station");
      $('.update-domain-button').text("Update Bus Station");

    });

    $('.update-domain').on( 'click', function() {
        console.log( $(this).data('name') )
        $('#sourceOrDestination').val( $(this).data('name') );
        $('#sourceDestinationId').val( $(this).data('id') );
        $('.change-me-please').text("Update Bus Source or Destination");
        $('.update-domain-button').text("Update Bus Source or Destination");

    });
});

$(document).on( 'click', '#delete-domain-proceed', function() {
    var object = $('#object').val();
    var id = $('#id').val();
    var name = $('#object-name').val();
    $.ajax({
       url: baseUrl + '/' + object + '/' + $('#id').val(),
       method: 'DELETE',
       data: {},
       success: function( data ) {
        $.notify('Successfully deleted '+object.replace('/',' ')+' - '+ name +'.','success');
        $( '.' + object.replace('/','-') + '-id-'+ id ).remove();
       },
       error: function ( error ) {
       }
    });
});

$(document).on('click', '.removeBusStation', function() {
    var stationId = $(this).data('id');
    var key;
    $.each( busStationCache, function(index,value) {
            console.log( value )
        if( value.stationId == stationId ) {
            key = index;
            return;
        }
    });
    busStationCache.splice( key, 1 )
    calculateAndDisplayRoute(directionsService, directionsDisplay);
    updateBusStationList();
});

var pagination = function( elementId, page, totalPage ) {
  if( page > totalPage ){
    page = totalPage;
  }
  var pages = '';
  if(totalPage != 1 ){
    var link = window.location.href;
    link = link.replace(/\?.*/g, '');
    link += '?page=';
    if( page > 1 ) {
      previousPage = page - 1;
      console.log( previousPage )

      pages += '<a class="btn btn-default" href = "'+link+previousPage+'" class="btn btn-default"><span aria-hidden="">&laquo;</span></a>';
      for(var ctr = page - 2; ctr < page ; ctr ++){
          console.log( page )

        if(ctr > 1){
          pages +=  '<a class="btn btn-default" href = "'+link+ctr+'">'+ctr+'</a>';
        }
      }
    }

    if( page != 0 ){
        console.log( page )
        pages +=  '<a class="btn btn-primary">'+page+'</a>';
    }

    for(ctr = page+1; ctr <= totalPage ; ctr ++){
        pages +=  '<a class="btn btn-default" href = "'+link+ctr+'" class="btn btn-default">'+ctr+'</a>';
        if(ctr > page + 2){
          break;
        }
    }
    if(page != totalPage)
        pages += '<a class="btn btn-default" href = "'+link+(page+1)+'" class="btn btn-default"><span aria-hidden="true">&raquo;</span></a>';

    $('#'+elementId).html( pages );
  }
}

var source, destination, directionsService, directionsDisplay, lat, lng;
