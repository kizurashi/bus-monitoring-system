var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var currentLocation;
var locations;
var isRunning = false;
var map;
var markers;
var directionsService;

var getLocation =  function( position ) {
    var currentLocation = { lat: position.coords.latitude,lng: position.coords.longitude }
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 7,
      center: currentLocation,
      mapTypeControl: true,
      navigationControl: true
    });
};

var markerRepo = {};

var updateMarker = function(bus) {
    var marker;
    var infowindow;
    var currentLocation = bus.currentGeoLocation;
    var location = new google.maps.LatLng(currentLocation.lat, currentLocation.lng);
    var start = bus.currentGeoLocation.lat +','+bus.currentGeoLocation.lng;
    var end = bus.nextBusStop.location.lat +','+bus.nextBusStop.location.lng;
    var estimatedTime =  calculateEstimatedArrivalTime( start, end );
    estimatedTime.then(function(e){
        var speed = 'Not Available', action = 'Not Available', activity = bus.lastActivity;
	if( activity ) {
		action = activity.action
	}
	if( currentLocation.speed ){
		speed = currentLocation.speed;
	}
        if( markerRepo[bus.busId] ) {
            var myMarker = markerRepo[bus.busId];
            myMarker.marker.setPosition(location)
            
            myMarker.content.setContent("Bus:  "+ bus.plateNumber+"<br>Speed:"+speed+"<br>Available Seat:"+bus.availableSeat+"<br>Next Bus Stop:"+bus.nextBusStop.stationName+"<br>ETA:"+e  );
            myMarker.content.open(map, myMarker.marker);
        } else {
            infowindow = new google.maps.InfoWindow();
            marker =  new google.maps.Marker({
                position: location,
                label: 'B',
                map: map,
                title: bus.plateNumber
            });
            markerRepo[bus.busId] = { marker: marker, content: infowindow};
            infowindow.setContent("Bus:  "+ bus.plateNumber+"<br>Speed:"+speed+"<br>Next Bus Stop:"+bus.nextBusStop.stationName+"<br>ETA:"+e  );
            infowindow.open(map, marker);
            addMarkerListner(marker,infowindow)
        }
    });
    
}


var error = function( error ) {
    alert( error.code + " : " + error.message );
};


var getCurrentLocation = function( ) {

}

function initMapsu() {
    directionsService = new google.maps.DirectionsService;
    if( navigator.geolocation ) {
        navigator.geolocation.getCurrentPosition( getLocation, error )
    } else {
        alert( "Browser not supports Geolocation" )
    }
}

function updateMarkerPosition() {

}

function addMarkerListner( marker, infowindow ) {
    google.maps.event.addListener(marker, 'click', (function(marker) {
        return function() {
          markerInfoTransition( marker, infowindow )
        }
    })(marker));
}

function markerInfoTransition( marker, infowindow ) {
    if( !infowindow.getMap() ) {
      infowindow.open(map, marker);
    } else {
      infowindow.close( map, marker );
    }
}
