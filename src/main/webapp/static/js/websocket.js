var stompClient = null;

function connect(subscribers) {
    var socket = new SockJS('/monitor');
    stompClient = Stomp.over(socket);
    //stompClient.debug = null;
    stompClient.connect({}, subscribers);
}

function publishMessage( listener, data ) {
    stompClient.send( listener, {}, JSON.stringify( data ) );
}


