/* eslint no-alert: 0 */

'use strict';

//
// Here is how to define your module
// has dependent on mobile-angular-ui
//
var app = angular.module('myApp', [
  'onsen'
]);

var gmarkers = [];
var closest = [];
var busStations = [];
var src, destination, currentLocation = { lat:0, lng:0 }, geocoder, travelEnabled;
var copyBusStations = [];

var codeAddress = function (address) {
  var numberOfResults = 25;
  var numberOfDrivingResults = 5;
  var latlng = {lat: address.lat, lng: address.lng};
  var sourceLatLng = new google.maps.LatLng(src.lat, src.lng)
  var destinationLatLng = new google.maps.LatLng(destination.lat, destination.lng)
  var sourceDist = computeDistance(sourceLatLng, address)
  var destinationDist = computeDistance(destinationLatLng, address)
  if( nextStop ) {
      var stationLatLng = new google.maps.LatLng(nextStop.lat, nextStop.lng)
      var stationDistance = computeDistance(stationLatLng, address)
     stationDistance.then(d=>{
          var index = getCurrentStation(nextStop.id)
          if( d.distance.value <= 100 ) {
              if( ( index + 1 ) < busStations.length -1 ) {
                  nextStop = busStations[index+1]
                  updateNextStop( nextStop )
              } else {
                  updateNextStop( {id:0, stationName:""} )
                  nextStop = {};
              }
          }
     })
  } else {
      if( sourceDist < destinationDist ) {
          console.log("Source")
          sortByPriority()
          reverseOriginDestination(0)
      } else {
          console.log("Destination")
          sortByPriorityDesc()
          reverseOriginDestination(1)
      }

      if( travelEnabled ) {
          updateNextStop(0)
          travelEnabled = true;
      } else {
          nextStop = busStations[0]
          updateNextStop( busStations[0] )
      }
  }
}

var computeDistance = function(pt, pt2){
    return new Promise(function(resolve){
        var service = new google.maps.DistanceMatrixService();
        var request = {
            origins:[pt] ,
            destinations: [pt2],
            travelMode: google.maps.TravelMode.DRIVING,
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        };
        service.getDistanceMatrix(request, function(response, status) {
            if (status != google.maps.DistanceMatrixStatus.OK) {
                alert('Error was: ' + status);
            } else {
                resolve(response.rows[0].elements[0]);
            }
        });
    })
    //return google.maps.geometry.spherical.computeDistanceBetween(pt, pt2);
}
var sortByPriority = function(){
    busStations.sort(function(a, b) {
        return a.priority + b.priority;
    });
}

var sortByPriorityDesc = function(){
    busStations.sort(function(a, b) {
        return b.priority + a.priority;
    });
}

var getCurrentStation = function(id){
    return busStations.findIndex(function(station) {
        return station.id == id;
    });
}

var urlpages = {
    "availSeat":"/view/bus/html/update-available-seat.jsp",
    "nextStop":"/view/bus/html/update-bus-stop.jsp",
    "origin":"/view/bus/html/change-source-destination.jsp",
    "password":"/view/bus/html/change-passowrd.jsp",
    "arrival":"/view/bus/html/arrival-departure.jsp"

}

var updateNextStop = function(station){
    $('#nextBusStop').text(station.title);
    nextStop = station;
    publishMessage( '/app/update-bus-stop/'+id, station.id );
}

var reverseOriginDestination = function( reverse ) {
  $.ajax({
     url: baseUrl + '/bus/reverse-bus-origin/'+id,
     method: 'POST',
     data: { travelMode: reverse },
     success: function( data ) {
      var origin, dest;
      if( reverse ) {
        origin = destination.title.split('|')[0];
        dest = src.title.split('|')[0];
      } else {
        origin = src.title.split('|')[0];
        dest = destination.title.split('|')[0];
      }
      $('#origin').text( origin );
      $('#destination').text( dest );
     },
     error: function ( error ) {
     }
  });
}

var watchId;
var currentLocation;
var getLocation = function( position ) {
    currentLocation = { "type": "Source", "title": "Current Location", "lat": position.coords.latitude, "lng": position.coords.longitude };
    if( nextStop ) {
      if( stompClient.connected ){
          publishMessage( '/app/geolocation/'+companyId, {"busId": id,"plateNumber": plateNo, "availableSeat": parseInt($('#availableSeat').text()),
            "currentGeoLocation": { "lat": position.coords.latitude, "lng": position.coords.longitude, "speed":speedInKPH(position.coords.speed) },
            "nextBusStop": { "location":{"lat": nextStop.lat, "lng": nextStop.lng}, "stationName": nextStop.title }
            } );
      }
      $('#location').text(nextStop.title);
    }
    if( stompClient.connected )
      codeAddress( currentLocation );
    $('#latitude').text( position.coords.latitude );
    $('#speed').text( speedInKPH(position.coords.speed) );
    $('#longitude').text( position.coords.longitude );
    $('#trackingStatus').text( 'Tracking is enabled for this bus.' );
 };

 var speedInKPH = function( speed ){
    if(!speed)
        return speed;
    return speed * 3.600000;
 }

 var stopTracking = function( ) {
     document.cookie="trackingStatus=off";
     $('#trackingStatus').text( 'Tracking is disabled for this bus' );
     $('#latitude').text("N/A");
     $('#longitude').text("N/A");
     $('#speed').text("N/A");
     navigator.geolocation.clearWatch(watchId);
     $('.tracking').css("background-color","red")

 };

var startTracking = function() {
    if( navigator.geolocation ) {
      document.cookie="trackingStatus=on";
      watchId = navigator.geolocation.watchPosition( getLocation, error );
    } else {
        stopTracking();
        alert( "Browser not supports Geolocation" )
    }
}

var error = function( error ) {
    stopTracking();
    alert( error.code + " : " + error.message );
};

var getCookie = function(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
};

var logActivity = function( action, message ) {
  var latLng = {"lat":currentLocation.lat, "lng":currentLocation.lng};
  getAddress( latLng ).then( address => {
    publishMessage( "/app/post/activity/"+id, { "activityId": null, "action": action, "message": message, "timestamp": new Date(),
                                              "location": { "locationName": address.formatted_address, "lat":latLng.lat, "lng":latLng.lng, "speed": 0 },
                                              "bus": { "plateNumber": plateNo, "busId": id },
                                              "companyId": companyId  } )
    updateActivityLogs(1);
  })
}

var getAddress = function ( latLng ){
  return new Promise( resolve => {
    geocoder.geocode({
      'location': latLng
    }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        resolve( results[0] );
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  });
}

var updateActivityLogs = function(page){
  $.ajax({
     url: baseUrl + '/activity',
     method: 'GET',
     data: {companyId:companyId, page: page},
     success: function( data ) {
      data = data.contents;
      var activities = '';
      Object.keys(data).forEach( (index)=>{
        activities += '<div class="he-who-must-not-be-named">'+
                        '<div class="a-s--title current-head" data-date="'+index+'">'+
                            '<h3>'+index+'</h3>'+
                        '</div>'+
                        '<div class="activity-streams activity-streams-'+index+'">'
                          + logContent(data[index]) +
                        '</div>'+
                      '</div>';
      });
      setTimeout(function(){
        document.getElementById('logActivities').innerHTML = activities;
      },3000)
     },error: function ( error ) {
     }
  });
}

var logContent = function( logActivities ) {
  var content = '';
  logActivities.forEach( logActivity => {
    var date = new Date(logActivity.timestamp);
    var time = date.getHours() +':'+ date.getMinutes() +':'+ date.getSeconds();
    content += '<div class="a-s--item">'+
                  '<div class="a-s--icon">'+
                  '</div>'+
                  '<div class="a-s--body">'+
                      '<h4>Bus No: '+logActivity.bus.plateNumber+'</h4>'+
                      '<h5>Message: '+logActivity.message+'</h5>'+
                      '<p>Posted: <strong> '+time+' | </strong>Status: <strong> '+logActivity.action+' | </strong>Location: <strong>'+logActivity.location.locationName+'</strong></p>'+
                  '</div>'+
                '</div>';
  })
  return content;
}

var logMalfunction = function() {
  var message = 'Bus is currently stuck because of malfunction.';
  logActivity( 'Need to repair.', message )
}

var logTraffic = function(){
  var message = 'Bus is currently stuck in the traffic.';
  logActivity( 'Traffic', message )
}

var logAccident = function(){
  var message = 'Bus is currently stuck in the accident.';
  logActivity( 'Accident', message )
}

// var search = function(currentPage){
//     if( stationId ){
//         $.ajax({
//             url: baseUrl + '/bus/reserved/slot/bus/'+id+'/station/'+stationId,
//             method: 'GET',
//             data: {code: $('#searchCode').val(), page: currentPage},
//             success: function( data ) {
//                 data = data.contents;
//                 var reserved = '';
//                 data.forEach( (r, i)=>{
//                     reserved += '<ons-list-item tappable="" modifier="longdivider" class="list-item list-item--longdivider"><div class="center list-item__center list-item--longdivider__center">'+
//                     '<ons-row>'+
//                     '<ons-col>'+r.code+'</ons-col>'+
//                     '<ons-col>'+r.reservedSlot+'</ons-col>'+
//                     '</ons-row>'+
//                     '</div></ons-list-item>';
//             });
//                 document.getElementById('reserved').innerHTML = reserved;
//             },
//             error: function ( error ) {
//             }
//         });
//     }
// }

app.controller('MainController', function($rootScope, $scope, $sce, $http, $compile, $timeout) {
  $scope.activity = {};
  $scope.currentPage = 1;
  $scope.pageSize = 10;
  updateActivityLogs($scope.currentPage)
  $scope.currentPageFN = function( currentPage ) {
    updateActivityLogs( currentPage );
    return $scope.currentPage = currentPage;
  }

  // $scope.currentPageRS = function( currentPage ) {
  //   $scope.currentPage = currentPage;
  //   search(currentPage);
  // }

  if( isReverse ) {
    $scope.test=true
    $('#switch1').val( "on" )
  } else {
    $scope.test=false
    $('#switch1').val("off")
  }
  // User agent displayed in home page
  $scope.userAgent = navigator.userAgent;
   // angular.element(document.getElementById('pagination').append(temp))
  $scope.logActivity = function() {
    var status = $('#select-status').val();
    getAddress( currentLocation )
    switch( status ) {
        case 'Malfunction':
            logMalfunction( );
            break;
        case 'Accident':
            logAccident( );
            break;
        case 'Traffic':
            logTraffic( )
            break;
    }
    ons.notification.alert("Successfully posted new activity log.")
  }

  // $scope.searchCode = function(){
  //   search();
  // };

  $scope.bus = {}

  $scope.enableTracking = function(){
    var background = "rgba(24,103,194,.81)"
    if( getCookie("trackingStatus") != 'off' ) {
        stopTracking()
        background = "red"
    } else  {
       background = "#0076ff"
       navigator.geolocation.clearWatch(watchId);
       startTracking()
    }
    $scope.trackingColor = { "background-color": background }
  }

  $scope.showDialog = function (id) {
    var infoButton = document.getElementById('info-button');
    var elem = document.getElementById(id);
    if (id === 'popover-dialog') {
      elem.show(infoButton);
    }
  };
  
   $scope.loadArrivalDeparture = function(){
      $scope.pushPage('/travel/logs/'+id+'/'+assigneeId, '')
      $timeout(function(){
        $.ajax({
          url: '/api/v1/travel/logs/'+ busId + '/'+assigneeId,
          method:"GET",
          beforeSend: function(){},
          success: function( data ) {
            $scope.departuretime = data.departuretime;
            $scope.arrivaltime = data.departuretime;
            if( data.departureTime ){
              $('#departure').val( new Date(data.departureTime).toLocaleDateString() +" "+ new Date(data.departureTime).toLocaleTimeString() )
              $('#arrival').val('')
              $('#arrivalbutton').removeAttr('disabled');
              $('#departurebutton').attr('disabled','');
            }
            if(data.arrivalTime ){
              console.log('jsdklfjsdklf')
              $('#arrival').val( new Date(data.arrivalTime).toLocaleDateString() +" "+ new Date(data.arrivalTime).toLocaleTimeString() )
              $('#departurebutton').removeAttr('disabled');
              $('#arrivalbutton').attr('disabled','');
            }
             // ons.notification.alert('Successfully changed your password');
              //window.location.href = "/logout";
          },
          error: function( err ) {
              //ons.notification.alert("Failed. Current password doesn't match")
          }
       });
      },1000)

    }
//.style.transform="0"
  $scope.pushPage = function (page, anim) {
      if (anim) {
        document.querySelector('#myNavigator').pushPage(page, { data: { title: page.title }, animation: anim });
      } else {
        document.querySelector('#myNavigator').pushPage(page, { data: { title: page.title } });
      }
  };
  $scope.arrivelDeparture = function(action){
    $.ajax({
      url: '/api/v1/travel/logs/'+action,
      method:"POST",
      headers: { 'Content-Type': 'application/json' },
      data: JSON.stringify({ isSourceToDestination: isReverse, bus: { busId : id }, busAssignee: { assigneeId: assigneeId }, busTrips: { companyBusTripId: busTripId } }),
      beforeSend: function(){},
      success: function( data ) {
        $scope.departuretime = data.departuretime;
        $scope.arrivaltime = data.departuretime;
        if( data.departureTime ){
          $('#departure').val( new Date(data.departureTime).toLocaleDateString() +" "+ new Date(data.departureTime).toLocaleTimeString() )
          $('#arrival').val('')
          $('#arrivalbutton').removeAttr('disabled');
          $('#departurebutton').attr('disabled','');
        }
        if(data.arrivalTime){
          $('#arrival').val( new Date(data.arrivalTime).toLocaleDateString() +" "+ new Date(data.arrivalTime).toLocaleTimeString() )
          $('#departurebutton').removeAttr('disabled');
          $('#arrivalbutton').attr('disabled','');
        }
         // ons.notification.alert('Successfully changed your password');
          //window.location.href = "/logout";
      },
      error: function( err ) {
          //ons.notification.alert("Failed. Current password doesn't match")
      }
   });
  }

  $scope.loadView = function (index) {
    document.getElementById('appTabbar').setActiveTab(index);
    document.getElementById('sidemenu').close();
  };

  $scope.hideDialog = function (id) {
    document.getElementById(id).hide();
    //if( page )
    //    $scope.pushPage(urlpages[page],"")
  };

  if( getCookie("trackingStatus") == 'on' ) {
     setTimeout(function(){
        $scope.trackingColor = { "background-color": "#0076ff" }
        startTracking()
     },3000)
  } else {
    $scope.trackingColor = {"background-color": "red"};
  }

  $scope.updateAvailableSeat = function() {
      $scope.availableSeat = $('#availableseat').val();
      if( !parseInt($scope.availableSeat) ){
      	if( $scope.availableSeat != '0'  ){
	        ons.notification.alert("Invalid value.");
	        return;
	    }
      }

      if( $scope.availableSeat > 45 ) {
          ons.notification.alert("Reached bus max 45 seat.");
          return;
      }
      $('#availableSeat').text($('#availableseat').val());
      publishMessage( '/app/update-available-seat/'+id, parseInt($('#availableseat').val()) );
      ons.notification.alert("Successfully updated the bus available seat")
  }

  $scope.updateNextStop = function() {
    var stationIndex = getCurrentStation($('#bus-stations').val())
    console.log( stationIndex )
    updateNextStop( busStations[stationIndex] );
    ons.notification.alert("Successfully updated the bus next stop")
  }

  $scope.reverse = function(e) {
    var origin = $('#origin-form').val();
    var destination = $('#destination-form').val();
    if ( $scope.test ) {
      $scope.test = false;
    } else {
      $scope.test = true;
    }
     var temp = origin;
     origin = destination;
     destination = temp;
     $('#origin-form').val( origin );
     $('#destination-form').val( destination );
  }

  $scope.customField = function() {
        var status = $(this).val();
        if( status == 'Others' ) {
            var placeholder = status.replace('Arrived ', '');
            var form = '<label>Custom Field</label>'+
                       '<input type="text" required class="form-control" placeholder="Enter '+placeholder+'" name="statusmessage">';
            $('#custom-field').html(form);
        }
  }

  $scope.saveReverseOriginDestination = function() {
    var reverse;
    if( $scope.test ) {
      reverse = 1;
    } else {
      reverse = 0;
    }
    isReverse = reverse;
    reverseOriginDestination(reverse);
    ons.notification.alert('Successfully updated bus origin/destination')
  }

  $scope.changePassword = function() {
    var message = "At least 1 Uppercase \nAt least 1 Lowercase \nAt least 1 Number At least 1 Symbol, symbol allowed --> !@#$%^&*_=+-,\nMin 8 chars and Max 30 chars";
    var regex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,30}$");
    console.log($('#newpassword').val().match(regex))
    if( !$('#newpassword').val().match(regex) || !$('#confirmpassword').val().match(regex) ) {
          ons.notification.alert(message);
          return
    }
    if( $('#newpassword').val() == $('#confirmpassword').val()  ) {

        $.ajax({
            url: '/users/change-password/'+$('#username').val(),
            method:"POST",
            data: { currentPassword: $('#currentpassword').val(), password: $('#newpassword').val() },
            beforeSend: function(){},
            success: function( data ) {
                ons.notification.alert('Successfully changed your password');
                window.location.href = "/logout";
            },
            error: function( err ) {
                ons.notification.alert("Failed. Current password doesn't match")
            }
        });
    } else {
        ons.notification.alert('Mismatch new password confirmation');
    }
  }
  $scope.availableseatform = availableSeat;
  $scope.addSeat = function(){
      if( !$scope.availableseatform ){
          $scope.availableseatform = 1;
      } else {
          if( $scope.availableseatform < 45 ) {
            $scope.availableseatform++
          } else {
              ons.notification.alert("Reached bus max 45 seat.");
              return;
          }
      }
  }

  $scope.minusSeat = function(){
      if( $scope.availableseatform ) {
          $scope.availableseatform--
      }
  }
});

