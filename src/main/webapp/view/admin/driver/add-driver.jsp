<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<div class="modal-header login-header w3-card-2">
    <h4 class="modal-title">${page}</h4>
</div>
<div class="modal-body w3-card-2 w3-white">
    <spring:url var="actionUrl" value="/admin/driver/add-driver" />
    <form:form method="POST" action="${actionUrl}" modelAttribute="driverForm">
        <spring:bind path="companyId" >
            <form:input path="companyId" cssClass="hidden" type="number" value="${company.getCompanyId()}"/>
            <form:errors path="companyId"/>
        </spring:bind>
        <spring:bind path="driverId" >
            <form:input path="driverId"  cssClass="hidden" type="number" value="${busForm.getDriverId()}"/>
            <form:errors path="driverId"/>
        </spring:bind>
        <spring:bind path="firstName">
            <form:input path="firstName" type="text" required="required" placeHolder="Driver First Name" value="${driverForm.getFirstName()}" maxLength="20"/>
            <form:errors path="firstName"/>
        </spring:bind>
        <spring:bind path="lastName">
            <form:input path="lastName" type="text" required="required" placeHolder="Driver Last Name" value="${driverForm.getLastName()}" maxLength="20"/>
            <form:errors path="lastName" />
        </spring:bind>
        <spring:bind path="contactNo">
            <form:input path="contactNo" type="text" required="required" placeHolder="Driver Contact Number" value="${driverForm.getContactNo()}" pattern="^(09)\d{9}$" maxLength="11" title="Format <eg. 09123456789>"/>
            <form:errors path="contactNo"/>
        </spring:bind>
        <input type="submit" class="btn btn-primary btn-sm" value= '${page}' />
    </form:form>
</div>
</div>
<%@ include file="../../template/footer.jsp" %>
