<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
  <div class="row">
      <c:if test = "${not empty status}" >
          <div class="alert alert-success lead">
              ${message}
          </div>
      </c:if>
      <hr>
        <div class="form-group row">
          <div class="col-sm-2">
            <a href="/admin/driver/add-driver"><button class="btn btn-primary"><span class="fa fa-plus"></span></button></a>
          </div>
          <form action="" method="GET">
              <div class="col-sm-6">
                <input type="search" name="driverName" class="form-control col-sm-2" placeholder="Search driver by name...">
              </div>
          </form>
          <!--<div class="col-sm-2 pull-right">
            <input type="number" class="form-control col-sm-2" value="20" title="Page limit">
          </div>-->
        </div>
      <hr>
      <div class="col-sm-12">
        <c:forEach var="driver" items = "${drivers.getContents()}">
          <div class="col-sm-6 col-md-4 driver-id-${driver.getDriverId()}">
            <div class="w3-container w3-card-2" style="background: bottom no-repeat #ba0101c2 !important;padding-top:16px;margin:5px;min-height:360px">
                <div class="w3-left" style="color:white;"><i class="fa fa-user"></i> <strong>${driver.getFirstName()} ${driver.getLastName()}</strong></div>
                <div class="w3-right">
                    <ul class="list-inline pull-right">
                      <li class="dropdown">
                        <span class="dropdown-toggle" id="menu1" data-toggle="dropdown">
                        <span class="fa fa-ellipsis-v" style="color:white;"></span></span>
                        <ul class="dropdown-menu object-dropdown" role="menu" aria-labelledby="menu1">
                          <li><center>Option</center></li>
                          <li role="presentation" class="divider"></li>
                          <li role="presentation">
                              <button role="menuitem" class= "delete-domain btn btn-danger col-sm-12" data-id="${driver.getDriverId()}"  data-name="${driver.getFirstName()} ${driver.getLastName()}" data-object="driver" style="border-radius:0px;" data-toggle="modal" data-target="#delete-confirmation">
                                  <span class="fa fa-trash"></span> Delete Driver
                              </button>
                          </li>
                          <li role="presentation">
                            <a  href="/admin/driver/update-driver/${driver.getDriverId()}">
                              <button role="menuitem" class="btn btn-info col-sm-12" style="border-radius:0px;">
                                  <span class="fa fa-edit"></span> Update Driver
                              </button>
                            </a>
                          </li>
                        </ul>
                      </li>
                  </ul>
                </div>
                <br>
                <hr>
                <div class="w3-left">
                  <ul>
                    <li>
                      <span style="font-size:16px;color:white;"><h3 style="font-size:20px;" class="data-title">Mobile:</h3> ${driver.getContactNo()}</span>
                    </li>
                    <li>
                      <span style="font-size:16px;color:white;"><h3 style="font-size:20px;" class="data-title">Bus:</h3> <a href="/admin/bus/${driver.getBusAssignee().getBus().getBusId()}">${driver.getBusAssignee().getBus().getPlateNumber()}</a></span><br>
                    </li>
                    <li>
                      <span style="font-size:16px;color:white;"><h3 style="font-size:20px;" class="data-title">Conductor:</h3> ${driver.getBusAssignee().getConductor().getFirstName()} ${driver.getBusAssignee().getConductor().getLastName()}</span>
                    </li>
                  </ul>
                </div>
                <div class="w3-clear"></div>
            </div>
        </div>
        </c:forEach>
      </div>
      <hr>
      <div class="btn-group pull-right" id="pagination" style="padding-right:20px;">
          <script>
              pagination( 'pagination', ${drivers.getCurrentPage()}, ${drivers.getTotalPage()} );
          </script>
      </div>
  </div>
<%@ include file="../../template/footer.jsp" %>
