<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Login - Real Time Bus Monitoring</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="./static/css/bootstrap.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="/static/css/bootstrap.css"  rel="stylesheet"></link>
<link href="/static/css/login.css" rel="stylesheet"></link>
<link rel="stylesheet" type="text/css" href="./static/css/font-awesome.css" />
<link rel="stylesheet" href="/static/css/alertify.css">
<script src="/static/jquery/jquery-3.1.1.min.js"></script>
<script src="/static/js/notify.js"></script>
<style type="text/css">

    body {
        background: #0e1a35;
    }

    .registeration{
        border-top: 10px solid #2196f3;
        -webkit-box-shadow: 0px 5px 21px -2px rgba(0,0,0,0.47);
        -moz-box-shadow: 0px 5px 21px -2px rgba(0,0,0,0.47);
        box-shadow: 0px 5px 21px -2px rgba(0,0,0,0.47);
        margin-top: 100px;
    }

    .registerInner{
     margin: 15px;
    }



    .signbuttons{
        margin-bottom: 35px;
        background: #2196f3;
        border: none;
    }

    input{
        border-top: none !important;
        border-right: none !important;
        border-left: none !important;
        border-bottom: 1px dotted #2196f3 !important;
        box-shadow: none !important;
        -webkit-box-shadow: none !important;
        -moz-box-shadow: none !important;
        -moz-transition: none !important;
        -webkit-transition: none !important;
    }

    .headerSign{
        color: #2196f3;
        margin-bottom: 50px;
        text-align: center;
    }

    .darktext{
        color: #2196f3;
    }

    .update-nag{
      width:100%;
      display: inline-block;
      background: #e0e0e0;
      font-size: 14px;
      text-align: left;
      background-color: #fff;
      height: 60px;
      -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.2);
      box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
      margin-bottom: 10px;
    }

    .update-nag > .update-split{
      background: #337ab7;
      width: 50px;
      float: left;
      color: #fff!important;
      height: 100%;
      text-align: center;
    }

    .update-nag > .update-split > .glyphicon{
      position:relative;
      top: calc(50% - 9px)!important; /* 50% - 3/4 of icon height */
    }

    .update-nag > .update-split.update-success{
      background: #5cb85c!important;
    }

    .update-nag > .update-split.update-danger{
      background: #d9534f!important;
    }

    .update-nag > .update-split.update-info{
      background: #5bc0de!important;
    }

    .update-nag > .update-text{
      line-height: 19px;
      padding-top: 20px;
      padding-left: 60px;
    }

</style>
</head>
    <body>
    <main class="main">
      <c:url var="loginUrl" value="/login" />
      <form action="${loginUrl}" method="post" class="form-horizontal login" style="background:white;">
        <div class="login-fieldset" style="z-index:1000000;">
          <input type="text" placeholder="Enter Your Username" required name = "username" class="login-fieldset-field">
          <input type="password" placeholder="Enter Your Password" required name = "password" class="login-fieldset-field">
          <div class="row" style="padding-left:48px;">
            <div class="col-sm-2">
                <input type="submit" class=" btn btn-primary" value="Sign In"/>
            </div>
            <!--<div class="col-sm-2">
                <a href="/register"><button type="button" class=" btn btn-primary">Sign Up</button></a>
            </div>-->
          </div>
        </div>
      </form>
    </main>
        <!--<div class="row col-md-5 col-md-offset-6 registeration">
            <div class="registerInner">
                <div class ="col-md-12 ">
                    <c:if test="${param.error != null}">
                        <div class="col-md-12">
                          <div class="update-nag">
                            <div class="update-split update-danger"><i class="glyphicon glyphicon-warning-sign"></i></div>
                            <div class="update-text" > <strong>Error:</strong> </div>
                          </div>
                        </div>
                    </c:if>
                    <c:if test="${param.logout != null}">
                        <div class="col-md-12">
                          <div class="update-nag">
                            <div class="update-split update-success"><i class="glyphicon glyphicon-leaf"></i></div>
                            <div class="update-text"> <strong>Success:</strong> You have been logged out successfully! </div>
                          </div>
                        </div>
                    </c:if>
                    <h3 class="headerSign">Sign In</h3>
					<c:url var="loginUrl" value="/login" />
                    <form action="${loginUrl}" method="post" class="form-horizontal">
                        <div class="form-group">
                            <input class="form-control login-fieldset-field" required type="text" name="username" id="username" placeholder="Enter username">
                        </div>
                        <div class="form-group">
                            <input class="form-control" required type="password" name="password" id="password" placeholder="Enter Password" value="" >
                        </div>
                        <input type="submit" class="signbuttons btn btn-primary" value="Sign In"/>
                    </form>
                </div>
            </div>
        </div>-->
    </body>
    <c:if test="${param.error != null}">
      <script>
        <c:if test="${param.error == ''}">
              $.notify('Invalid username and password.');
        </c:if>
        <c:if test="${param.error != ''}">
              $.notify('${param.error}');
        </c:if>
      </script>
    </c:if>
</html>