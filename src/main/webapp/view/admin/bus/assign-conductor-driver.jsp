<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<c:if test = "${not empty status}" >
    <div class="alert alert-${status} lead">
        ${message}
    </div>
</c:if>
<div class="modal-header login-header w3-card-2">
    <h4 class="modal-title">${action} Bus Driver & Conductor assignee</h4>
</div>
<div class="modal-body w3-card-2 w3-white">
    <spring:url var="actionUrl" value="/admin/bus/assign"/>
    <form:form method="POST" action="${actionUrl}" modelAttribute="busAssigneeForm">
        <spring:bind path="assigneeId">
            <form:input path="assigneeId" cssClass="hidden" value="${busAssigneeForm.getAssigneeId()}" type="text"  placeholder = "Bus Plate Number"/>
        </spring:bind>
        <spring:bind path="bus.busId">
            <form:input path="bus.busId" cssClass="hidden" value="${busId}" type="text"  placeholder = "Bus Plate Number"/>
        </spring:bind>
        <spring:bind path="timeIn">
            <form:input path="timeIn" required="required" placeholder="Ex. 10:00:00"
                pattern="([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]" type="text"
                title="Please enter valid 24 hours time. Ex, 08:00:00"
                value = "${busAssigneeForm.getTimeIn()}" />
            <form:errors path="timeIn"/>
        </spring:bind>
        <spring:bind path="timeOut">
            <form:input path="timeOut" required="required" placeholder="Ex. 11:00:00"
                pattern="([01]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]"
                type="text"
                title="Please enter valid 24 hours time. Ex, 17:00:00"
                value="${busAssigneeForm.getTimeOut()}"
            />
            <form:errors path="timeOut"/>
        </spring:bind>
        <spring:bind path="driver.driverId">
            <form:select path="driver.driverId" required="required">
                <option value="">Select driver:</option>
                <c:if test="${busAssigneeForm.getDriver().getDriverId() != null }">
                    <option value="${busAssigneeForm.getDriver().getDriverId()}" selected>
                        ${busAssigneeForm.getDriver().getFirstName()} ${busAssigneeForm.getDriver().getLastName()}
                    </option>
                </c:if>
                <c:forEach var="driver" items="${drivers}">
                    <option value="${driver.getDriverId()}" <c:if test="${not empty busAssigneeForm.getDriver().getDriverId()}"> selected   </c:if>>${driver.getFirstName()} ${driver.getLastName()}</option>
                </c:forEach>
            </form:select>
            <form:errors path="driver.driverId"/>
        </spring:bind>
        <spring:bind path="conductor.conductorId">
            <form:select path="conductor.conductorId" required="required">
                <option value="">Select conductor:</option>
                <c:if test="${busAssigneeForm.getConductor().getConductorId() != null }">
                    <option value="${busAssigneeForm.getConductor().getConductorId()}" selected>
                        ${busAssigneeForm.getConductor().getFirstName()} ${busAssigneeForm.getConductor().getLastName()}
                    </option>
                </c:if>
                <c:forEach var="conductor" items="${conductors}">
                    <option value="${conductor.getConductorId()}" <c:if test="${not empty busAssigneeForm.getConductor().getConductorId()}"> selected </c:if>>
                        ${conductor.getFirstName()} ${conductor.getLastName()}
                    </option>
                </c:forEach>
            </form:select>
            <form:errors path="conductor.conductorId"/>
        </spring:bind>
        <input type="submit" class="btn btn-primary btn-sm" value= '${action} Bus Assignee' />
    </form:form>
</div>
</div>
<%@ include file="../../template/footer.jsp" %>
