<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<div class="row">
	<script>
		var getHoursDiff = function( date1, date2, id ){
			var delta = Math.abs(date2 - date1) / 1000;

      // calculate (and subtract) whole days
      var days = Math.floor(delta / 86400);
      delta -= days * 86400;

      // calculate (and subtract) whole hours
      var hours = Math.floor(delta / 3600) % 24;
      delta -= hours * 3600;

      // calculate (and subtract) whole minutes
      var minutes = Math.floor(delta / 60) % 60;
      delta -= minutes * 60;

      // what's left is seconds
      var seconds = delta % 60;
    	$('#mehehe-'+id).text( hours + " hrs " + minutes + " mins" )
    }
	</script>
  <div class="w3-card-4">
      <div class="w3-container w3-padding" style="background: bottom no-repeat #ba0101c2;color:white;">
        <h4>Dispatcher's Report</h4>
      </div>
      <div class="a-s--wrapper">
        <div class="form-group row">
            <form action="" method="GET" id="searchByDate">
                <div class="col-sm-6">
                      <input type="date" id="date" name="date" class="form-control col-sm-2" value="${date}">
                </div>
                <div class="col-sm-2">
                  <input type="submit" class="btn btn-primary"/>
                </div>
            </form>
        </div>
        <div id="mehehe">
          <div class="table-responsive">
            <table class="table table-striped">
              <tr>
                <th>Bus no.</th>
                <th>Departure Time</th>
                <th>Arrival Time</th>
                <th>Driver</th>
                <th>Conductor</th>
                <th>Source - Destionation</th>
                <th>Duration</th>
              </tr>
              <c:forEach var = "travellog" items= "${traveLogs.getContents()}">
                <tr>
                  <td>${travellog.bus.plateNumber}</td>
                  <td>${travellog.departureTime}</td>
                  <td>${travellog.arrivalTime}</td>
                  <td>${travellog.busAssignee.driver.firstName} ${travellog.busAssignee.driver.lastName}</td>
                  <td>${travellog.busAssignee.conductor.firstName} ${travellog.busAssignee.conductor.lastName}</td>
                  <td>
                    <c:if test = "${travellog.isSourceToDestination == 0}" >
                        ${travellog.busTrips.busTrips.source.source} -
                        ${travellog.busTrips.busTrips.destination.source} 
                    </c:if>
                    <c:if test = "${travellog.isSourceToDestination == 1}" >
                        ${travellog.busTrips.busTrips.destination.source} -
                        ${travellog.busTrips.busTrips.source.source} 
                    </c:if>
                  </td>
                  <td>
                  	<span id="mehehe-${travellog.getTravelLogId()}"></span>
                  	<script> 
                  		getHoursDiff( new Date("${travellog.departureTime}"), new Date("${travellog.arrivalTime}"), "${travellog.getTravelLogId()}"  );
                  	</script>
                  </td>
                </tr>
              </c:forEach>
            </table>
          </div>
        </div>
          <hr>
          <div class="btn-group pull-right" id="pagination" style="padding-right:20px;">
            <script>
                pagination( 'pagination', ${traveLogs.getCurrentPage()}, ${traveLogs.getTotalPage()} );

            </script>
          </div>
          <br><br>
      </div>
  </div>
</div>
</div>
<%@ include file="../../template/footer.jsp" %>
