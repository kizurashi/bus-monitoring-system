<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<div class="row">
  <!-- <hr>
    <div class="form-group row w3-padding">
      <div class="col-sm-6">
        <input type="search" class="form-control col-sm-2" placeholder="Search bus trip...">
      </div>
    </div>
  <hr> -->
  <div id="map" class="col-sm-12" style="height:700px;"></div>
  <script src="/static/js/gmap.js"></script>
  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRMdeKYLeFVApPCn2TjCbhin55dGkezaY\
    &libraries&callback=initMapsu">
  </script>
  <script>
    var subscribe = function(){
      stompClient.subscribe('/topic/geolocation/'+companyId, function (bus) {
        bus = JSON.parse(bus.body);
        console.log(bus)
        if( bus.companyId == companyId ) {
          updateMarker(bus);
        }
      });
    }

    function calculateEstimatedArrivalTime( start, end ) {
        return new Promise( resolve => {
            directionsService.route({
                 origin: start,
                 destination: end,
                 travelMode: 'DRIVING'
            }, function(response, status) {
                 if (status === 'OK') {
                     resolve( response.routes[0].legs[0].duration.text );
                 } else {
                    resolve( 'Not Available' );
                 }
            });
        })

    }
  </script>
</div>
</div>
<%@ include file="../../template/footer.jsp" %>
