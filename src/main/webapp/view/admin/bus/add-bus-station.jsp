<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<div class="row">
    <div class="col-sm-12">
        <div class="col-md-12 table-responsive" style="padding-left:0;">
            <spring:url var="actionUrl" value="/admin/bus/add-bus-station"/>
            <div class="pac-card" id="pac-card">
                <div>
                    <div id="title">
                        Bus Station
                    </div>
                </div>
                <br>
                <div id="pac-container">
                    <input id="pac-input" type="text" placeholder="Enter a location" value="">
                </div>
            </div>
            <div id="map" style="height:400px;"></div>
            <div id="infowindow-content">
                <img src="" width="16" height="16" id="place-icon">
                <span id="place-name"  class="title"></span><br>
                <span id="place-address"></span>
            </div>
        <br><br>
        <form:form modelAttribute="stationForm" method="POST" action="${actionUrl}">
            <spring:bind path="companyStationId">
                <form:input type="hidden" path="companyStationId" id="companyStationId" value="${stationForm.getCompanyStationId()}"></form:input>
            </spring:bind>
            <spring:bind path="busStation.stationId">
                <form:input type="hidden" path="busStation.stationId" id="stationId" value="${stationForm.getBusStation().getStationId()}"></form:input>
            </spring:bind>
            <spring:bind path="busStation.location.locationId">
                <form:input type="hidden" path="busStation.location.locationId" id="locationId" value="${stationForm.getBusStation().getLocation().getLocationId()}"></form:input>
            </spring:bind>
            <spring:bind path="busStation.location.lat">
                <form:input type="hidden" path="busStation.location.lat" id="lat" value="${stationForm.getBusStation().getLocation().getLat()}"></form:input>
            </spring:bind>
            <spring:bind path="busStation.location.lng">
                <form:input type="hidden" path="busStation.location.lng" id="lng" value="${stationForm.getBusStation().getLocation().getLng()}"></form:input>
            </spring:bind>
            <spring:bind path="busStation.stationName">
                <form:input type="hidden" path="busStation.stationName" id="stationName" placeholder="Enter Bus Station" value="${stationForm.getBusStation().getStationName()}"></form:input>
                <form:errors path="busStation.stationName"/>
            </spring:bind>
        </div>
        <div class="modal-footer">
            <a href="/admin/bus/bus-station"><button type="button" class="cancel" data-dismiss="modal">Cancel</button></a>
            <button type="submit" class="add-project update-domain-button">Add Bus Station</button>
        </div>
        </form:form>
        <br>
    </div>
</div>
</div>
<script>
    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 3
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);
        var geocoder = new google.maps.Geocoder();
        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions(
            {'country': ['ph']});
        var country = "Philippines";
        geocoder.geocode( {'address' : country}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
            }
        });

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            anchorPoint: new google.maps.Point(0, -29)
        });

        function geocodePosition(pos) {
            return new Promise( function( resolve ) {
              geocoder.geocode({
                latLng: pos
              }, function(responses) {
                if (responses && responses.length > 0) {
                    resolve(responses[0].formatted_address);
                } else {
                  alert('Cannot determine address at this location.');
                  resolve("")
                }
              });
            });
        }

          google.maps.event.addListener(marker, 'dragend', function() {
            var position = marker.getPosition();
            geocodePosition(position).then( address=>{
                map.panTo(position); 
                changePlace( address, address, position.lat(), position.lng());
            })
          });
          
        function changePlace(placeName, address, lat, lon){
            $('#stationName').val( placeName +', '+ address);
            infowindowContent.children['place-address'].textContent = address;
            $('#lat').val( lat );
            $('#lng').val( lon );
            input.value = address
            infowindow.setContent(address);
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);
        }

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(18);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            var lat = place.geometry.location.lat();
            var lon = place.geometry.location.lng();
            changePlace( place.name, address, lat, lon );
        });

        <c:if test="${ not empty stationForm.getBusStation().getStationName()}">
        $('#spinner').modal('show')
        google.maps.event.addListenerOnce(map, 'idle', function () {
            setTimeout(function(){
                $('#spinner').modal('hide')
                $('#pac-input').val("${stationForm.getBusStation().getStationName()}");
                if( "${stationForm.getBusStation().getStationName()}"){
                    var latLng = new google.maps.LatLng(${stationForm.getBusStation().getLocation().getLat()}, ${stationForm.getBusStation().getLocation().getLng()})
                    map.setCenter(latLng);
                    marker =  new google.maps.Marker({
                        position: latLng,
                        label: "S",
                        map: map,
                    });
                    infowindow.setContent("${stationForm.getBusStation().getStationName()}");
                    infowindow.open(map, marker);
                }
            },3000)
        });
        </c:if>
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRMdeKYLeFVApPCn2TjCbhin55dGkezaY&libraries=places&callback=initMap"
        async defer></script>
<%@ include file="../../template/modal/add-bus-station.jsp" %>
<%@ include file="../../template/footer.jsp" %>
