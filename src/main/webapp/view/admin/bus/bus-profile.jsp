<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>

<div class="row">
    <c:if test = "${not empty status}" >
        <div class="alert alert-success lead">
            ${message}
        </div>
    </c:if>
    <div class="col-sm-12">
		<div class="w3-main " style="background: #ba0101c2 !important;">
		  <div class="w3-hide-large"></div>
		  	<div class="w3-container w3-card-2" id="apartment">
		  		<div>
		  			<div class="col-sm-10">
		    			<h2 class="w3-text-green">Bus Plate #: ${bus.getPlateNumber()} </h2>
		  			</div>
		  			<div class="col-sm-2"><br>
		    			<a href="/admin/bus/update-bus/${bus.getBusId()}"><button class="btn btn-info pull-right"><span class="fa fa-edit "></span></button></a>
		  			</div>
		  		</div>
		  		<br><br>
                <hr>
	            <h4 class="w3-text-green"><strong>Basic Information</strong></h4>
	            <div class="w3-row w3-large">
	              <div class="w3-col s6">
	                <p style="color:white;"><i class="fa fa-fw fa-male" style="color:white;"></i> Source: ${bus.getBusTrips().getBusTrips().getSource().getSource()}</p>
	                <p style="color:white;"><i class="fa fa-fw fa-bath" style="color:white;"></i> Destination: ${bus.getBusTrips().getBusTrips().getDestination().getSource()}</p>
	              </div>
	              <div class="w3-col s6">
	                <p style="color:white;"><i class="fa fa-fw fa-clock-o" style="color:white;"></i> Passenger Capacity: ${bus.getPassengerCapacity()}</p>
	              </div>
	            </div>
	            <hr>

	        </div>
		</div>
    </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="w3-main" style="background: #ba0101c2;">
      <div class="w3-container w3-card-2" id="apartment">
		    <h4 class="w3-text-green" ><strong>Bus Source & Destination</strong></h4>
            <div style="padding-left:40px">
              <hr>
              <h5 style="color:white;"><strong >Source: ${bus.getBusTrips().getBusTrips().getSource().getSource()} - Destination: ${bus.getBusTrips().getBusTrips().getDestination().getSource()} ( Vice Versa )</strong></h5>
              <hr>
              <div style="padding-left:40px">
                <h5 style="color:white;"><stronBus Station(s)</strong></h5>
                <c:forEach var="busStation" items="${busStations}">
	                <div class="w3-row w3-large">
	                  <div class="w3-col s6">
	                    <p style="color:white;"> Station: </p>
	                  </div>
	                  <div class="w3-col s6">
	                    <p style="color:white;"	> ${busStation.busStation.getStationName()}</p>
	                  </div>
	                </div>
                </c:forEach>
              </div>
            </div>
      </div>
    </div>
  </div>  
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="w3-main" style="background: #ba0101c2;">
      <div class="w3-container w3-card-2" id="apartment">
          <h4 class="w3-text-green"><strong>Bus Conductor & Driver Asignees</strong></h4>
	            <div style="padding-left:40px">
              	<hr>
	            	<c:forEach var="busAssignee" items = "${bus.getBusAssignees()}" varStatus="status">
		            	<div class="col-sm-6 col-md-4 bus-assignee-id-${busAssignee.getAssigneeId()}">
				          <div class="w3-container w3-card-2 w3-white" style="padding-top:16px;margin:5px;min-height:300px;">
				              <div class="w3-left"><i class="fa fa-user"></i>  Assignee ${status.count}</div>
               				  &nbsp;<a href="/admin/bus" style="color:#23527c;" title="View more info"></a>
				              <div class="w3-right">
				                  <ul class="list-inline pull-right">
				                    <li class="dropdown">
				                      <span class="dropdown-toggle" id="menu1" data-toggle="dropdown">
				                      <span class="fa fa-ellipsis-v"></span></span>
				                      <ul class="dropdown-menu object-dropdown" role="menu" aria-labelledby="menu1">
				                        <li><center>Option</center></li>
				                        <li role="presentation" class="divider"></li>
				                        <li role="presentation">
				                            <button role="menuitem" class= "delete-domain btn btn-danger col-sm-12" data-id="${busAssignee.getAssigneeId()}" data-name="Assignee ${status.count}" data-object="bus-assignee" style="border-radius:0px;"  data-toggle="modal" data-target="#delete-confirmation">
				                              <span class="fa fa-trash"></span> Delete Assignee
				                            </button>
				                        </li>
				                        <li role="presentation">
				                            <a href="/admin/bus/update-assignee/${busAssignee.getAssigneeId()}"><button role="menuitem" class="btn btn-info col-sm-12 update-custom-source" style="border-radius:0px;"
				                            data-id=""
				                            data-target="#add-source-destination" data-toggle="modal">
				                                <span class="fa fa-edit"></span> Update Assignee
				                            </button></a>
				                        </li>
				                      </ul>
				                    </li>
				                </ul>
				              </div>
				              <br>
				              <hr>
				              <div class="w3-left">
				                <span style="font-size:18px;"><h3 style="font-size:22px;" class="data-title">Working Hrs:</h3><br>${busAssignee.getTimeIn()} - ${busAssignee.getTimeOut()}</span><br>
				                <span style="font-size:18px;"><h3 style="font-size:22px;" class="data-title">Conductor:</h3> <br>${busAssignee.getConductor().getFirstName()} ${busAssignee.getConductor().getLastName()}</span><br>
				                <span style="font-size:18px;"><h3 style="font-size:22px;" class="data-title">Driver:</h3>${busAssignee.getDriver().getFirstName()} ${busAssignee.getDriver().getLastName()}</span>
				              </div>
				              <div class="w3-clear"></div>
				          </div>
				        </div>
	              	</c:forEach>
	            </div>
      </div>
    </div>
  </div>  
</div>
</div>
<%@ include file="../../template/footer.jsp" %>
