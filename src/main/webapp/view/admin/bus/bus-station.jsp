<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<div class="row">
    <c:if test = "${not empty status}" >
        <div class="alert alert-success lead">
            ${message}
        </div>
    </c:if>
    <hr>
      <div class="form-group row">
        <div class="col-sm-2">
            <a href="/admin/bus/add-bus-station">
                <button class="btn btn-primary"><span class="fa fa-plus"></span></button>
            </a>
        </div>
        <!-- <div class="col-sm-6">
          <input type="search" class="form-control col-sm-2" placeholder="Search bus trip...">
        </div> -->
        <!--<div class="col-sm-2 pull-right">
          <input type="number" class="form-control col-sm-2" value="20" title="Page limit">
        </div>-->
      </div>
    <hr>
    <div class="col-sm-12">
      <div class="col-md-12 table-responsive" style="padding-left:0;">
        <table class="w3-table w3-striped w3-bordered w3-border w3-hoverable w3-white">
            <tr style="background: bottom no-repeat #ba0101c2 !important;color:white;">
                <th class="col-sm-1">ID</th>
                <th class="col-sm-3">Bus Station</th>
                <th class="col-sm-3"></th>
            </tr>
            <c:forEach var="station" items = "${busStations.getContents()}" varStatus="status">
              <tr class="company-bus-station-id-${station.getCompanyStationId()}">
                <td class="col-sm-1">${status.count}</td>
                <td>${station.getBusStation().getStationName()}</td>
                <td class="pull-right">
                  <a href="/admin/bus/update-bus-station/${station.getCompanyStationId()}">
                    <span class="update-domain-station" data-id="${station.getCompanyStationId()}" data-name="${station.getBusStation().getStationName()}" data-target="#add-bus-station" data-toggle="modal"><span class="fa fa-edit fw" style="color:#0046fe;"></span></span>
                  </a>
                  <span class="delete-domain" data-id="${station.getCompanyStationId()}" data-name="${station.getBusStation().getStationName()}" data-object="company/bus-station" style="border-radius:0px;" data-toggle="modal" data-target="#delete-confirmation"><span class="fa fa-trash fw" style="color:#ff0700;"></span></span>
                </td>
              </tr>
            </c:forEach>
            <tr>
                <td></td>
                <td></td>
                <td>
                    <div class="btn-group pull-right" id="pagination" style="padding-right:20px;">
                        <script>
                            pagination( 'pagination', ${busStations.getCurrentPage()}, ${busStations.getTotalPage()} );
                        </script>
                    </div>
                </td>
            </tr>
        </table>

        <br>
      </div>
    </div>
</div>
</div>
<%@ include file="../../template/footer.jsp" %>
