<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<div class="row">
    <c:if test = "${not empty status}" >
        <div class="alert alert-success lead">
            ${message}
        </div>
    </c:if>
    <hr>
      <div class="form-group row">
        <div class="col-sm-2">
          <a href="/admin/bus/add-bus">
            <button class="btn btn-primary"><span class="fa fa-plus fw"></span></button>
          </a>
        </div>
        <form action="" method="GET">
            <div class="col-sm-6">
              <input type="search" name="plateNo" class="form-control col-sm-2" placeholder="Search bus by bus number...">
            </div>
        </form>
      </div>
    <hr>
    <div class="col-sm-12">
      <c:forEach var="bus" items = "${busses.getContents()}">
        <div class="col-sm-6 col-md-4 bus-id-${bus.getBusId()}" style="">
          <div class="w3-container w3-card-2" style="background: bottom no-repeat #ba0101c2 !important;padding-top:16px;padding-bottom:150px;margin:5px;max-height: 300px;">
              <div class="w3-left"><i class="fa fa-bus" style="color:white;"> </i></div>
               &nbsp;<a href="/admin/bus/${bus.getBusId()}" style="color:white;" title="View more info">${bus.getPlateNumber()}</a>
              <div class="w3-right">
                  <ul class="list-inline pull-right">
                    <li class="dropdown">
                      <span class="dropdown-toggle"  id="menu1" data-toggle="dropdown">
                      <span class="fa fa-ellipsis-v" style="color:white;"></span></span>
                      <ul class="dropdown-menu object-dropdown" role="menu" aria-labelledby="menu1">
                        <div class="navbar-content">
                            <div><center><span>Option</span></center></div>
                            <div role="presentation" class="divider"></div>
                            <li role="presentation">
                                <button role="menuitem" class= "delete-domain btn btn-danger col-sm-12" data-id="${bus.getBusId()}" data-name="${bus.getPlateNumber()}" data-object="bus" style="border-radius:0px;"  data-toggle="modal" data-target="#delete-confirmation">
                                  <span class="fa fa-trash"></span> Delete Bus
                                </button>
                            </li>
                            <li role="presentation">
                              <a  href="/admin/bus/update-bus/${bus.getBusId()}">
                                <button role="menuitem" class="btn btn-info col-sm-12" style="border-radius:0px;">
                                    <span class="fa fa-edit"></span> Update Bus
                                </button>
                              </a>
                            </li>
                            <div role="presentation" class="divider"></div>
                            <div><center><span>Assign</span></center></div>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation">
                                <a  href="/admin/bus/assign/${bus.getBusId()}">
                                  <button role="menuitem" class="btn btn-primary col-sm-12" style="border-radius:0px;">
                                    <span class="fa fa-edit"></span> Driver & Conductor
                                  </button>
                                </a>
                            </li>
                        </div>
                      </ul>
                    </li>
                </ul>
              </div>
              <hr>
              <div class="w3-left">
                <ul>
                    <li>
                        <span style="font-size:16px;color:white;"><h3 style="font-size:18px;color:white;" class="data-title">Source:</h3> ${bus.getBusTrips().getBusTrips().getSource().getSource()}</span><br>
                    </li>
                    <li>
                        <span style="font-size:16px;color:white;"><h3 style="font-size:18px;color:white;" class="data-title">Destination:</h3> ${bus.getBusTrips().getBusTrips().getDestination().getSource()}</span>
                    </li>
                    <li>
                        <span style="font-size:16px;color:white;"><h3 style="font-size:18px;color:white;" class="data-title">Passenger Capacity:</h3> ${bus.getPassengerCapacity()}</span><br>
                    </li>
                </ul>
                <!-- <a href="/bus/${bus.getBusId()}"><button class="btn btn-info">View More info</button></a>-->
              </div>
              <div class="w3-clear"></div>
          </div>
        </div>
      </c:forEach>
    </div>
    <hr>
    <div class="btn-group pull-right" id="pagination" style="padding-right:20px;">
        <script>
            pagination( 'pagination', ${busses.getCurrentPage()}, ${busses.getTotalPage()} );
        </script>
    </div>
</div>
</div>

<%@ include file="../../template/footer.jsp" %>
