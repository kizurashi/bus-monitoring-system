<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<script>
  source = "${customSourceDestination.getBusTrips().getSource().getSource()}"
  destination = "${customSourceDestination.getBusTrips().getDestination().getSource()}"
  lat = ${customSourceDestination.getBusTrips().getSource().getLocation().getLat()};
  lng = ${customSourceDestination.getBusTrips().getSource().getLocation().getLng()}
</script>
<div class="modal-header login-header w3-card-2">
    <h4 class="modal-title">${page}</h4>
</div>
<div class="modal-body w3-card-2 w3-white">
    <spring:url var="actionUrl" value="/admin/bus/assign/bus-station"/>
    <form:form method="POST" action="${actionUrl}" modelAttribute="customSourceDestinationForm">
        <spring:bind path="companyBusTripId">
            <form:input path="companyBusTripId" cssClass="hidden" type="number" value="${customSourceDestination.getCompanyBusTripId()}"/>
            <form:errors path="companyBusTripId"/>
        </spring:bind>
        <spring:bind path="company.companyId">
            <form:input path="company.companyId" cssClass="hidden" type="number" value="${customSourceDestination.getCompany().getCompanyId()}"/>
            <form:errors path="company.companyId"/>
        </spring:bind>
        <spring:bind path="busTrips.source.sourceId">
            <form:input path="busTrips.source.sourceId" cssClass="hidden" type="number" value="${customSourceDestination.getBusTrips().getSource().getSourceId()}"/>
            <form:errors path="busTrips.source.sourceId"/>
        </spring:bind>
        <spring:bind path="busTrips.destination.sourceId">
            <form:input path="busTrips.destination.sourceId" cssClass="hidden" type="number" value="${customSourceDestination.getBusTrips().getDestination().getSourceId()}"/>
            <form:errors path="busTrips.destination.sourceId"/>
        </spring:bind>
        <spring:bind path="busTrips.busTripId">
            <form:input path="busTrips.busTripId" cssClass="hidden" type="number" value="${customSourceDestination.getBusTrips().getBusTripId()}"/>
            <form:errors path="busTrips.busTripId"/>
        </spring:bind>
        <div class="input-group">
          <select class="bus-station">
              <option value="">Select Bus Station:</option>
              <c:forEach var="busStation" items="${busStations.getContents()}" >
                  <option value="${busStation.getBusStation().getStationId()}" data-locationid="${busStation.getBusStation().getLocation().getLocationId()}" data-companyid="${busStation.getCompany().getCompanyId()}" data-companystationid="${customSourceDestination.getCompanyBusTripId()}" data-name="${busStation.getBusStation().getStationName()}" data-lat="${busStation.getBusStation().getLocation().getLat()}" data-lng="${busStation.getBusStation().getLocation().getLng()}" data-busTripId="${customSourceDestination.getCompanyBusTripId()}" >${busStation.getBusStation().getStationName()}</option>
              </c:forEach>
          </select>
          <span class="input-group-btn">
              <button type="button" class="btn btn-info" style="padding: 14px 10px;" id="assign-bus-station"><center>Assign</center></button>
          </span>
        </div>
        <table class="table table-inverse">
          <tr>
            <td>Station Priority </td>
            <td>Station Name</td>
          </tr>
          <tbody class="busStationList">
          </tbody>
        </table>
        <input type="submit" class="btn btn-primary btn-sm" value= '${page}' />
    </form:form>
    <div id="map" style="width: 100%;height:400px;"></div>
</div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRMdeKYLeFVApPCn2TjCbhin55dGkezaY&libraries=places&callback=initBusTripMap" async defer></script>
<script>
function initBusTripMap() {
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: {lat: lat, lng: lng}
  });
  directionsDisplay.setMap(map);
  google.maps.event.addListenerOnce(map, 'idle', function () {
        setTimeout(function(){
          getBusStations()
        },1000)
  });
}

function getBusStations(){
  var busTripId = ${customSourceDestination.getCompanyBusTripId()};
  if( busTripId ) {
    $.ajax({
      url: '/api/v1/company/bus-trip/'+busTripId,
      method: 'GET',
      data:{},
      success: function(data){
        $.each(data.companyBusStations, function(index, busStation){
          addBuSStationCache( ${company.getCompanyId()}, busStation.busStation.stationId, busStation.busStation.stationName, busStation.busStation.location.locationId, busStation.busStation.location.lat, busStation.busStation.location.lng, busTripId, busStation.id, busStation.priority);

          updateBusStationList();
          calculateAndDisplayRoute(directionsService, directionsDisplay);  
        });
      },
      error: function(error){

      }
    });
  }
}
function calculateAndDisplayRoute(directionsService, directionsDisplay) {
  $('#spinner').modal('show')
  var waypts = [];
  var checkboxArray = document.getElementById('waypoints');
  for (var i = 0; i < busStationCache.length; i++) {
    if (busStationCache[i]) {
      waypts.push({
        location: new google.maps.LatLng(busStationCache[i].lat,busStationCache[i].lng),
        stopover: true
      });
    }
  }
  directionsService.route({
      origin: source,
      destination: destination,
      waypoints: waypts,
      optimizeWaypoints: true,
      travelMode: 'DRIVING'
  }, function(response, status) {
      $('#spinner').modal('hide')
      directionsDisplay.setDirections(response);
  });
}
</script>
<%@ include file="../../template/footer.jsp" %>
