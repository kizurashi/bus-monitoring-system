<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>

<div class="modal-header login-header w3-card-2">
    <h4 class="modal-title">${page}</h4>
</div>
<div class="modal-body w3-card-2 w3-white">
    <spring:url var="actionUrl" value="/admin/bus/add-bus"/>
    <form:form method="POST" action="${actionUrl}" modelAttribute="busForm">
        <spring:bind path="companyId" >
            <form:input path="companyId" cssClass="hidden" type="number" value="${company.getCompanyId()}"/>
            <form:errors path="companyId"/>
        </spring:bind>
        <spring:bind path="busId" >
            <form:input path="busId" cssClass="hidden" type="number" value="${busForm.getBusId()}"/>
            <form:errors path="busId"/>
        </spring:bind>
        <spring:bind path="plateNumber" >
            <form:input path="plateNumber" required="required" type="text" pattern="[0-9]+" placeholder = "Bus No." value="${busForm.getPlateNumber()}" maxLength="4" />
            <form:errors path="plateNumber"/>
        </spring:bind>
        <spring:bind path="passengerCapacity">
            <c:if test="${empty busForm.getPassengerCapacity()}">${busForm.setPassengerCapacity(45)}</c:if>
            <form:input path="passengerCapacity" required="required" type="number" min="1" max="45" placeholder="Bus Passenger Capacity" value="${busForm.getPassengerCapacity()}"/>
            <form:errors path="passengerCapacity"/>
        </spring:bind>
        <spring:bind path="busTrips.companyBusTripId">
            <form:select path="busTrips.companyBusTripId" required="required" cssClass="bus-station">
                <option value="">Select Custom Source-Destination:</option>
                <c:forEach var="custom" items="${customs.getContents()}">
                    <option value="${custom.getCompanyBusTripId()}" <c:if test="${busForm.getBusTrips().getCompanyBusTripId() == custom.getCompanyBusTripId()}">SELECTED</c:if> data-name="${custom.getBusTrips().getSource().getSource()} - ${custom.getBusTrips().getDestination().getSource()}"> ${custom.getBusTrips().getSource().getSource()} - ${custom.getBusTrips().getDestination().getSource()}</option>
                </c:forEach>
              </form:select>
            <form:errors path="busTrips.companyBusTripId"/>
        </spring:bind>
        <input type="submit" class="btn btn-primary btn-sm" value="${page}"/>
    </form:form>
</div>
</div>
<%@ include file="../../template/footer.jsp" %>
