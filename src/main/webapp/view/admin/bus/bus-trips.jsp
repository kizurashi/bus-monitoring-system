<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<div class="row">
    <c:if test = "${not empty status}" >
        <div class="alert alert-success lead">
            ${message}
        </div>
    </c:if>
    <div class="col-sm-12" style="padding-top:16px;margin:5px;">
      <div class="col-sm-5 col-md-4" style="margin:5px;">
      </div>
    </div>
    <hr>
      <div class="form-group row">
        <div class="col-sm-2">
          <button class="btn btn-primary" data-target="#add-source-destination" data-toggle="modal"><span class="fa fa-plus"></span></button>
        </div>
        <!-- <div class="col-sm-6">
          <input type="search" class="form-control col-sm-2" placeholder="Search bus trip...">
        </div> -->
      </div>
    <hr>
    <div class="col-sm-12">
      <c:forEach var="customSourceDestination" items = "${customSourceDestinations.getContents()}">
        <div class="col-sm-6 col-md-4 company-bus-trip-id-${customSourceDestination.getCompanyBusTripId()}">
          <div class="w3-container w3-card-2" style="background: bottom no-repeat #ba0101c2 !important;padding-top:16px;margin:5px;min-height:300px;">
              <div class="w3-left" style="color:white"><i class="fa fa-bus"></i></div>
              <div class="w3-right">
                  <ul class="list-inline pull-right">
                    <li class="dropdown">
                      <span class="dropdown-toggle" id="menu1" data-toggle="dropdown">
                      <span class="fa fa-ellipsis-v" style="color:white"></span></span>
                      <ul class="dropdown-menu object-dropdown" role="menu" aria-labelledby="menu1">
                        <li><center>Option</center></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation">
                            <button role="menuitem" class= "delete-domain btn btn-danger col-sm-12" data-id="${customSourceDestination.getCompanyBusTripId()}" data-name="${customSourceDestination.getBusTrips().getSource().getSource()} - ${customSourceDestination.getBusTrips().getDestination().getSource()}" data-object="company/bus-trip" style="border-radius:0px;"  data-toggle="modal" data-target="#delete-confirmation">
                              <span class="fa fa-trash"></span> Delete Bus
                            </button>
                        </li>
                        <li role="presentation">
                            <button role="menuitem" class="btn btn-info col-sm-12 update-custom-source" style="border-radius:0px;"
                            data-id="${customSourceDestination.getCompanyBusTripId()}"
                            data-source="${customSourceDestination.getBusTrips().getSource().getSourceId()}"
                            data-destination="${customSourceDestination.getBusTrips().getDestination().getSourceId()}"
                            data-target="#add-source-destination" data-toggle="modal">
                                <span class="fa fa-edit"></span> Update Bus
                            </button>
                        </li>
                        <li role="presentation" class="divider"></li>
                        <li><center>Assign</center></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation">
                            <a  href="/admin/bus/assign/bus-station/${customSourceDestination.getCompanyBusTripId()}">
                              <button role="menuitem" class="btn btn-primary col-sm-12" style="border-radius:0px;">
                                <span class="fa fa-edit"></span> Bus Station
                              </button>
                            </a>
                        </li>
                      </ul>
                    </li>
                </ul>
              </div>
              <br>
              <hr>
              <div class="w3-left">
                <span style="font-size:16px;color:white"><h4 style="font-size:20px;" class="data-title">Source:</h4> ${customSourceDestination.getBusTrips().getSource().getSource()}</span><br>
                <span style="font-size:16px;color:white"><h4 style="font-size:20px;" class="data-title">Destination:</h4> ${customSourceDestination.getBusTrips().getDestination().getSource()}</span>
              </div>
              <div class="w3-clear"></div>
          </div>
        </div>
      </c:forEach>
    </div>
    <hr>
    <div class="btn-group pull-right" id="pagination" style="padding-right:20px;">
        <script>
            pagination( 'pagination', ${customSourceDestinations.getCurrentPage()}, ${customSourceDestinations.getTotalPage()} );
        </script>
    </div>
</div>
</div>

<%@ include file="../../template/modal/add-custom-source-destination.jsp" %>
<%@ include file="../../template/footer.jsp" %>
