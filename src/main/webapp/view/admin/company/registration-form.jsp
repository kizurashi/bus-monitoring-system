<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>

<spring:url var="actionUrl" value="/company/add" />
<form:form method="POST" action="${actionUrl}" modelAttribute="companyForm">
    <spring:bind path="companyName">
        <form:input path="companyName" type="text" placeHolder="Hello"/>
        <form:errors path="companyName"/>
    </spring:bind>
</form:form>

<%@ include file="../template/footer.jsp" %>