<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>

<div class="modal-header login-header w3-card-2">
    <h4 class="modal-title">${page}</h4>
</div>
<div class="modal-body w3-card-2 w3-white">
    <spring:url var="actionUrl" value="/admin/company/update-profile"/>
    <form:form method="POST" action="${actionUrl}" modelAttribute="companyForm">
        <spring:bind path="companyId" >
            <form:input path="companyId" cssClass="hidden" type="number" value="${company.getCompanyId()}"/>
            <form:errors path="companyId"/>
        </spring:bind>
        <spring:bind path="user.id" >
            <form:input path="user.id" cssClass="hidden" type="number" value="${company.getUser().getId()}"/>
            <form:errors path="user.id"/>
        </spring:bind>
        <spring:bind path="companyName" >
            <form:input path="companyName" type="text" placeholder="Company Name" value="${company.getCompanyName()}" maxLength="60"/>
            <form:errors path="companyName"/>
        </spring:bind>
        <spring:bind path="contactNo" >
            <form:input path="contactNo" required="required" type="text" placeholder = "Company Contact No." value="${company.getContactNo()}" pattern="[0][0-9]{11}" maxLength="11" title="Format <eg. 09123456789>"/>
            <form:errors path="contactNo"/>
        </spring:bind>
        <spring:bind path="emailAddress">
            <form:input path="emailAddress" required="required" type="email" placeholder="Company Email Addresss" value="${company.getEmailAddress()}"/>
            <form:errors path="emailAddress"/>
        </spring:bind>
        <input type="submit" class="btn btn-primary btn-sm"
            value="${page}"/>
    </form:form>
</div>
</div>
<%@ include file="../../template/footer.jsp" %>
