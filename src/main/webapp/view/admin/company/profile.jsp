<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<div class="modal-header login-header w3-card-2">
    <div>
        <div class="col-sm-10">
            <h4 class="modal-title">Company: ${company.getCompanyName()} </h4>
        </div>
        <div class="col-sm-2">
            <a href="/admin/company/update-profile"><button class="btn btn-info pull-right"><span class="fa fa-edit "></span></button></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
		<div class="w3-main w3-white">
		  <div class="w3-hide-large"></div>
		  	<div class="w3-container w3-card-2" id="apartment">
	            <h4 class="w3-text-green"><strong>Basic Information</strong></h4><br>
	            <div class="w3-row w3-large">
	              <div class="w3-col s6">
	                <p><i class="fa fa-fw fa-male"></i> User: ${company.getUser().getUsername()}</p>
	                <p><i class="fa fa-fw"></i> Mobile: ${company.getContactNo()}</p>
	              </div>
                  <div class="w3-col s6">
                    <p><i class="fa fa-fw"></i> Email Address: ${company.getEmailAddress()}</p>
                  </div>
	            </div>
	            <hr>

	        </div>
		</div>
    </div>
</div>
</div>
<%@ include file="../../template/footer.jsp" %>
