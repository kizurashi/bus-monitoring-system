<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<c:if test = "${not empty status}" >
    <div class="alert alert-${status} lead">
        ${message}
    </div>
</c:if>
<div class="modal-header login-header w3-card-2">
    <h4 class="modal-title">${page}</h4>
</div>
<div class="modal-body w3-card-2 w3-white">

    <spring:url var="actionUrl" value="/admin/conductor/add-conductor" />
    <form:form method="POST" action="${actionUrl}" modelAttribute="conductorForm">
        <spring:bind path="companyId" >
            <form:input path="companyId" cssClass="hidden" type="number" value="${company.getCompanyId()}" />
            <form:errors path="companyId"/>
        </spring:bind>
        <c:if test="${conductorForm.getUser().getId() != null }">
        <spring:bind path="user.username" >
            <form:input path="user.username"  cssClass="hidden" type="hidden" value="${conductorForm.getUser().getUsername()}" maxLength="60"/>
        </spring:bind>
        </c:if>
        <spring:bind path="conductorId" >
            <form:input path="conductorId"  cssClass="hidden"  type="number" value="${conductorForm.getConductorId()}" />
            <form:errors path="conductorId"/>
        </spring:bind>
        <c:if test="${conductorForm.getUser().getId() == null }">
        <spring:bind path="user.username">
            Username:
            <form:input path="user.username" type="text" required="required" placeHolder="Conductor User name" value="${conductorForm.getUser().getUsername()}" maxLength="20"/>
            <form:errors path="user.username"/>
        </spring:bind>
        </c:if>
        <spring:bind path="firstName">
            First Name
            <form:input path="firstName" type="text" required="required" placeHolder="Conductor First Name" value="${conductorForm.getFirstName()}" maxLength="20"/>
            <form:errors path="firstName"/>
        </spring:bind>
        <spring:bind path="lastName">
            Last Name:
            <form:input path="lastName" type="text" required="required" placeHolder="Conductor Last Name" value="${conductorForm.getLastName()}" maxLength="20"/>
            <form:errors path="lastName"/>
        </spring:bind>
        <spring:bind path="contactNo">
            Contact Number:
            <form:input path="contactNo" type="text" required="required" placeHolder="Conductor Contact Number"  maxlength="10" pattern="^(09)\d{9}$" title="Format <eg. 09123456789>" maxLength="11" value="${conductorForm.getContactNo()}"/>
            <form:errors path="contactNo"/>
        </spring:bind>
        <input type="submit" class="btn btn-primary btn-sm" value ='${page}'/>
    </form:form>
</div>
</div>
<%@ include file="../../template/footer.jsp" %>
