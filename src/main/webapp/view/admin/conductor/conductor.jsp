<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<div class="row">
    <c:if test = "${not empty status}" >
        <div class="alert alert-success lead">
            ${message}
        </div>
    </c:if>
    <div class="col-sm-12" style="padding-top:16px;margin:5px;">
      <div class="col-sm-5 col-md-4" style="margin:5px;">
      </div>
    </div>
    <hr>
      <div class="form-group row">
        <div class="col-sm-2">
            <a href="/admin/conductor/add-conductor"><button class = "btn btn-primary"><span class="fa fa-plus"></span></button></a>
        </div>
        <form action="" method="GET">
            <div class="col-sm-6">
              <input type="search" name="conductorName" class="form-control col-sm-2" placeholder="Search conductor by name...">
            </div>
        </form>
        <!--<div class="col-sm-2 pull-right">
          <input type="number" class="form-control col-sm-2" value="20" title="Page limit">
        </div>-->
      </div>
    <hr>
    <div class="col-sm-12">
      <c:forEach var="conductor" items = "${conductors.getContents()}">
        <div class="col-sm-6 col-md-4 conductor-id-${conductor.getConductorId()}">
          <div class="w3-container w3-card-2" style="background: bottom no-repeat #ba0101c2 !important;padding-top:16px;margin:5px;min-height:360px;">
              <div class="w3-left" style="color:white;"><i class="fa fa-user"></i> <strong>${conductor.getFirstName()} ${conductor.getLastName()}</strong></div>
              <div class="w3-right">
                  <ul class="list-inline pull-right">
                    <li class="dropdown">
                      <span class="dropdown-toggle" id="menu1" data-toggle="dropdown">
                      <span class="fa fa-ellipsis-v" style="color:white;"></span></span>
                      <ul class="dropdown-menu object-dropdown" role="menu" aria-labelledby="menu1">
                        <li><center>Option</center></li>
                        <li role="presentation" class="divider"></li>
                        <li role="presentation">
                            <button role="menuitem" class= "delete-domain btn btn-danger col-sm-12" data-id="${conductor.getConductorId()}" data-name="${conductor.getFirstName()} ${conductor.getLastName()}" data-object="conductor" style="border-radius:0px;"  data-toggle="modal" data-target="#delete-confirmation">
                              <span class="fa fa-trash"></span> Delete Conductor
                            </button>
                        </li>
                        <li role="presentation">
                          <a  href="/admin/conductor/update-conductor/${conductor.getConductorId()}">
                            <button role="menuitem" class="btn btn-info col-sm-12" style="border-radius:0px;">
                                <span class="fa fa-edit"></span> Update Conductor
                            </button>
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
              </div>
              <br>
              <hr>
              <div class="w3-left">
                <ul>
                  <li>
                    <span style="color:white;"><strong style="font-size:20px;color:white;" class="data-title">Username:</strong> ${conductor.getUser().getUsername()}</span>
                  </li>
                  <li>
                    <span style="font-size:16px;color:white;"><strong style="font-size:20px;color:white;" class="data-title">Mobile:</strong> ${conductor.getContactNo()}</span>
                  </li>
                  <c:if test="${conductor.getBusAssignee() != null}">
                  <li>
                    <span style="font-size:16px;color:white;"><strong style="font-size:20px;color:white;" class="data-title">Working Hrs: </strong>${conductor.getBusAssignee().getTimeIn()} - ${conductor.getBusAssignee().getTimeOut()}</span>
                  </li>
                  </c:if>
                  <li>
                    <span style="font-size:16px;color:white;"><strong style="font-size:20px;color:white;" class="data-title">Bus:</strong> <a href="/admin/bus/${driver.getBusAssignee().getBus().getBusId()}">${conductor.getBusAssignee().getBus().getPlateNumber()}</a></span>
                  </li>
                  <li>
                      <span style="font-size:16px;color:white;"><strong style="font-size:20px;color:white;" class="data-title">Driver:</strong> ${conductor.getBusAssignee().getDriver().getFirstName()} ${conductor.getBusAssignee().getDriver().getLastName()}</span>
                  </li>
                </ul>
            </div>

              <div class="w3-clear"></div>
          </div>
        </div>
    
      </c:forEach>
    </div>
      <hr>
      <div class="btn-group pull-right" id="pagination" style="padding-right:20px;">
          <script>
              pagination( 'pagination', ${conductors.getCurrentPage()}, ${conductors.getTotalPage()} );
          </script>
      </div>
    </div>
</div>
<%@ include file="../../template/footer.jsp" %>
