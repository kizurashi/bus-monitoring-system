<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login - Real Time Bus Monitoring</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="./static/css/bootstrap.min.css" rel="stylesheet">
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href="/static/css/bootstrap.css"  rel="stylesheet"></link>
	<link href="/static/css/app.css" rel="stylesheet"></link>
	<link rel="stylesheet" type="text/css" href="./static/css/font-awesome.css" />
    <style type="text/css">

        body {
            background: #0e1a35;
        }

        *{
            border-radius: 0 !important;
        }

        html,body{

        }

        .registeration{
            border-top: 10px solid #2196f3;
            -webkit-box-shadow: 0px 5px 21px -2px rgba(0,0,0,0.47);
            -moz-box-shadow: 0px 5px 21px -2px rgba(0,0,0,0.47);
            box-shadow: 0px 5px 21px -2px rgba(0,0,0,0.47);
            margin-top: 100px;
        }

        .registerInner{
         margin: 15px;
        }

        .form-group{
            width: 100%;
            line-height: 50px;
        }

        .signbuttons{
            margin-bottom: 35px;
            background: #2196f3;
            border: none;
        }

        input{
            border-top: none !important;
            border-right: none !important;
            border-left: none !important;
            border-bottom: 1px dotted #2196f3 !important;
            box-shadow: none !important;
            -webkit-box-shadow: none !important;
            -moz-box-shadow: none !important;
            -moz-transition: none !important;
            -webkit-transition: none !important;
        }

        .headerSign{
            color: #2196f3;
            margin-bottom: 50px;
            text-align: center;
        }

        .darktext{
            color: #2196f3;
        }

        div{
            background:white;
        }

        .update-nag{
          width:100%;
          display: inline-block;
          background: #e0e0e0;
          font-size: 14px;
          text-align: left;
          background-color: #fff;
          height: 60px;
          -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.2);
          box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
          margin-bottom: 10px;
        }

        .update-nag > .update-split{
          background: #337ab7;
          width: 50px;
          float: left;
          color: #fff!important;
          height: 100%;
          text-align: center;
        }

        .update-nag > .update-split > .glyphicon{
          position:relative;
          top: calc(50% - 9px)!important; /* 50% - 3/4 of icon height */
        }

        .update-nag > .update-split.update-success{
          background: #5cb85c!important;
        }

        .update-nag > .update-split.update-danger{
          background: #d9534f!important;
        }

        .update-nag > .update-split.update-info{
          background: #5bc0de!important;
        }

        .update-nag > .update-text{
          line-height: 19px;
          padding-top: 20px;
          padding-left: 60px;
        }

    </style>
</head>
    <body>
        <div class="row col-md-5 col-md-offset-6 registeration">
            <div class="registerInner">
                <div class ="col-md-12 ">
                    <c:if test="${param.error != null}">
                        <div class="col-md-12">
                          <div class="update-nag">
                            <div class="update-split update-danger"><i class="glyphicon glyphicon-warning-sign"></i></div>
                            <div class="update-text" > <strong>Error:</strong> Invalid username and password.</div>
                          </div>
                        </div>
                    </c:if>
                    <c:if test="${param.logout != null}">
                        <div class="col-md-12">
                          <div class="update-nag">
                            <div class="update-split update-success"><i class="glyphicon glyphicon-leaf"></i></div>
                            <div class="update-text"> <strong>Success:</strong> You have been logged out successfully! </div>
                          </div>
                        </div>
                    </c:if>
                    <h3 class="headerSign">Sign Up</h3>
					<c:url var="register" value="/register" />
                    <form:form action="${register}" method="post" modelAttribute="companyForm" class="form-horizontal">
                        <div class="form-group">
                            <spring:bind path="user.username" >
                                <form:input path="user.username" cssClass="form-control" type="text" placeholder="Enter username"/>
                                <form:errors path="user.username"/>
                            </spring:bind>
                        </div>
                         <div class="form-group">
                            <spring:bind path="user.password" >
                                <form:input path="user.password" cssClass="form-control" type="password" placeholder="Enter password"/>
                                <form:errors path="user.password"/>
                            </spring:bind>
                        </div>
                        <div class="form-group">
                            <spring:bind path="user.firstName" >
                                <form:input path="user.firstName" cssClass="form-control" type="text" placeholder="Enter username"/>
                                <form:errors path="user.firstName"/>
                            </spring:bind>
                        </div>
                         <div class="form-group">
                            <spring:bind path="user.lastName" >
                                <form:input path="user.lastName" cssClass="form-control" type="password" placeholder="Enter password"/>
                                <form:errors path="user.lastName"/>
                            </spring:bind>
                        </div>
                        <div class="form-group">
                            <spring:bind path="companyName" >
                                <form:input path="companyName" cssClass="form-control" type="text" placeholder="Enter company name"/>
                                <form:errors path="companyName"/>
                            </spring:bind>
                        </div>
                        <div class="form-group">
                            <spring:bind path="contactNo" >
                                <form:input path="contactNo" cssClass="form-control" type="number" placeholder="Enter contact no."/>
                                <form:errors path="contactNo"/>
                            </spring:bind>
                        </div>
                        <div class="form-group">
                            <spring:bind path="emailAddress" >
                                <form:input path="emailAddress"  class="form-control" cssClass="form-control" type="text" name="email" id="email" placeholder="Enter email"/>
                                <form:errors path="emailAddress"/>
                            </spring:bind>
                        </div>
                        <!--<div class="form-group">
                            <input class="form-control" type="text" name="password" id="password" placeholder="Enter Address" value="">
                        </div>-->
                        <button type="submit" class="signbuttons btn btn-primary">Sign Up</button>
                        <a href="/login"><button type="button" class="signbuttons btn btn-primary">Sign In</button></a>
                    </form:form>
                </div>
            </div>
        </div>
    </body>
</html>
