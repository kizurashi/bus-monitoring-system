<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../template/header.jsp" %>
        <div class="row padding-bottom">
          <div class="col-sm-4">
            <div class="w3-container w3-blue w3-padding-16">
                <div class="w3-left"><i class="fa fa-user w3-xxxlarge"></i></div>
                <div class="w3-right">
                  <h3>${driverCount}</h3>
                </div>
                <div class="w3-clear"></div>
                <h4><a href="/admin/driver" title="View all">Driver(s)</a></h4>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="w3-container w3-red w3-padding-16">
                <div class="w3-left"><i class="fa fa-user w3-xxxlarge"></i></div>
                <div class="w3-right">
                  <h3>${conductorCount}</h3>
                </div>
                <div class="w3-clear"></div>
                <h4><a href="/admin/conductor" title="View all">Conductor(s)</a></h4>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="w3-container w3-teal w3-padding-16">
              <div class="w3-left"><i class="fa fa-bus w3-xxxlarge"></i></div>
              <div class="w3-right">
                <h3>${busCount}</h3>
              </div>
              <div class="w3-clear"></div>
              <h4><a href="/admin/bus" title="View all">Busses</a></h4>
            </div>
          </div>
        </div>
          <div class="row padding-bottom">
            <div class="">
              <div class="w3-card-4">
                <div class="w3-container w3-padding" style="background: bottom no-repeat #ba0101c2 !important;color:white;">
                  <h4>Bus Activity Stream(s)<span class="pull-right"><a href="/admin/activity/${company.getCompanyId()}">View All</a></span></h4>
                </div>
                <hr>
                <div class="a-s--wrapper" id="mehehe">
                  <div class="a-s--group-by-date">
                      <c:forEach var = "activityByDate" items="${activities.getContents()}">
                        <div class="he-who-must-not-be-named">
                          <div class="a-s--title current-head" data-date="${activityByDate.getKey()}">
                            <h3>${activityByDate.getKey()}</h3>
                          </div>
                          <div class="activity-streams activity-streams-${activityByDate.getKey()}">
                            <c:forEach var = "activity" items= "${activityByDate.getValue()}">
                                <div class="a-s--item">
                                  <div class="a-s--icon">
                                  </div>
                                  <div class="a-s--body">
                                      <h4>Bus No: ${activity.getBus().getPlateNumber()}</h4>
                                      <h5>Message: ${activity.getMessage()}</h5>
                                      <p>Posted: <strong>${activity.getTimestamp().toString().split(' ')[1].substring(0, activity.getTimestamp().toString().split(' ')[1].length()-2)} | </strong>Status: <strong>${activity.getAction()} | </strong>Location: <strong>${activity.getLocation().getLocationName()}</strong></p>
                                  </div>
                                </div>
                            </c:forEach>
                          </div>
                        </div>
                      </c:forEach>
                  </div>
                </div>
            </div>
            </div>
          </div>
        </div>
<%@ include file="../template/footer.jsp" %>


