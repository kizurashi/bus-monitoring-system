<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ include file="../../template/header.jsp" %>
<div class="row">
  <div class="w3-card-4">
      <div class="w3-container w3-padding" style="background: bottom no-repeat #ba0101c2;color:white;">
        <h4>Bus Activity Stream(s)</h4>
      </div>
      <div class="a-s--wrapper">
        <div class="form-group row">
            <form action="" method="GET" id="searchByDate">
                <div class="col-sm-6">
                      <input type="date" id="date" name="date" class="form-control col-sm-2" value="${date}">
                </div>
                <div class="col-sm-2">
                  <input type="submit" class="btn btn-primary"/>
                </div>
            </form>
        </div>
        <div id="mehehe">
          <c:forEach var = "activityByDate" items="${activities.getContents()}">
          <div class="a-s--group-by-date">
              <div class="he-who-must-not-be-named">
              <div class="a-s--title current-head" data-date="${activityByDate.getKey()}">
                <h3>${activityByDate.getKey()}</h3>
              </div>
              <div class="activity-streams activity-streams-${activityByDate.getKey()}">
                <c:forEach var = "activity" items= "${activityByDate.getValue()}">
                    <div class="a-s--item">
                      <div class="a-s--icon">
                      </div>
                      <div class="a-s--body">
                          <h4>Bus No: ${activity.getBus().getPlateNumber()}</h4>
                          <h5>Message: ${activity.getMessage()}</h5>
                          <p>Posted: <strong>${activity.getTimestamp().toString().split(' ')[1].substring(0, activity.getTimestamp().toString().split(' ')[1].length()-2)} | </strong>Status: <strong>${activity.getAction()} | </strong>Location: <strong>${activity.getLocation().getLocationName()}</strong></p>
                      </div>
                    </div>
                </c:forEach>
              </div>
              </div>
          </div>
          </c:forEach>
        </div>
          <hr>
          <div class="btn-group pull-right" id="pagination" style="padding-right:20px;">
            <script>
                pagination( 'pagination', ${activities.getCurrentPage()}, ${activities.getTotalPage()} );
            </script>
          </div>
          <br><br>
      </div>
  </div>
</div>
</div>
<%@ include file="../../template/footer.jsp" %>
