<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

            </div>
    </div>
</div>
<div id="spinner" class="modal fade" role="dialog" style="margin-top:200px;" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" style="color:white">
      <center>
          <span class="fa fa-spinner fa-pulse fa-fw fa-5x"  ></span>
          <br><br>
          <span class="src-only">Loading...</span>
      </center>
  </div>
</div>
<%@ include file="./modal/delete-confirmation.jsp" %>
<%@ include file="./modal/change-password.jsp" %>
<script>
    var dateFormatter = function( date ) {
        var d = new Date( date )
        var month = ''+(d.getMonth() + 1)
        var year = d.getFullYear()
        var day = ''+d.getDate();
        if( month.length < 2 )
            month = '0'+month;
        if( day.length < 2 )
            day = '0'+day;
        return year+"-"+month+"-"+day;
    };

    $(document).ready(function(){
        var subscriber = function(){
            stompClient.subscribe('/topic/admin/listener', function (activity) {
                var activity = JSON.parse(activity.body);
                var receivedDate =  new Date( activity.timestamp );
                var formattedDate = dateFormatter(receivedDate);
                var formattedTime = receivedDate.getHours() +":"+receivedDate.getMinutes()+":"+receivedDate.getSeconds();
                var currentHead = $('.current-head:nth-of-type(1)');
                var currentDate = currentHead.data('date');
                var newActivityItem = '<div class="a-s--item">' +
                                      '<div class="a-s--icon">' +
                                      '</div>' +
                                      '<div class="a-s--body">' +
                                          '<h4>Bus No: '+ activity.bus.plateNumber +'</h4>' +
                                          '<h5>'+ activity.message +'</h5>' +
                                          '<p>Posted: <strong>'+ formattedTime +' | </strong>Status: <strong>'+ activity.action +' | </strong>Location: <strong>'+ activity.location.locationName +'</strong></p>' +
                                      '</div>' +
                                    '</div>';
                var countActivityStreams = document.querySelectorAll( '.activity-streams .a-s--item').length;
                var status = '';
                if( activity.action == 'Traffic' ||  activity.action == 'Accident' ) {
                    status = '<strong><center>Warning</center></strong><br>';
                }

                $.notify("Bus: "+activity.bus.plateNumber+" \n Status: "+activity.action+" \n Message:" +activity.message, "success")
                var count = 0;
                if( "${page}" != "" ) {
                    $.each(  document.querySelectorAll( '.a-s--group-by-date .he-who-must-not-be-named' ), function( index, value ) {
                         if( value.querySelectorAll( '.activity-streams .a-s--item' ).length == 1 ) {
                            value.remove();
                         }
                         $.each( value.querySelectorAll( '.activity-streams .a-s--item' ), function( i, v ) {
                            if( count >= 9 ) {
                                v.remove();
                            }
                            count ++;
                         });
                    });
                }
                var currentStream = $('.activity-streams-'+formattedDate).html();
                if ( currentDate == formattedDate ) {
                    $('.activity-streams-'+formattedDate).html( newActivityItem + currentStream );
                } else {
                    var streamWrapper = $('#mehehe').html();
                    var newItem = '<div class="a-s--group-by-date">'+
                                      '<div class="a-s--title current-head" data-date="'+formattedDate+'">'+
                                        '<h3>'+formattedDate+'</h3>'+
                                      '</div>'+
                                      '<div class="activity-streams">'+
                                        newActivityItem +
                                      '</div>'+
                                 '</div>';
                    $('.a-s--wrapper').html( newItem + streamWrapper );
                }
            });
            if( typeof(subscribe) != 'undefined') {
                subscribe();
            }
        }
        connect(subscriber);
    });
  </script>
</body>
</html>