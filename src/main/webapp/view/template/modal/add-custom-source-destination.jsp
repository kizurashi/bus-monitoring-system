<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div id="add-source-destination" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Bus Source and Destination</h4>
            </div>
            <div class="modal-body">
            <spring:url var="actionUrl" value="/admin/bus/add-company-bus-trip"/>
            <form:form modelAttribute="busTripForm" method="POST" action="${actionUrl}">
                <spring:bind path="companyBusTripId">
                    <form:input type="hidden" id="companyBusTripId" path="companyBusTripId" value="${busTripForm.getCompanyBusTripId()}"></form:input>
                </spring:bind>
                <spring:bind path="busTrips.source.sourceId">
                    <form:select path="busTrips.source.sourceId" id="sourceId" required="required">
                        <option value="">Select Source:</option>
                        <c:forEach var="source" items="${companyBusSource.getContents()}">
                            <option value="${source.getSourceDestination().getSourceId()}">${source.getSourceDestination().getSource()}</option>
                        </c:forEach>
                    </form:select>
                </spring:bind>
                <spring:bind path="busTrips.destination.sourceId">
                    <form:select path="busTrips.destination.sourceId" id="destinationId" required="required">
                        <option value="">Select Destination:</option>
                        <c:forEach var="source" items="${companyBusSource.getContents()}">
                            <option value="${source.getSourceDestination().getSourceId()}">${source.getSourceDestination().getSource()}</option>
                        </c:forEach>
                    </form:select>
                </spring:bind>
                </div>
                <div class="modal-footer">
                    <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="add-project">Yes, Proceed!</button>
                </div>
            </form:form>
        </div>
    </div>
</div>
