<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<sec:authentication property="principal" var="user"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getUserDetails("${user.getUsername()}")' var="userDetails"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getCompanyDetails("${userDetails.getId()}")' var="company"/>
<div id="change-password" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title change-me-please">Change Password: User, ${user.getUsername()}</h4>
            </div>
            <form>
            <div class="modal-body" id="change-password">
                <input type="password" required id="currentpassword" placeholder="Enter Current Password" ></input>
                <input type="password" required id="newpassword" placeholder="Enter New Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,30}$" title="At least 1 Uppercase At least 1 Lowercase At least 1 Number At least 1 Symbol, symbol allowed --> !@#$%^&*_=+-, Min 8 chars and Max 30 chars"></input>
                <input type="password" required id="confirmpassword" placeholder="Enter Confirm New Password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,30}$" title="At least 1 Uppercase At least 1 Lowercase At least 1 Number At least 1 Symbol, symbol allowed --> !@#$%^&*_=+-, Min 8 chars and Max 30 chars"></input>
            </div>
                <div class="modal-footer">
                    <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="add-project update-domain-button" value="Save New Password"/>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#change-password').on( 'submit', function( e ) {
            e.preventDefault();
            if( $('#newpassword').val() == $('#confirmpassword').val()  ) {
                $.ajax({
                    url: '/users/change-password/${user.getUsername()}',
                    method:"POST",
                    data: { currentPassword: $('#currentpassword').val(), password: $('#newpassword').val() },
                    beforeSend: function(){},
                    success: function( data ) {
                        $.notify("You successfully changed your password!", "success")
                        window.location.href = "/logout";
                    },
                    error: function( err ) {
                        $.notify("Failed to changed your password!", "danger")
                    }
                });
            } else {
                $.notify('Mismatch current password!','danger');
            }
        });
    });
</script>
