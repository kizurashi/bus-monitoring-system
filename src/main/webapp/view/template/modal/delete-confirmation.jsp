<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div id="delete-confirmation" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete <span class="delete-object"></span> - <span class="name"></span></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="object"/>
                <input type="hidden" id="id"/>
                <input type="hidden" id="object-name"/>
                <span id="message">Are you sure you want to proceed to delete <span class="delete-object"></span> - <strong><span class="name"> </span></strong>?</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="add-project" id="delete-domain-proceed" data-dismiss="modal">Yes, Proceed!</button>
            </div>
        </div>
    </div>
</div>
