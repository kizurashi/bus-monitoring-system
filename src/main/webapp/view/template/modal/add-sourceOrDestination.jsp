<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div id="add-source-or-destination" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title change-me-please">Add Bus Source or Destination</h4>
            </div>
            <div class="modal-body">
            <spring:url var="actionUrl" value="/admin/bus/add-bus-source"/>
            <form:form modelAttribute="sourceForm" method="POST" action="${actionUrl}">
                <spring:bind path="company.companyId">
                    <form:input type="hidden" path="company.companyId" value="${company.getCompanyId()}"></form:input>
                </spring:bind>
                <spring:bind path="sourceDestinationId">
                    <form:input type="hidden" id="sourceDestinationId" path="sourceDestinationId" value=""></form:input>
                </spring:bind>
                <spring:bind path="sourceOrDestination">
                    <form:input type="text" path="sourceOrDestination" id="sourceOrDestination" placeholder="Enter Bus Source or Destination"></form:input>
                    <form:errors path="sourceOrDestination"/>
                </spring:bind>
                </div>
                <div class="modal-footer">
                    <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="add-project update-domain-button">Add Bus Source or Destination</button>
                </div>
            </form:form>
        </div>
    </div>
</div>
