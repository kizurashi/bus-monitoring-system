<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication property="principal" var="user"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getUserDetails("${user.getUsername()}")' var="userDetails"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getCompanyDetails("${userDetails.getId()}")' var="company"/>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Real Time Bus Monitoring</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/static/css/bootstrap.min.css">
  <link rel="stylesheet" href="/static/css/font-awesome.css">
  <link rel="stylesheet" href="/static/css/w3.css">
  <link rel="stylesheet" href="/static/css/app.css">
  <link rel="stylesheet" href="/static/css/google-map.css">
  <link rel="stylesheet" href="/static/css/card.css">
  <link rel="stylesheet" href="/static/css/activity-stream.css">
  <script src="/static/jquery/jquery-3.1.1.min.js"></script>
  <script src="/static/js/notify.js"></script>
  <script src="/static/js/bootstrap.min.js"></script>
  <script src="/static/js/monitoring.js"></script>
  <script src="/static/js/sockjs/sockjs.min.js"></script>
  <script src="/static/js/stomp/stomp.min.js"></script>
  <script src="/static/js/websocket.js"></script>
  <script>
    var companyId = "${company.getCompanyId()}";
    var baseUrl = '/api/v1';
  </script>
  <style>
  .display-table-cell .v-align {
    min-height: 100%;
    height: 100%;
    box-sizing: border-box;
  }
  .footer {
    padding: 1rem;
    background-color: #efefef;
    text-align: center;
  }
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 550px}

    strong{
        font-family: "Segoe UI",Arial,sans-serif;
        font-weight: 400;
    }

    body{
      background:#f1f1f1!important;
    }
    .object-dropdown {
        left: -200px;
    }
    .col-sm-4, .col-sm-12, .col-sm-5, .col-sm-6 {
       padding-left:0px !important;
       padding-right:0px !important;
    }
    .padding-bottom {
        padding-bottom:10px !important;
    }

    .row {
        padding-bottom:10px !important;
        margin: 0px !important;
    }
    .object-dropdown>li>a {
        padding:0px;
    }
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }

    .dropdown-menu {
      min-width: 220px; 
    }

    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 767px) {
      .row.content {height: auto;}
    }
    html, body{
        height: 100%;
    }

    .data-title {
      display: inline-block;
    }

    .object-dropdown .col-sm-12 {
      width:100%;
    }

    hr {
        border-top: 1px solid white;
    }
  </style>

  <script>
    var page = "${page}".replace( " ", "-" ).toLowerCase();
    var previousPage = "";
    var activePage = ".page-"+page;
    $(document).ready(function(){
      $(".page-"+page).addClass( "w3-blue");
    });
  </script>
</head>
<body>
<div class="container-fluid display-table">
    <div class="row display-table-row">
        <div class="col-md-2 col-sm-1 hidden-xs display-table-cell v-align box" id="navigation">
            <nav class=" w3-collapse <!-- w3-animate-left -->" style="<!--z-index:3;width:300px;-->"><br>
              <div class="w3-container w3-row">
                <div class="w3-col s12 w3-bar" style="color:white;">
                  <spring:eval expression="T(com.project.monitor.permission.service.AuthenticatedUserImpl).getCompanyDetails( 33 )" var="company"/>
                  <spring:eval expression="T(com.project.monitor.permission.service.AuthenticatedUserImpl).getUserDetails( 'JC' )" var="user"/>
                  <span>${company.getCompanyName()}</span><br>
                </div>
              </div>
              <hr>
              <div class="w3-bar-block navi">
                <a href="/admin/dashboard" class="page-dashboard w3-bar-item w3-padding"><i class="fa fa-home fa-fw"></i>Dashboard</a>
                <a href="/admin/company/profile" class="page-company-profile w3-bar-item w3-padding"><i class="fa fa-user fa-fw"></i>Company Profile</a>
                <a data-toggle="collapse" href="#dropdown-lvl1" class="w3-bar-item w3-padding">
                  <i class="fa fa-user fa-fw"></i>Bus Menu <span class="caret"></span>
                </a>
                <div id="dropdown-lvl1" class="panel-collapse collapse">
                  <div class="panel-body">
                    <a href="/admin/bus" class="page-bus w3-bar-item w3-padding"><i class="fa fa-bus fa-fw"></i>Bus </a>
                    <a href="/admin/bus/bus-station" class="page-bus-station w3-bar-item w3-padding"><i class="fa fa-bus fa-fw"></i>Bus Station </a>
                    <a href="/admin/bus/bus-source-or-destination" class="page-bus-source w3-bar-item w3-padding"><i class="fa fa-bus fa-fw"></i>Bus Source </a>
                    <a href="/admin/bus/bus-trips" class="page-bus-source w3-bar-item w3-padding"><i class="fa fa-bus fa-fw"></i>Bus Trips </a>
                  </div>
                </div>
                <a href="/admin/driver" class="page-driver w3-bar-item w3-padding"><i class="fa fa-user fa-fw"></i>Driver</a>
                <a href="/admin/conductor" class="page-conductor w3-bar-item w3-padding"><i class="fa fa-user fa-fw"></i>Conductor</a>
                <a href="/admin/bus/bus-locator" class="page-bus-locator w3-bar-item w3-padding"><i class="fa fa-compass fa-fw"></i>Bus Locator</a>
                <a href="/admin/activity/${company.getCompanyId()}" class="page-activity w3-bar-item w3-padding"><i class="fa fa-cog fa-fw"></i>Activity Stream</a>
                <a href="/admin/bus/travel/logs" class="page-activity w3-bar-item w3-padding"><i class="fa fa-cog fa-fw"></i>Dispatcher's Report</a>
              </div>
            </nav>
        </div>
        <div class="display-table-cell v-align">
          <div class="row">
              <header class="w3-card-2 w3-white">
                  <div class="col-md-7">
                      <nav class="navbar-default pull-left">
                          <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed" data-toggle="offcanvas" data-target="#side-menu" aria-expanded="false">
                                  <span class="sr-only">Toggle navigation</span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                              </button>
                          </div>
                      </nav>
                  </div>
                  <div class="col-md-5">
                      <div class="header-rightside">
                          <ul class="list-inline header-top pull-right">
                              <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Image" class="w3-left w3-margin-right" style="width:30px">
                                    <span class="fa fa-ellipsis-v"></span>
                                  </a>
                                  <ul class="dropdown-menu">
                                      <li>
                                          <div class="navbar-content">
                                              <span>Welcome, ${user.getUsername()}</span>
                                              <p class="text-muted small">
                                                  ${user.getEmailAdd()}
                                              </p>
                                              <div class="divider"></div>
                                              <a data-toggle="modal" data-target="#change-password" class="view btn-sm active">Change Password</a>
                                              <div class="divider"></div>
                                              <a href="/logout" class="view btn-sm active">Log out</a>
                                          </div>
                                      </li>
                                  </ul>
                              </li>
                          </ul>
                      </div>
                  </div>
              </header>
          </div>
          <div class="col-sm-12" style="padding:0;">
            <div style="padding-right: 10px;padding-left: 10px;">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="background: bottom no-repeat #ba0101c2;">
                    <li></li>
                    <li class="active" style="color:white;"><a href="">${page}</a></li>
                </ol>
              </nav>