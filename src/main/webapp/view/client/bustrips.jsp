<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Real Time Bus Monitoring</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/static/css/bootstrap.min.css">
  <link rel="stylesheet" href="/static/css/font-awesome.css">
  <link rel="stylesheet" href="/static/css/w3.css">
  <link rel="stylesheet" href="/static/css/app.css">
  <link rel="stylesheet" href="/static/css/google-map.css">
  <link rel="stylesheet" href="/static/css/card.css">
  <link rel="stylesheet" href="/static/css/activity-stream.css">
  <link rel="stylesheet" href="/static/css/alertify.css">

    <script>
    var companyId = "${company.getCompanyId()}";
    var baseUrl = 'http://localhost:8090/api/v1';
   </script>
  <script src="/static/jquery/jquery-3.1.1.min.js"></script>
  <script src="/static/js/bootstrap.min.js"></script>
  <script src="/static/js/monitoring.js"></script>
  <script src="/static/js/sockjs/sockjs.min.js"></script>
  <script src="/static/js/stomp/stomp.min.js"></script>
  <script src="/static/js/websocket.js"></script>
  <script src="/static/js/alertify.js"></script>

  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 550px}

    body{
        background:#f1f1f1!important;
    }
    .object-dropdown {
        left: -140px;
    }
    .col-sm-4, .col-sm-12, .col-sm-5, .col-sm-6 {
       padding-left:0px !important;
       padding-right:0px !important;
       padding-bottom:0px !important;
    }
    .padding-bottom {
        padding-bottom:10px !important;
    }

    .row {
        padding-bottom:10px !important;
        margin: 0px !important;
    }
    .object-dropdown>li>a {
        padding:0px;
    }
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }

    .dropdown-menu {
      min-width: 220px;
    }

    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 767px) {
      .row.content {height: auto;}
    }
    html, body{
        height: 100%;
    }

    .data-title {
      display: inline-block;
    }

    .object-dropdown .col-sm-12 {
      width:100%;
    }

    .jumborton{
      background: #ba0101c2;
    }

  </style>
</head>
  <body>
    <div class="container-fluid display-table">
      <div class="row display-table-row">
        <div class="display-table-cell v-align">
          <div class="col-sm-12" style="padding:0;">
            <ul class="breadcrumb" style="background:white">
                <li><a href="http://localhost:8090/bus">/</a></li>
                <li class="active" style="color:#337ab7;"><a href="http://localhost:8090/bus">Tracking Bus</a></li>
            </ul>
            <div style="padding-right: 10px;padding-left: 10px;">
              <div class="row">
                  <div class="col-sm-12" style="padding-top:16px;">
                    <div class="col-sm-3" >
                      <select class="form-group">
                        <option>Bus Company</option>
                        <option>Philippine Rabit Bus Corp</option>

                      </select>
                    </div>
                    <div class="col-sm-2" >
                      <select class="form-group">
                        <option>Bus Type</option>
                        <option>Air Conditioned</option>
                      </select>
                    </div>
                    <div class="col-sm-2" >
                      <select class="form-group">
                        <option>Bus Trips</option>
                        <option>Tarlac - Cubao</option>
                      </select>
                    </div>
                    <div class="col-sm-2" >
                      <select class="form-group">
                        <option>Bus Station</option>
                        <option>Cubao</option>
                        <option>Dau</option>
                        <option>Siesta</option>
                      </select>
                    </div>
                    <div class="col-sm-3" >
                      <input class="form-group btn btn-primary" type="submit" value="Search"/>
                      <input class="form-group btn btn-primary" type="submit" value="Clear"/>
                    </div>
                  </div>
                  <div class="col-sm-12">
                      <div class="col-sm-6 col-md-4 bus-id-31">
                        <div class="w3-container w3-card-2" style="background: bottom no-repeat #ba0101c2 !important;padding-top:16px;padding-bottom:150px;margin:5px;min-height: 300px;">
                            <div class="w3-left" style="color:white"><i class="fa fa-bus"> </i></div>
                             &nbsp;<a href="http://localhost:8090/bus/31" style="color:white" title="View more info">jskdlf</a>
                            <div class="w3-right">
                                <ul class="list-inline pull-right">
                                  <li class="dropdown">
                                    <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu object-dropdown" role="menu" aria-labelledby="menu1">
                                      <li><center>Option</center></li>
                                      <li role="presentation" class="divider"></li>
                                      <li role="presentation">
                                          <button role="menuitem" class="delete-domain btn btn-danger col-sm-12" data-id="31" data-name="jskdlf" data-object="bus" style="border-radius:0px;" data-toggle="modal" data-target="#delete-confirmation">
                                            <span class="fa fa-trash"></span> Delete Bus
                                          </button>
                                      </li>
                                      <li role="presentation">
                                        <a href="http://localhost:8090/bus/update-bus/31">
                                          <button role="menuitem" class="btn btn-info col-sm-12" style="border-radius:0px;">
                                              <span class="fa fa-edit"></span> Update Bus
                                          </button>
                                        </a>
                                      </li>
                                      <li role="presentation" class="divider"></li>
                                      <li><center>Assign</center></li>
                                      <li role="presentation" class="divider"></li>
                                      <li role="presentation">
                                          <a href="http://localhost:8090/bus/assign/31">
                                            <button role="menuitem" class="btn btn-primary col-sm-12" style="border-radius:0px;">
                                              <span class="fa fa-edit"></span> Driver &amp; Conductor
                                            </button>
                                          </a>
                                      </li>
                                    </ul>
                                  </li>
                              </ul>
                            </div>
                            <hr>
                            <div class="w3-left">
                              <ul>
                                  <li>
                                      <span style="font-size:16px;"><h3 style="font-size:18px;" class="data-title">Source:</h3> Tarlac</span><br>
                                  </li>
                                  <li>
                                      <span style="font-size:16px;"><h3 style="font-size:18px;" class="data-title">Destination:</h3> Cubao</span>
                                  </li>
                                  <li>
                                      <span style="font-size:16px;"><h3 style="font-size:18px;" class="data-title">Passenger Capacity:</h3> 40</span><br>
                                  </li>
                                  <li>
                                      <span style="font-size:16px;"><h3 style="font-size:18px;" class="data-title">Bus Type:</h3> Ordinary</span><br>
                                  </li>
                              </ul>
                              <!-- <a href="/bus/31"><button class="btn btn-info">View More info</button></a>-->
                            </div>
                            <div class="w3-clear"></div>
                        </div>
                      </div>
                  </div>
              </div>
            </div>
            <div class="w3-black w3-center w3-padding-24">Powered by <a href="https://www.w3schools.com/w3css/default.asp" title="W3.CSS" target="_blank" class="w3-hover-opacity">w3.css</a></div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>