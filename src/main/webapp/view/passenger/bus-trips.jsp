<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Real Time Bus Monitoring</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/static/css/bootstrap.min.css">
  <link rel="stylesheet" href="/static/css/font-awesome.css">
  <link rel="stylesheet" href="/static/css/w3.css">
  <link rel="stylesheet" href="/static/css/app.css">
  <link rel="stylesheet" href="/static/css/google-map.css">
  <link rel="stylesheet" href="/static/css/card.css">
  <link rel="stylesheet" href="/static/css/activity-stream.css">
  <link rel="stylesheet" href="/static/css/alertify.css">
  <script src="/static/jquery/jquery-3.1.1.min.js"></script>
  <script src="/static/js/bootstrap.min.js"></script>
  <script src="/static/js/monitoring.js"></script>
  <script src="/static/js/sockjs/sockjs.min.js"></script>
  <script src="/static/js/stomp/stomp.min.js"></script>
  <script src="/static/js/websocket.js"></script>
  <script src="/static/js/notify.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 550px}

    * {
        letter-spacing: 1px;
    }

    .text {
        font-size: 36px;
        color: #fff;
        font-weight: normal;
        margin: 0 auto;
        line-height: 1;
    }

    .object-dropdown {
        left: -140px;
    }
    .col-sm-4, .col-sm-12, .col-sm-5, .col-sm-6 {
       padding-left:0px !important;
       padding-right:0px !important;
    }

    .padding-bottom {
        padding-bottom:10px !important;
    }

    .row {
        padding-bottom:10px !important;
        margin: 0px !important;
    }
    .object-dropdown>li>a {
        padding:0px;
    }
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }

    .dropdown-menu {
      min-width: 220px;
    }

    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 767px) {
      .row.content {height: auto;}
    }
    html, body{
        height: 100%;
    }

    .data-title {
      display: inline-block;
    }

    .object-dropdown .col-sm-12 {
      width:100%;
    }

    .jumbotron{
      background: #ba0101c2 !important;
    }
  </style>
</head>
  <body>
    <div class="container-fluid display-table">
      <div class="row display-table-row">
        <div class="display-table-cell v-align">
          <div class="col-sm-12" style="padding:0;">
            <div class="jumbotron" style="border-radius:0px;">
            	<img src="/static/rabbitlogo.jpg" height="50" width="100%">
            </div>
            <div style="padding-right: 10px;padding-left: 10px;">
              <div class="row">
                  <form id="search-form">   
                  <div class="col-sm-12" style="padding-top:16px;">
                    <div class="col-sm-3" >
                      <select class="form-group" id="companyId">
                        <option value = "">Bus Company</option>
                        <c:forEach var="company" items="${companies}">
                          <option value="${company.getCompanyId()}">${company.getCompanyName()}</option>
                        </c:forEach>
                      </select>
                    </div>
                    <!--<div class="col-sm-2" >
                      <select class="form-group" id="busTypeId">
                        <option value = "">Bus Type</option>
                        <option value = "1">Air Conditioned</option>
                      </select>
                    </div>-->
                    <div class="col-sm-2" >
                      <select class="form-group" id="busTripId">
                        <option value = "">Bus Trips</option>
                        <c:forEach var="busTrip" items="${bustrips.getContents()}">
                          <option value="${busTrip.getBusTrips().getBusTripId()}">${busTrip.getBusTrips().getSource().getSource()} - ${busTrip.getBusTrips().getDestination().getSource()}</option>
                        </c:forEach>
                      </select>
                    </div>
                    <div class="col-sm-2" >
                      <select class="form-group" id="busStationId" required>
                        <option value = "" >Bus Station</option>
                        <c:forEach var="busStation" items="${busStations.getContents()}">
                          <option value = "${busStation.getBusStation().getStationId()}" data-name="${busStation.getBusStation().getStationName()}">${busStation.getBusStation().getStationName()}</option>
                        </c:forEach>
                      </select>
                    </div>
                    <div class="col-sm-3" >
                      <input class="form-group btn btn-primary" type="submit" value="Search"/>
                      <a href="">
                      <input class="form-group btn btn-warning" type="button" value="Clear"/>
                      </a>
                    </div>
                    </form>
                  </div>
                  <div class="col-sm-12 search-result">
                    <div class="text" style="padding-top:100px;padding-bottom:100px;color:white;">
                       <center></center>
                    </div>
                  </div>
              </div>
            </div>
            <div class="w3-black w3-center w3-padding-24" style="position:absolute;bottom:0;"></div>
          </div>
        </div>
      </div>
    </div>
<div id="agreement" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header login-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Agreement</h4>
            </div>
            <div class="modal-body">
                <form action="" id="iagree">
                <input type="checkbox" style="width:30px;float:left" required>&nbsp;Check here to indicate that you have
                read and agree to our <a href="/privacy" style="color:blue;">Privacy Policy</a>
                <input type="submit" value="submit" class="btn btn-primary"/>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="spinner" class="modal fade" role="dialog" style="margin-top:200px;" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" style="color:white">
      <center>
          <span class="fa fa-spinner fa-pulse fa-fw fa-5x"  ></span>
          <br><br>
          <span class="src-only">Loading...</span>
      </center>
  </div>
</div>
<script>
  var directionsService;

  var setCookie = function(name, value, expireInMinute){
    if( !expireInMinute ) {
        expireInMinute = 5;
    }
    var date = new Date();
    expireInMinute = date.setTime( date.getTime() + (expireInMinute*60*1000) )
    document.cookie=name+"="+value;
  }

  var getCookie = function( cname ) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
  }


  if( !getCookie("iagree") ){
    $('#agreement').modal('show');
  }

  $('#iagree').on('submit',function(e){
    setCookie('iagree', 'mehehe', 86000);
  })

  function initMap() {
    directionsService = new google.maps.DirectionsService;
  }

  function calculateEstimatedArrivalTime( start, end ) {
    return new Promise( resolve => {
        directionsService.route({
             origin: start,
             destination: end,
             travelMode: 'DRIVING'
        }, function(response, status) {
             if (status === 'OK') {
                 resolve( response.routes[0].legs[0].duration.text );
             } else {
                 window.alert('Directions request failed due to ' + status);
             }
        });
    });
  }

  $(document).ready(function() {
    $('#search-form').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: '/api/bus-search',
            method: 'GET',
            data: { companyId: $('#companyId').val(), busTripId: $('#busTripId').val(), busTypeId: $('#busTypeId').val(), busStationId: $('#busStationId').val() },
            beforeSend: function() {
                $('#spinner').modal('show');
            },
            success: function( busses ) {
                var entry="";
                if ( busses.length == 0 ) {
                    entry = '<div class="text" style="padding-top:100px;padding-bottom:100px;color:black;">'+
                               '<center>No result found!</center>'+
                            '</div>';
                    $('.search-result').html( entry );
                    $('#spinner').modal('hide');
                }
                for( var ctr = 0; ctr < busses.length; ctr ++ ) {
                    var value = busses[ctr];
                    var availableSeat = value.availableSeat ? value.availableSeat : 0;
                    if( value.nextBusStop && value.nextBusStop.location || value.busTrips != null ) {
                      var action = 'Not Available', locationName = 'Not Available';
          
                      entry += '<div class="col-sm-6 col-md-4 bus-id-31">'+
                                '<div class="w3-container w3-card-2" style="background: bottom no-repeat #ba0101c2;padding-top:16px;margin:5px;min-height: 350px;">'+
                                    '<div class="w3-left" style="color:white"><i class="fa fa-bus"></i></div>'+
                                     '&nbsp;<span style="color:white">'+value.plateNumber.toUpperCase()+
                                    '</span><div class="w3-right">'+
                                  '</div>'+
                                    '<hr>'+
                                    '<div class="w3-left">'+
                                      '<ul>'+
                                          '<li>'+
                                              '<span style="font-size:16px;color:white;"><h3 style="font-size:18px;" class="data-title">Company:</h3> '+value.busTrips.company.companyName+'</span><br>'+
                                          '</li>'+
                                          '<li>'+
                                              '<span style="font-size:16px;color:white;"><h3 style="font-size:18px;" class="data-title">Bus Trip:</h3> '+value.busTrips.busTrips.source.source+' - '+value.busTrips.busTrips.destination.source+'</span><br>'+
                                          '</li>'+
                                          // '<li>'+
                                          //     '<span style="font-size:16px;"><h3 style="font-size:18px;" class="data-title">Bus Type:</h3> Ordinary</span><br>'+
                                          // '</li>'+
                                          '<li>'+
                                              '<span style="font-size:16px;color:white;"><h3 style="font-size:18px;" class="data-title">Passenger Capacity:</h3> '+value.passengerCapacity+'</span><br>'+
                                          '</li>'+
                                          '<li>'+
                                              '<span style="font-size:16px;color:white;"><h3 style="font-size:18px;" class="data-title">Available Seat:</h3> <span id="bus-availableseat-'+value.plateNumber+'">'+availableSeat+'</span></span><br>'+
                                          '</li>'+
                                          '<li>'+
                                            '<span style="font-size:16px;color:white;"><h3 style="font-size:18px;" class="data-title">Speed:</h3><span id="bus-'+value.plateNumber+'">Not Available</span></span><br>'+
                                          '</li>'+getLocation(value.lastActivity)+
                                          '<li>'+
                                              '<span  style="font-size:16px;color:white;"><h3 style="font-size:18px;" class="data-title" >Estimated Arrival Time:</h3> <span id="bus-eta-'+value.plateNumber+'">Not Available</span></span><br>'+
                                          '</li>'+
                                      '</ul>'+
                                    '</div>'+getLocationDate(value.lastActivity)+
                                '</div>'+
                            '</div>';
                    }
                    if( ctr+1 == (busses.length) ){
                      if( entry == '' ){
                        entry = '<div class="text" style="padding-top:100px;padding-bottom:100px;color:black;">'+
                                  '<center>No result found!</center>'+
                                '</div>'
                       }
                      $('.search-result').html( entry );
                      $('#spinner').modal('hide');
                    } 
                }
            },
            error: function( error ) {
            }
        });
    });
  });

  var getLocation = function( lastActivity ) {
    if( lastActivity ) {
      var receivedDate =  dateFormatter( new Date( lastActivity.timestamp ) );
      var currentDate = dateFormatter( new Date() );
      if( receivedDate == currentDate ){
          action = lastActivity.action
          locationName = lastActivity.location.locationName
          return '<li>'+
                  '<span style="font-size:16px;color:white;"><h3 style="font-size:18px;" class="data-title">Status:</h3> <span class="label label-info">'+action+'</span></span><br><br>'+
                '</li>';
      }
    }
    return '';
  }

  var dateFormatter = function( date ) {
      var d = new Date( date )
      var month = ''+(d.getMonth() + 1)
      var year = d.getFullYear()
      var day = ''+d.getDate();
      if( month.length < 2 )
          month = '0'+month;
      if( day.length < 2 )
          day = '0'+day;
      return year+"-"+month+"-"+day;
  };

  var getLocationDate = function( lastActivity ) {
    if( lastActivity ) {
      var receivedDate =  dateFormatter( new Date( lastActivity.timestamp ) );
      var currentDate = dateFormatter( new Date() );
      if( receivedDate == currentDate ){
        return '<div class="w3-clear"><span class="label label-info" class="data-title">'+new Date(lastActivity.location.timestamp).toDateString()+' '+new Date(lastActivity.location.timestamp).toLocaleTimeString()+'</span><br><br> </div>';
      }
    }
    return ''
  }

  var plateNo, busId, slotNo;
  var subscribe = function(){
    stompClient.subscribe('/topic/geolocation/'+14, function (bus) {
      bus = JSON.parse(bus.body);
      if( bus.companyId == 14 ) {
        $('#bus-'+bus.plateNumber ).text(bus.currentGeoLocation.speed)
        var end = bus.nextBusStop.location.lat +','+bus.nextBusStop.location.lng;
        var eta = 'Not Available';
        var start = bus.currentGeoLocation.lat +','+bus.currentGeoLocation.lng;
        var estimatedTime = calculateEstimatedArrivalTime( start, end );
        estimatedTime.then(function(e){
          $('#bus-eta-'+bus.plateNumber).text(e)
        })
      }
    });

    stompClient.subscribe('/topic/availableSeat/listener', function (bus) {
      bus = JSON.parse(bus.body);
      if( bus.companyId == 14 ) {
        console.log('jaklsdf');
        $('#bus-availableseat-'+bus.plateNumber ).text(bus.availableSeat)
      }
    });
  }
  connect( subscribe );
</script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRMdeKYLeFVApPCn2TjCbhin55dGkezaY&callback=initMap">
    </script>
    <div id="reserve-seat-slot" class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header login-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reserved Bus Passenger Slot <span class="plateNo"></span></h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="busId"/>
                    <input type="text" id="busStation" required readonly/>
                    <input type="number" id="slotNo" required placeholder="Enter number of slot you want reserve..."/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                    <button type="button" class="add-project" id="reserve-form" data-dismiss="modal">Yes, Proceed!</button>
                </div>
            </div>
        </div>
    </div>

    <div id="reserve-seat-slot-confirmation" class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header login-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Reserved Bus Passenger Slot Confirmation</span></h4>
                </div>
                <div class="modal-body">
                    <span id="message">Are you sure you want to proceed to reserve <span id="slot"></span> to bus <strong><span class="plateNo"> </span></strong>?</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="cancel" data-dismiss="modal">Cancel</button>
                    <button type="button" class="add-project" id="reserve-now" data-dismiss="modal">Yes, Proceed!</button>
                </div>
            </div>
        </div>
    </div>
  </body>
</html>