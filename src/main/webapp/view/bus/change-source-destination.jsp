<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<spring:eval expression="T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails(31)" var="conductor"/>
<div class="modal changesourcedestination" ui-if="changesourcedestination" ui-shared-state="changesourcedestination">
  <div class="modal-backdrop in"></div>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" 
                ui-turn-off="changesourcedestination">&times;</button>
        <h4 class="modal-title">Change bus origin & source</h4>
      </div>
      <form id="reverse-destination">
          <div class="modal-body">
            <div class="form-group has-success has-feedback">
              <label>Origin</label>
              <input type="text" class="form-control" id="origin-form" readonly value="${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSource()}"/>
            </div>
            <div class="form-group has-success has-feedback">
              <label>Destination</label>
              <input type="text" class="form-control" id="destination-form" readonly value="${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getSource()}"/>
              <input type="hidden" id="reverse" name="travelToDestination" readonly value="${conductor.getBusAssignee().getBus().getTravelToDestination()}"/>
              <input type="hidden" class="form-control" name="busId" readonly value="${conductor.getBusAssignee().getBus().getBusId()}"/>
            </div>
            <div class="form-group">
              <label>Reverse origin & destination</label>
              <ui-switch ng-model="rememberMe" id="reverse" data-reverse="false"></ui-switch>
            </div>
          </div>
          <div class="modal-footer">
            <button ui-turn-off="changesourcedestination" class="btn btn-default">Close</button>
            <button type="submit" class="btn btn-primary" >Save changes</button>
          </div>
      </form>
    </div>
  </div>
</div>