<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<spring:eval expression="T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails(31)" var="conductor"/>
<div class="modal" ui-if="changepassword" ui-shared-state="changepassword">
  <div class="modal-backdrop in"></div>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button class="close" ui-turn-off="changepassword">&times;</button>
        <h4 class="modal-title">Change Password, JCJC</h4>
      </div>
      <form id="changepassword">
          <div class="modal-body">
            <div class="form-group has-success has-feedback">
              <label>Current Password</label>
              <input type="password" class="form-control" id="currentpassword"/>
            </div>
            <div class="form-group has-success has-feedback">
              <label>New Password</label>
              <input type="password" class="form-control" id="newpassword" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,30}$" title="At least 1 Uppercase At least 1 Lowercase At least 1 Number At least 1 Symbol, symbol allowed --> !@#$%^&*_=+-, Min 8 chars and Max 30 chars"/>
            </div>
            <div class="form-group has-success has-feedback">
              <label>Confirm New Password</label>
              <input type="password" class="form-control" id="confirmpassword" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,30}$" title="At least 1 Uppercase At least 1 Lowercase At least 1 Number At least 1 Symbol, symbol allowed --> !@#$%^&*_=+-, Min 8 chars and Max 30 chars"/>
              <input type="hidden" class="form-control" name="conductorId" readonly value="${conductor.getConductorId()}"/>
            </div>
          </div>
          <div class="modal-footer">
            <button ui-turn-off="changepassword" class="btn btn-default">Close</button>
            <input type="submit" class="btn btn-primary" value="Save changes"/>
          </div>
      </form>
    </div>
  </div>
</div>