<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<spring:eval expression="T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails(31)" var="conductor"/>
<html lang="en" ng-app="myApp" ng-controller="MainController">
<head>
    <meta charset="UTF-8">
    <title>Bus Monitoring System - Conductor Module</title>
<!--     <link rel="stylesheet" href="/static/css/onsenui.min.css">
    <link rel="stylesheet" href="/static/css/onsen-css-components.min.css">
    <link rel="stylesheet" href="/static/css/font-awesome.css">
    <script src="/static/js/angular.min.js"></script>
    <script src="/static/js/angular-route.min.js"></script>
    <script src="/static/js/onsenui.min.js"></script>
    <script src="/static/js/angular-onsenui.min.js"></script>
    <script src="/static/jquery/jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" href="/static/css/activity-stream.css">
    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <script src="/static/js/sockjs/sockjs.min.js"></script>
    <script src="/static/js/stomp/stomp.min.js"></script>
    <script src="/static/js/websocket.js"></script> -->
    <style>
         body{font-size: 17px !important;
         }
    </style>
    <script>
        var id = "${conductor.getBusAssignee().getBus().getBusId()}";
        var baseUrl = "http://localhost:8090/api/v1"
        var busStations = [];
    </script>
</head>

<body >
    <table border="1">
  <tr>
    <td>
      <div id="map" style="height: 600px; width:500px;"></div>
    </td>
    <td>
      <div id="side_bar"></div>
    </td>
  </tr>
</table>
    <div id="map-canvas" style="width:100%;height:500px;"></div>
<div id="info"></div>

<!-- 

<ons-navigator id="myNavigator" page="splitter.html">
    <div id="map-canvas" style="width:100%;height:500px;"></div>
</ons-navigator>
<template id="splitter.html">
    <ons-page>
        <ons-splitter id="mySplitter">
            <ons-splitter-side id="sidemenu" page="view/bus/html/menu.jsp" swipeable width="250px" collapse swipe-target-width="50px">
            </ons-splitter-side>
            <ons-splitter-content page="tabbar.html">
            </ons-splitter-content>
        </ons-splitter>
    </ons-page>
</template>

<template id="tabbar.html">
    <ons-page id="tabbarPage">
        <ons-toolbar>
            <div class="left">
                <ons-toolbar-button component="button/menu">
                    <ons-icon icon="ion-navicon, material:md-menu" size="32px, material:24px"></ons-icon>
                </ons-toolbar-button>
            </div>
            <div class="center" ng-model="title">Bus Monitoring System</div>
            <div class="right" ng-model="title">
                <ons-toolbar-button id="info-button" component="button/menu" ng-click="showDialog('popover-dialog')" style="margin-right:10px;">
                    <span class="fa fa-ellipsis-v" size="32px, material:24px"></span>
                </ons-toolbar-button>
            </div>
        </ons-toolbar>
        <ons-if>
            <ons-fab position="right bottom" component="button/new-task">
                <ons-icon icon="md-edit"></ons-icon>
            </ons-fab>
        </ons-if>

        <ons-tabbar swipeable id="appTabbar" position="auto">

            <ons-tab title="Animations" page="/view/bus/html/home.jsp" active></ons-tab>
              <ons-tab title="Animations" page="/view/bus/html/profile.jsp"></ons-tab>
              <ons-tab title="Animations" page="/bus-client/log-activities" ></ons-tab>
              <ons-tab title="Animations" page="/view/bus/html/reserve-seat.jsp"></ons-tab>
        </ons-tabbar>
        <script>
              ons.getScriptPage().addEventListener('prechange', function(event) {
                if (event.target.matches('#appTabbar')) {
                  event.currentTarget.querySelector('ons-toolbar .center').innerHTML = event.tabItem.getAttribute('title');
                }
              });
        </script>

        <c:forEach var="station" items="${conductor.getBusAssignee().getBus().getBusTrips().getCompanyBusStations()}">
            <script>
                busStations.push({ "station":"${station.getStationName()}", "lat":"${station.getLocation().getLat()}", "lng":"${station.getLocation().getLng()}"})
            </script>
        </c:forEach>
        <div id="map-canvas" style="width:100%;height:300px;"></div>
        <script>

        </script>
    </ons-page>
</template>

<ons-fab position="bottom center" class="tracking" ng-style="trackingColor" ng-click="enableTracking()">
    <ons-icon icon="md-power"></ons-icon>
</ons-fab>

<ons-popover id="popover-dialog" cancelable direction="down" cover-target target="#info-button">
  <ons-list id="popover-list">
    <ons-list-item class="more-options" tappable ng-click="hideDialog('popover-dialog', 'origin')">
      <div class="center">Reverse Origin/Destination</div>
    </ons-list-item>
    <ons-list-item class="more-options" tappable ng-click="hideDialog('popover-dialog', 'nextStop')">
      <div class="center">Update Next Bus Stop</div>
    </ons-list-item>
    <ons-list-item class="more-options" tappable ng-click="hideDialog('popover-dialog', 'availSeat')">
      <div class="center">Update Available Seat</div>
    </ons-list-item>
    <ons-list-item class="more-options" tappable ng-click="hideDialog('popover-dialog', 'password')">
      <div class="center">Change Password</div>
    </ons-list-item>
    <ons-list-item class="more-options" tappable ng-click="hideDialog('popover-dialog', '')">
      <div class="center">Logout</div>
    </ons-list-item>
  </ons-list>
</ons-popover> -->
<input id="address" type="text" value="Siesta Terminal"></input>
<input type="button" value="Search" onclick="codeAddress();"></input>

<!-- <script src="/static/js/app.js"></script>
<script src="/static/js/controllers.js"></script>
<script src="/static/js/mobile.js"></script> -->
<script>
    //connect();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRMdeKYLeFVApPCn2TjCbhin55dGkezaY&libraries=geometry"></script>
<script>
    var geocoder = null;
    var map = null;
    var customerMarker = null;
    var gmarkers = [];
    var closest = [];

    var error = function( error ) {
        stopTracking();
        alert( error.code + " : " + error.message );
    };

    var startTracking = function() {
        if( navigator.geolocation ) {
            watchId = navigator.geolocation.watchPosition( getLocation, error );
        } else {
            stopTracking();
            alert( "Browser not supports Geolocation" )
        }
    }
    var getLocation = function( position ) {
        var currentLocation = {"lat": position.coords.latitude, "lng": position.coords.longitude };
        var pt = new google.maps.LatLng(currentLocation.lat, currentLocation.lng );
        //publishMessage( listener, {"busId": 3, "currentGeoLocation": currentLocation } );
    };
    function callMe(){
        var currentLocation = {"station":"Current Location","lat":"${conductor.getBusAssignee().getBus().getCurrentGeoLocation().getLat()}", "lng":"${conductor.getBusAssignee().getBus().getCurrentGeoLocation().getLng()}"}
        var source = {"station":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSource()}","lat":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getLocation().getLat()}", "lng":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getLocation().getLng()}"}
        var destination = {"station":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getSource()}","lat":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getLocation().getLat()}", "lng":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getLocation().getLng()}"}
        busStations.unshift(source)
        busStations.push(destination)
        function initialize() {
            geocoder = new google.maps.Geocoder();
            var country = "Philippines";
            
            map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 6
            });
            geocoder.geocode( {'address' : country}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                }
            });
            var infowindow = new google.maps.InfoWindow();
            var marker, i;
            var bounds = new google.maps.LatLngBounds();
            document.getElementById('info').innerHTML = "found " + busStations.length + " locations<br>";
            for (i = 0; i < busStations.length; i++) {
              var pt = new google.maps.LatLng(busStations[i].lat, busStations[i].lng);
              bounds.extend(pt);
              marker = new google.maps.Marker({
                position: pt,
                map: map,
                address: busStations[i].station,
              });
              gmarkers.push(marker);
              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                  return function() {
                    infowindow.setContent(marker.html);
                    infowindow.open(map, marker);
                  }
                })
                (marker, i));
            }
            map.fitBounds(bounds);
        }
            function codeAddress(lagLng) {
              var numberOfResults = 25;
              var numberOfDrivingResults = 5;
              geocoder.geocode({
                'location': latLng
              }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  customerMarker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                  });
                  closest = findClosestN(results[0].geometry.location);
                  // get driving distance
                  calculateDistances(results[0].geometry.location, closest);
                } else {
                  alert('Geocode was not successful for the following reason: ' + status);
                }
              });
            }

            function findClosestN(pt) {
              var closest = [];
              document.getElementById('info').innerHTML += "processing " + gmarkers.length + "<br>";
              for (var i = 0; i < gmarkers.length; i++) {
                gmarkers[i].distance = google.maps.geometry.spherical.computeDistanceBetween(pt, gmarkers[i].getPosition());
                document.getElementById('info').innerHTML += "process " + i + ":" + gmarkers[i].getPosition().toUrlValue(6) + ":" + gmarkers[i].distance.toFixed(2) + "<br>";
                closest.push(gmarkers[i]);
              }
              closest.sort(sortByDist);
              return closest;
            }

            function sortByDist(a, b) {
              return (a.distance - b.distance)
            }

            function calculateDistances(pt, closest) {
              var service = new google.maps.DistanceMatrixService();
              var request = {
                origins: [pt],
                destinations: [],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
              };
              for (var i = 0; i < closest.length; i++) {
                request.destinations.push(closest[i].getPosition());
              }
              service.getDistanceMatrix(request, function(response, status) {
                if (status != google.maps.DistanceMatrixStatus.OK) {
                  alert('Error was: ' + status);
                } else {
                  var origins = response.originAddresses;
                  var destinations = response.destinationAddresses;
                  var outputDiv = document.getElementById('side_bar');

                  var results = response.rows[0].elements;
                  // save title and address in record for sorting
                  for (var i = 0; i < closest.length; i++) {
                    results[i].title = closest[i].title;
                    results[i].address = closest[i].address;
                  }
                  results.sort(sortByDistDM);
                  console.log(closest);
                  closes[0]
                  for (var i = 0; ( (i < closest.length)); i++) {
                    closest[i].setMap(map);
                    outputDiv.innerHTML += results[i].address + "<br>" + results[i].distance.text + ' appoximately ' + results[i].duration.text + '<br><hr>';
                  }
                }
              });
            }

            function sortByDistDM(a, b) {
              return (a.distance.value - b.distance.value)
            }
</script>
<c:forEach var="station" items="${conductor.getBusAssignee().getBus().getBusTrips().getCompanyBusStations()}">
    <script>
        busStations.push({ "station":"${station.getStationName()}", "lat":"${station.getLocation().getLat()}", "lng":"${station.getLocation().getLng()}"})
    </script>
</c:forEach>
<script>
    callMe();
    google.maps.event.addDomListener(window, 'load', initialize);
    
</script>
</body>
</html>
