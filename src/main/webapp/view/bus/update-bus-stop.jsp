<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<spring:eval expression="T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails(31)" var="conductor"/>
<div class="modal" ui-if="updatebusstop" ui-shared-state="updatebusstop">
        <div class="modal-backdrop in"></div>
        <div class="modal-dialog">
            <div class="modal-content">
                <form id = "updatebusstop">
                    <div class="modal-header">
                        <button class="close"
                                ui-turn-off="updatebusstop">&times;</button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group has-success has-feedback">
                            <label>Next Bus Stop.</label>
                            <select class="form-control" id="bus-stations">
                                <option>Select Bus Station:</option>
                                <c:forEach var="station" items="${conductor.getBusAssignee().getBus().getBusTrips().getCompanyBusStations()}">
                                    <option value = "${station.getStationId()}" <c:if test="${station.getStationId() == conductor.getBusAssignee().getBus().getNextBusStop().getStationId()}">SELECTED</c:if> > ${station.getStationName()}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button ui-turn-off="updatebusstop" class="btn btn-default">Close</button>
                        <button type = "submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>