<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>Onsen-UI-Mobile App-Login-Screen - Bootsnipp.com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/css/onsenui.css"/>
    <link rel="stylesheet" href="/static/css/onsen-css-components.css"/>
    <script src="/static/js/angular.min.js"></script>
    <script src="/static/js/onsenui.min.js"></script>
    <script src="/static/js/angular-onsenui.min.js"></script>

    <style type="text/css">
      .login-form {
        text-align: center;
        width: 80%;
        margin: 60px auto 0;
      }

      input[type=email], input[type=password] {
        display: block;
        width: 100%;
        margin: 0 auto;
        outline: none;
        height: 100%;
        padding-top: 15px;
        padding-bottom: 16px;
      }

      .forgot-password {
        display: block;
        margin: 8px auto 0 auto;
        font-size: 14px;
      }
      .button--large--cta {
        position: relative;
        display: inline-block;
        vertical-align: top;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-background-clip: padding-box;
        background-clip: padding-box;
        padding: 0;
        margin: 0;
        font: inherit;
        color: inherit;
        background: transparent;
        border: none;
        line-height: normal;
        font-family: -apple-system, 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        font-weight: 400;
        font-size: 17px;
        cursor: default;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        height: auto;
        text-decoration: none;
        padding: 4px 10px;
        font-size: 17px;
        line-height: 32px;
        letter-spacing: 0;
        color: #fff;
        vertical-align: middle;
        background-color: #1aefa1;
        border: 0px solid currentColor;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        -webkit-transition: none;
        -moz-transition: none;
        -o-transition: none;
        transition: none;
        border: none;
        background-color: #1aefa1;
        color: #fff;
        font-size: 17px;
        font-weight: 500;
        line-height: 36px;
        padding: 4px 12px;
        width: 100%;
        text-align: center;
        display: block;
      }
      .button--large--cta:hover {
        -webkit-transition: none;
        -moz-transition: none;
        -o-transition: none;
        transition: none;
      }
      .button--large--cta:focus {
        outline: 0;
      }
      .button--large--cta:active {
        color: #fff;
        background-color: #1aefa1;
        -webkit-transition: none;
        -moz-transition: none;
        -o-transition: none;
        transition: none;
        opacity: 0.2;
      }
      .button--large--cta:disabled,
      .button--large--cta[disabled] {
        opacity: 0.3;
        cursor: default;
        pointer-events: none;
      }
    </style>
  </head>
  <body>
    <ons-page>
    <ons-toolbar>
        <div class="center">Log In</div>
        <div class="left"><ons-back-button>Home</ons-back-button></div>
      <div class="center"></div>
    </ons-toolbar>

      <div class="login-form">

        <input type="email" class="text-input--underbar" placeholder="Email" value="" autofocus="" required>
        <input type="password" class="text-input--underbar" placeholder="Password" value="" required>
        <br><br>
        <button class="button--large--cta" >Login</button>
        <br><br>
        <ons-button modifier="quiet" class="forgot-password">Forgot password?</ons-button>

        <p align="center">OR</p>
        <ons-button modifier="large" class="signup-button">Sign Up</ons-button>
   </div>
    </ons-page>

  </body>
</html>
