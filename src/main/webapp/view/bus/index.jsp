<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication property="principal" var="user"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails("${user.getUsername()}")' var="conductor"/>
<html lang="en" ng-app="myApp" ng-controller="MainController">
<head>
    <meta charset="UTF-8">
    <title>Bus Monitoring System - Conductor Module</title>
    <link rel="stylesheet" href="/static/css/onsenui.min.css">
    <link rel="stylesheet" href="/static/css/onsen-css-components.min.css">
    <link rel="stylesheet" href="/static/css/font-awesome.css">
    <script src="/static/js/angular.min.js"></script>
    <script src="/static/js/angular-route.min.js"></script>
    <script src="/static/js/onsenui.min.js"></script>
    <script src="/static/js/angular-onsenui.min.js"></script>
    <script src="/static/jquery/jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" href="/static/css/activity-stream.css">
    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <script src="/static/js/sockjs/sockjs.min.js"></script>
    <script src="/static/js/stomp/stomp.min.js"></script>
    <script src="/static/js/websocket.js"></script>
    <script src="/static/js/notify.js"></script>
    <style>
         body{font-size: 17px !important;
         }
         .toolbar{
            background: bottom no-repeat #ba0101c2;
            padding-top: 10;
         }
         .toolbar__center, .toolbar__title {
            color: #ffffff;
        }
        .toolbar-button{
            color:white;
        }
        .toolbar, .toolbar__item {
            height: 60px;
        }
        .page, .page__background, .page__content {
            background-color: #E5E5E5 !important;
        }
        .page {
            color: black;
        }
        .list-to {
            background: #ff8d8dd9;
        }
    </style>
    <script>
        var nextStop,isReverse = 0;
         <c:if test="${conductor.getBusAssignee().getBus().getTravelToDestination() != null}" >
            isReverse = ${conductor.getBusAssignee().getBus().getTravelToDestination()}
         </c:if>;
        var companyId = "${conductor.getCompanyId()}";
        var id = "${conductor.getBusAssignee().getBus().getBusId()}";
        var assigneeId = "${conductor.getBusAssignee().getAssigneeId()}";
        var plateNo = "${conductor.getBusAssignee().getBus().getPlateNumber()}";
        var busId = "${conductor.getBusAssignee().getBus().getBusId()}";
        var busTripId = ${conductor.getBusAssignee().getBus().getBusTrips().companyBusTripId};
        var stationId = "${conductor.getBusAssignee().getBus().getNextBusStop().getStationId()}";
        var baseUrl = "api/v1"
        var availableSeat = "${conductor.getBusAssignee().getBus().getAvailableSeat()}";
        var subscribers = function(){
                // stompClient.subscribe('/topic/conductor/reserve-seat/'+id, function (slot) {
                //     var r = JSON.parse( slot.body );
                //     $.notify('Received new slot reservation \nto the next stop\n with the code '+r.code+'.', 'success')
                //     var reserved = '<ons-list-item tappable="" modifier="longdivider" class="list-item list-item--longdivider"><div class="center list-item__center list-item--longdivider__center">'+
                //                             '<ons-row>'+
                //                                 '<ons-col>'+r.code+'</ons-col>'+
                //                                 '<ons-col>'+r.reservedSlot+'</ons-col>'+
                //                             '</ons-row>'+
                //                         '</div></ons-list-item>';
                //     document.getElementById('reserved').innerHTML += reserved;
                // });
            }
        connect(subscribers);
    </script>
</head>

<body >


<ons-navigator id="myNavigator" page="splitter.html">
    <div id="map-canvas" style="width:100%;height:500px;"></div>
</ons-navigator>
<template id="splitter.html">
    <ons-page>
        <ons-splitter id="mySplitter">
            <ons-splitter-side id="sidemenu" page="view/bus/html/menu.jsp" swipeable width="250px" collapse swipe-target-width="50px">
            </ons-splitter-side>
            <ons-splitter-content page="tabbar.html">
            </ons-splitter-content>
        </ons-splitter>
    </ons-page>
</template>

<!-- Third navigation component: Tabbar. This will disappear if the first or second components change their content. -->
<template id="tabbar.html">
    <ons-page id="tabbarPage">
        <ons-toolbar>
            <div class="left">
                <ons-toolbar-button component="button/menu">
                    <ons-icon icon="ion-navicon, material:md-menu" size="32px, material:24px"></ons-icon>
                </ons-toolbar-button>
            </div>
            <div class="center" ng-model="title">Bus Monitoring System</div>
        </ons-toolbar>
        <ons-if>
            <ons-fab position="right bottom" component="button/new-task">
                <ons-icon icon="md-edit"></ons-icon>
            </ons-fab>
        </ons-if>

        <ons-tabbar swipeable id="appTabbar" position="auto">
            <ons-tab title="Home" page="/view/bus/html/home.jsp" active></ons-tab>
            <ons-tab title="Profile" page="/view/bus/html/profile.jsp"></ons-tab>
            <ons-tab title="Activity Logs" page="/bus-client/log-activities" ></ons-tab>
        </ons-tabbar>
        <script>
              ons.getScriptPage().addEventListener('prechange', function(event) {
                if (event.target.matches('#appTabbar')) {
                  event.currentTarget.querySelector('ons-toolbar .center').innerHTML = event.tabItem.getAttribute('title');
                }
              });
        </script>
        <div id="map-canvas" style="width:100%;height:300px;"></div>
        <script>

        </script>
    </ons-page>
</template>


<ons-fab position="bottom center" class="tracking" ng-style="trackingColor" ng-click="enableTracking()">
    <ons-icon icon="md-power"></ons-icon>
</ons-fab>
<script src="/static/js/app.js"></script>
<script src="/static/js/controllers.js"></script>
<script src="/static/js/mobile.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRMdeKYLeFVApPCn2TjCbhin55dGkezaY&libraries=geometry"></script>
<c:if test="${ not empty conductor.getBusAssignee()}">
<script>
    <c:if test="${  conductor.getBusAssignee().getBus().getNextBusStop() != null }">
     nextStop = {"id":${conductor.getBusAssignee().getBus().getNextBusStop().getStationId()}, "type":"next_stop","title":"${conductor.getBusAssignee().getBus().getNextBusStop().getStationName()}","lat":${conductor.getBusAssignee().getBus().getNextBusStop().getLocation().getLat()}, "lng":${conductor.getBusAssignee().getBus().getNextBusStop().getLocation().getLng()}}
    </c:if>
    function callMe(){
        <c:if test="${conductor.getBusAssignee().getBus().getCurrentGeoLocation() != null }">
            currentLocation = {"type":"location","title":"Current Location","lat":${conductor.getBusAssignee().getBus().getCurrentGeoLocation().getLat()}, "lng":${conductor.getBusAssignee().getBus().getCurrentGeoLocation().getLng()}}
        </c:if>

        src = {"id":${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSourceId()}, "type":"Source","title":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSource()}","lat":${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getLocation().getLat()}, "lng":${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getLocation().getLng()}}
        destination = {"id":${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSourceId()}, "type":"Destination","title":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getSource()}","lat":${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getLocation().getLat()}, "lng":${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getLocation().getLng()}}

//        busStations.unshift(source)
//        busStations.push(destination)
        for (i = 0; i < busStations.length; i++) {
          var pt = new google.maps.LatLng(busStations[i].lat, busStations[i].lng);
          marker = new google.maps.Marker({
            position: pt,
            title: busStations[i].id.toString(),
            address: busStations[i].title + '|' + busStations[i].type ,
          });
          gmarkers.push(marker);
        }
        //codeAddress(currentLocation)
    }
</script>
</c:if>
<c:forEach var="station" items="${conductor.getBusAssignee().getBus().getBusTrips().getCompanyBusStations()}">
    <script>
        busStations.push({ "id":${station.getBusStation().getStationId()}, "type":"bus_stop","title":"${station.getBusStation().getStationName()}", "lat":${station.getBusStation().getLocation().getLat()}, "lng":${station.getBusStation().getLocation().getLng()}, "priority":"${station.getPriority()}"})
    </script>
</c:forEach>
<script>
    callMe()
    <c:if test="${ conductor.getBusAssignee().getBus().getTravelToDestination() == 1 }">
        sortByPriorityDesc()
    </c:if>
    <c:if test="${ conductor.getBusAssignee().getBus().getTravelToDestination() == 0 }">
        sortByPriority()
    </c:if>
    geocoder = new google.maps.Geocoder();
</script>
</body>
</html>
