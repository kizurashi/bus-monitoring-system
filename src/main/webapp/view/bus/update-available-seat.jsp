<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<spring:eval expression="T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails(31)" var="conductor"/>
<div class="modal" ui-if="updateavailableseat" ui-shared-state="updateavailableseat">
  <div class="modal-backdrop in"></div>
  <div class="modal-dialog">
    <div class="modal-content">
        <form id = "updateavailableseat">
          <div class="modal-header">
            <button class="close" ui-turn-off="updateavailableseat">&times;</button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            <div class="form-group has-success has-feedback">
              <label>Update Available Seat</label>
              <input type="number" min="1" max="${conductor.getBusAssignee().getBus().getPassengerCapacity()}" class="form-control" id="availableseat"/>
            </div>
          </div>
          <div class="modal-footer">
            <button ui-turn-off="updateavailableseat" class="btn btn-default">Close</button>
            <button type = "submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
    </div>
  </div>
</div>
