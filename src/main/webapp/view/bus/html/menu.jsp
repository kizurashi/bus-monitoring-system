<ons-page><br>
  <ons-toolbar style="" id="list-to">
    <div class="left">
      <ons-toolbar-button onclick="fn.open()">
        <ons-icon icon="md-menu"></ons-icon>
      </ons-toolbar-button>
    </div>
    <div class="center">
      Main
    </div>
  </ons-toolbar>
  <ons-list>
    <ons-list-item ng-click="loadView(0)" tappable>
      Home
    </ons-list-item>
    <ons-list-item ng-click="loadView(1)" tappable>
      Profile
    </ons-list-item>
    <ons-list-item ng-click="loadView(2)" tappable>
      Activity Logs
    </ons-list-item>
    <ons-list-item ng-click="pushPage('/view/bus/html/update-available-seat.jsp', '')" tappable>
      Update Available Seat
    </ons-list-item>
    <ons-list-item ng-click="pushPage('/view/bus/html/update-bus-stop.jsp', '')" tappable>
        Update Bus Stop
    </ons-list-item>
    <ons-list-item ng-click="pushPage('/view/bus/html/change-source-destination.jsp', '')" tappable>
        Reverse Origin/Destination
    </ons-list-item>
    <ons-list-item ng-click="loadArrivalDeparture()" tappable>
        Arrival/Departure Time
    </ons-list-item>
    <ons-list-item ng-click="pushPage('/view/bus/html/change-password.jsp', '')" tappable>
        Change Password
    </ons-list-item>
    <ons-list-item  tappable>
        <a href="/logout">Logout</a>
    </ons-list-item>
  </ons-list>
</ons-page>