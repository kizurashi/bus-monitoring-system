<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication property="principal" var="user"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails("${user.getUsername()}")' var="conductor"/>
<ons-page id="newTaskPage"><br>
  <ons-toolbar>
    <div class="left"><ons-back-button>Home</ons-back-button></div>
    <div class="center">Bus Stop</div>
  </ons-toolbar>
  <ons-list-title>Update next stop</ons-list-title>
  <ons-list>
    <ons-list-item class="input-items">
    <div class="left">
      <ons-icon icon="md-face" class="list-item__icon"> Next Stop</ons-icon>
    </div>
      <div class="center">
        <ons-select required style="width:100%;" id="bus-stations">
              <option>Select Next Stop:</option>
              <c:forEach var="station" items="${conductor.getBusAssignee().getBus().getBusTrips().getCompanyBusStations()}">
                  <option value = "${station.getBusStation().getStationId()}" <c:if test="${station.getBusStation().getStationId() == conductor.getBusAssignee().getBus().getNextBusStop().getStationId()}">SELECTED</c:if> > ${station.getBusStation().getStationName()}</option>
              </c:forEach>
        </ons-select>
      </div>
    </ons-list-item>
  </ons-list>
  <ons-button modifier="large" ng-click="updateNextStop()">Update Next Stop</ons-button>
</ons-page>