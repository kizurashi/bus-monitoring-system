<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication property="principal" var="user"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails("${user.getUsername()}")' var="conductor"/>
<ons-page id="newTaskPage"><br>
  <ons-toolbar>
    <div class="left"><ons-back-button>Home</ons-back-button></div>
    <div class="center">Change Password</div>
  </ons-toolbar>
  <ons-list>
    <ons-list-item class="input-items">
      <div class="left">
        <ons-icon icon="md-face" class="list-item__icon"> Current Password</ons-icon>
      </div>
      <div class="center">
        <input type="hidden" value="${conductor.getUser().getUsername()}" id="username">
        <ons-input type="password"  id="currentpassword" float></ons-input>
      </div>
    </ons-list-item>
    <ons-list-item class="input-items">
      <div class="left">
        <ons-icon icon="md-face" class="list-item__icon"> New Password</ons-icon>
      </div>
      <div class="center">
        <ons-input type="password"  id="newpassword" float></ons-input>
      </div>
    </ons-list-item>
    <ons-list-item class="input-items">
        <div class="left">
          <ons-icon icon="md-face" class="list-item__icon"> Confirm Password</ons-icon>
        </div>
      <div class="center">
        <input type="hidden"  name="conductorId" readonly value="${conductor.getConductorId()}"></input>
        <ons-input type="password"  id="confirmpassword"float></ons-input>
      </div>
    </ons-list-item>
  </ons-list>
    <ons-button modifier="large" ng-click="changePassword()">Change Password</ons-button>
</ons-page>
