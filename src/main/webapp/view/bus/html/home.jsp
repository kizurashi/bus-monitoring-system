<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication property="principal" var="user"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails("${user.getUsername()}")' var="conductor"/>
<ons-page ng-app="myApp" ng-controller="MainController">
    <p class="intro">

        <center>Welcome, <span id="user">${user.getUsername()}.</span></center>
    </p>
    <div ng-model="trackingStatus" id="trackingStatus" style="margin:10px;">Click the power button below to enable tracking this bus.</div>

    <ons-list-title>Bus Tracking Information</ons-list-title>
    <ons-list modifier="inset">
        <ons-list-item tappable modifier="longdivider">
            <ons-row>
                <ons-col>Origin</ons-col>
                <ons-col ng-model="origin" id="origin">
                    <c:choose>
                        <c:when test="${ conductor.getBusAssignee().getBus().getTravelToDestination() == 1 }">
                            ${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getSource()}
                        </c:when>
                        <c:otherwise>
                            ${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSource()}
                        </c:otherwise>
                    </c:choose>
                </ons-col>
            </ons-row>
        </ons-list-item>
        <ons-list-item tappable modifier="longdivider">
            <ons-row>
                <ons-col>Destination</ons-col>
                <ons-col ng-model="destination" id="destination">
                    <c:choose>
                        <c:when test="${ conductor.getBusAssignee().getBus().getTravelToDestination() == 1 }">
                            ${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSource()}
                        </c:when>
                        <c:otherwise>
                            ${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getSource()}
                        </c:otherwise>
                    </c:choose>
                </ons-col>
            </ons-row>
        </ons-list-item>
        <ons-list-item tappable modifier="longdivider">
            <ons-row>
                <ons-col>Bus Stop</ons-col>
                <ons-col ng-model="nextBusStop" id="nextBusStop">${conductor.getBusAssignee().getBus().getNextBusStop().getStationName()}</ons-col>
            </ons-row>
        </ons-list-item>
        <ons-list-item tappable modifier="longdivider">
            <ons-row>
                <ons-col>Passenger Capacity</ons-col>
                <ons-col ng-model="passengerCapacity" id="passengerCapacity">${conductor.getBusAssignee().getBus().getPassengerCapacity()}</ons-col>
            </ons-row>
        </ons-list-item>
        <ons-list-item tappable modifier="longdivider">
            <ons-row>
                <ons-col>Available Passenger <br>Seat</ons-col>
                <ons-col ng-model="availableSeat" id="availableSeat">${conductor.getBusAssignee().getBus().getAvailableSeat()}</ons-col>
            </ons-row>
        </ons-list-item>
        <ons-list-item tappable modifier="longdivider">
            <ons-row>
                <ons-col>Latitude</ons-col>
                <ons-col ng-model="latitude" id="latitude">N/A</ons-col>
            </ons-row>
        </ons-list-item>
        <ons-list-item tappable modifier="longdivider">
            <ons-row>
                <ons-col>Longitude</ons-col>
                <ons-col ng-model="longitude" id="longitude">N/A</ons-col>
            </ons-row>
        </ons-list-item>
        <ons-list-item tappable modifier="longdivider">
            <ons-row>
                <ons-col>Speed</ons-col>
                <ons-col ng-model="speed" id="speed">N/A</ons-col>
            </ons-row>
        </ons-list-item>
    </ons-list>
    <style>
      .intro {
        text-align: center;
        padding: 0 20px;
        margin-top: 40px;
      }

      ons-card {
        cursor: pointer;
        color: #333;
      }

      .card__title,
      .card--material__title {
        font-size: 20px;
      }
    </style>
</ons-page>
