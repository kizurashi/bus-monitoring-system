<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<ons-page>
    <br><br>
    <ons-list-title>Bus Reserved Slots</ons-list-title>
    <ons-list modifier="inset">
        <ons-list-item tappable modifier="longdivider">
            <ons-row>
                <ons-col></ons-col>
                <ons-col ng-model="origin" id="origin">
                    <ons-input type="text" required id="searchCode" ng-change="searchCode()" ng-model="code" placeholder="Search Reserved Seat Code..."></ons-input>
                </ons-col>
            </ons-row>
        </ons-list-item>
        <ons-list-item tappable modifier="longdivider">
            <ons-row>
                <ons-col><strong>Code</strong></ons-col>
                <ons-col><strong>Number of Slot(s)</strong></ons-col>
            </ons-row>
        </ons-list-item>
        <div id="reserved">
        <c:forEach var="reservedSeat" items="${reservedSeats.getContents()}">
            <ons-list-item tappable modifier="longdivider">
                <ons-row>
                    <ons-col>${reservedSeat.getCode()}</ons-col>
                    <ons-col>${reservedSeat.getReservedSlot()}</ons-col>
                </ons-row>
            </ons-list-item>
        </c:forEach>
        </div>
        <ons-list-item tappable modifier="longdivider">
            <ons-button ng-disabled="currentPage == 1" ng-click="currentPageRS(currentPage-1)">
                Previous
            </ons-button>
            {{currentPage}}/{{ ${reservedSeats.getTotalPage()} }}
            <ons-button ng-disabled="currentPage >= ${reservedSeats.getTotalPage()} " ng-click="currentPageRS(currentPage+1)">
                Next
            </ons-button>
        </ons-list-item>
    </ons-list>
    
</ons-page>
