<ons-page id="newTaskPage"><br>
  <ons-toolbar>
    <div class="left"><ons-back-button>Home</ons-back-button></div>
    <div class="center">Log New Activitiy</div>
  </ons-toolbar>
  <ons-list-title>Add new activity log</ons-list-title>
  <ons-list>
    <ons-list-item class="input-items">
      <div class="left">
        <ons-icon icon="md-face" class="list-item__icon"> Status</ons-icon>
      </div>
      <div class="center">
       <ons-select style="width: 100%" required id="select-status" data-ng-model="activity.selectedstatus">
                   <option value="">Select status:</option>
                   <option value="Malfunction">Malfunction</option>
                   <option value="Traffic">Traffic</option>
                   <option value="Accident">Accident</option>
                   <!--<option value="Arrived Bus Station">Arrived Bus Station</option>
                   <option value="Arrived Destination">Arrived Destination</option>
                   <option value="Others">Others</option>-->
       </ons-select>
      </div>
    </ons-list-item>
    <ons-list-item class="input-items">
<!--<div class="left">
  <ons-icon icon="md-face" class="list-item__icon"> Message</ons-icon>
</div>
      <div class="center">
        <ons-input type="text" required placeholder="Enter message" name="message" id="activity-message" ng-model="activity.message" value="jkdf"></ons-input>
      </div>
    </ons-list-item>-->
  </ons-list>
    <ons-button modifier="large" ng-click="logActivity()">Log Activity</ons-button>
  <br>
</ons-page>

