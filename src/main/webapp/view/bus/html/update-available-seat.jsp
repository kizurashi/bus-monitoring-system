<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<spring:eval expression="T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails(31)" var="conductor"/>
<ons-page id="newTaskPage"><br>
  <ons-toolbar>
    <div class="left"><ons-back-button>Home</ons-back-button></div>
    <div class="center">Bus Available Seat</div>
  </ons-toolbar>
  <ons-list-title>Update Available Seat</ons-list-title>
  <ons-list>
    <ons-list-item class="input-items">
        <div class="left">
          <ons-icon icon="md-face" class="list-item__icon"> Available Seat</ons-icon>
        </div>
      <div class="center">
        <ons-input ng-model="availableseatform" type="text" style="width:100%" maxLength="2" pattern="{2}[0-9]" id="availableseat"></ons-input>
        <ons-button modifier="small" ng-click="addSeat()"><span>Add</span></ons-button>
        <ons-button modifier="small" ng-click="minusSeat()"><span>Minus</span></ons-button>
       </div>
    </ons-list-item>
  </ons-list>
  <ons-button modifier="large" ng-click="updateAvailableSeat()">Update Available Seat</ons-button>
</ons-page>
