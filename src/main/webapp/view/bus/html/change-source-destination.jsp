<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication property="principal" var="user"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails("${user.getUsername()}")' var="conductor"/>
<ons-page id="newTaskPage"><br>
  <ons-toolbar>
    <div class="left"><ons-back-button>Home</ons-back-button></div>
    <div class="center">Source - Destination</div>
  </ons-toolbar>
  <ons-list-title>Change origin to destination</ons-list-title>
  <ons-list>
    <ons-list-item class="input-items">
    <div class="left">
      <ons-icon icon="md-face" class="list-item__icon"> Origin</ons-icon>
    </div>
      <div class="center">
        <ons-input type="text"  id="origin-form" ng-model="origin" readonly
            value='<c:choose><c:when test="${ conductor.getBusAssignee().getBus().getTravelToDestination() == 1 }">${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getSource()}</c:when> <c:otherwise>${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSource()}</c:otherwise></c:choose>'></ons-input>
       </div>
    </ons-list-item>
    <ons-list-item class="input-items">
    <div class="left">
      <ons-icon icon="md-face" class="list-item__icon"> Destination</ons-icon>
    </div>
      <div class="center">
        <input type="hidden" id="reverse" name="travelToDestination"  ng-model="origin" readonly value="${conductor.getBusAssignee().getBus().getTravelToDestination()}"/>
        <input type="hidden"  name="busId" readonly value="${conductor.getBusAssignee().getBus().getBusId()}"/>
        <ons-input type="text"  id="destination-form" readonly
        value="<c:choose><c:when test="${ conductor.getBusAssignee().getBus().getTravelToDestination() == 1 }">${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSource()}</c:when><c:otherwise>${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getSource()}</c:otherwise></c:choose>" float></ons-input>
      </div>
    </ons-list-item>
    <ons-list-item>
            <label id="enabled-label" class="center" for="switch1">
              Reverse
            </label>
            <div class="right">
              <ons-switch input-id="switch1" id="switchsu" ng-model="test" ng-change="reverse()" >
              </ons-switch>
            </div>
    </ons-list-item>
  </ons-list>
  <ons-if>
    <ons-button modifier="large" ng-click="saveReverseOriginDestination()">Reverse Origin/Destination</ons-button>
  </ons-if>
</ons-page>
