<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication property="principal" var="user"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails("${user.getUsername()}")' var="conductor"/>
<ons-page>
  <br>
        <ons-card>
          <div class="w3-hide-large"></div>
            <div class="w3-container w3-card-2" id="apartment">
                <h4 class="w3-text-green"><strong>Basic Information</strong></h4><br>
                <div class="w3-row w3-large">
                  <div class="w3-col s12">
                    <p><i class="fa fa-fw fa-male"></i> Name: ${conductor.getFirstName()} ${conductor.getLastName()}</p>
                    <p><i class="fa fa-fw"></i> Mobile: ${conductor.getContactNo()}</p>
                  </div>
                </div>
                <hr>
            </div>
        </ons-card>
        <hr style="background:gray;">
        <ons-card>
          <div class="w3-hide-large"></div>
            <div class="w3-container w3-card-2" id="apartment">
                <h4 class="w3-text-green"><strong>Driver Information</strong></h4><hr>
                <div class="w3-row w3-large">
                  <div class="w3-col s12">
                    <p><i class="fa fa-fw fa-male"></i> Name: ${conductor.getBusAssignee().getDriver().getFirstName()} ${conductor.getBusAssignee().getDriver().getLastName()}</p>
                    <p><i class="fa fa-fw"></i> Mobile: ${conductor.getBusAssignee().getDriver().getContactNo()}</p>
                  </div>
                </div>
                <hr>
            </div>
        </ons-card>
        <hr style="background:gray;">
        <ons-card>
          <div class="w3-hide-large"></div>
            <div class="w3-container w3-card-2" id="apartment">
                <h4 class="w3-text-green"><strong>Bus Information</strong></h4><hr>
                <div class="w3-row w3-large">
                  <div class="w3-col s6">
                    <p><i class="fa fa-fw fa-male"></i> Plate #: </p>
                    <p><i class="fa fa-fw"></i> Origin: </p>
                    <p><i class="fa fa-fw"></i> Source: </p>
                  </div>
                  <div class="w3-col s6">
                    <p><i class="fa fa-fw"></i> ${conductor.getBusAssignee().getBus().getPlateNumber()}</p>
                    <p><i class="fa fa-fw "></i> ${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSource()}</p>
                    <p><i class="fa fa-fw"></i> ${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getSource()}</p>
                  </div>
                </div>
                <hr>
            </div>
        </ons-card>
</ons-page>
