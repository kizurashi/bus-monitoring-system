<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication property="principal" var="user"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails("${user.getUsername()}")' var="conductor"/>
<ons-page id="newTaskPage"><br>
  <ons-toolbar>
    <div class="left"><ons-back-button>Home</ons-back-button></div>
    <div class="center">Arrival/Departure Time</div>
  </ons-toolbar>
  <ons-list>
    <ons-list-item class="input-items">
      <div class="left">
         Departure Time
      </div>
      <div class="center">
        <ons-input type="text"  id="departure" float readonly value="${travellog.getDepartureTime()}"></ons-input>
      </div>
    </ons-list-item>
    <ons-list-item class="input-items">
        <div class="left">
          Arrival Time
        </div>
      <div class="center">
        <ons-input type="text"  id="arrival" float readonly value="${travellog.getArrivalTime()}"></ons-input>
      </div>
    </ons-list-item>
  </ons-list>
    <ons-button modifier="large" ng-click="arrivelDeparture('departure')" id="departurebutton" <c:if test="${not empty travellog.getTravelLogId()}" >disabled</c:if> >Set Current Time As Departure</ons-button>
    <ons-button modifier="large" ng-click="arrivelDeparture('arrival')" id="arrivalbutton" <c:if test="${empty travellog.getTravelLogId()}" >disabled</c:if>>Set Current Time As Arrival</ons-button>
</ons-page>