<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div ui-content-for="title">
  <span>Bus Reserved Seat </span>
</div>
<div class="scrollable">
  <div class="scrollable-content">
    <div class="section">
      <br>
      <div class="">
        <div class="w3-card-4">

          <div class="a-s--wrapper">
            <div class="form-group row">
              <div class="col-sm-3 pull-right">
                  <input type="text" class="form-control col-sm-2" placeholder="Search Code...">
              </div>
            </div>
            <hr>
              <div class="a-s--group-by-date">
                <div class="he-who-must-not-be-named">
                  <div class="a-s--title current-head">
                      <h3>Reserved Seat</h3>
                  </div>
                  <div class="activity-streams">
                    <c:forEach var = "reserved" items="${reservedSeats.getContents()}">
                        <div class="a-s--item">
                          <div class="a-s--icon">
                          </div>
                          <div class="a-s--body">
                              <h4>Code: ${reserved.code}</h4>
                              <h5>Station: ${reserved.getBusStation().getStationName()}</h5>
                              <p>Posted: <strong>${reserved.getCreated_at()}</strong></p>
                          </div>
                        </div>
                    </c:forEach>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
      <br><br><br>
    </div>
  </div>
</div>
