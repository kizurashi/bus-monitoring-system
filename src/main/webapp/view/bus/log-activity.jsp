<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<ons-page><br>
<ons-card >
  <div class="a-s--wrapper">
    <div class="a-s--group-by-date" id="logActivities">
      <c:forEach var = "activityByDate" items="${activities.getContents()}">
          <div class="he-who-must-not-be-named">
            <div class="a-s--title current-head" data-date="${activityByDate.getKey()}">
                <h3>${activityByDate.getKey()}</h3>
            </div>
            <div class="activity-streams activity-streams-${activityByDate.getKey()}">
              <c:forEach var = "activity" items= "${activityByDate.getValue()}">
                  <div class="a-s--item">
                    <div class="a-s--icon">
                    </div>
                    <div class="a-s--body">
                        <h4>Bus No: ${activity.getBus().getPlateNumber()}</h4>
                        <h5>Message: ${activity.getMessage()}</h5>
                        <p>Posted: <strong>${activity.getTimestamp().toString().split(' ')[1].substring(0, activity.getTimestamp().toString().split(' ')[1].length()-2)} | </strong>Status: <strong>${activity.getAction()} | </strong>Location: <strong>${activity.getLocation().getLocationName()}</strong></p>
                    </div>
                  </div>
              </c:forEach>
            </div>
          </div>
      </c:forEach>
    </div>
    <hr>
    <ons-button ng-disabled="currentPage == 1" ng-click="currentPageFN(currentPage-1)">
        Previous
    </ons-button>
    {{currentPage}}/{{ ${activities.getTotalPage()} }}
    <ons-button ng-disabled="currentPage >= ${activities.getTotalPage()} " ng-click="currentPageFN(currentPage+1)">
        Next
    </ons-button>
    <br><br>
  </div>
</ons-card>
</ons-page>
