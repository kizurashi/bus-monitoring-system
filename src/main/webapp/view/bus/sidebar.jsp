<div class="scrollable">
  <h1 class="scrollable-header app-name">Bus - Application System</h1>
  <div class="scrollable-content">
    <div class="list-group" ui-turn-off="uiSidebarLeft">
      <a class="list-group-item" href="/bus-client#!/">Home <i class="fa fa-chevron-right pull-right"></i></a>
      <a class="list-group-item" href="/bus-client#!/profile">Profile <i class="fa fa-chevron-right pull-right"></i></a>
      <a class="list-group-item" href="/bus-client#!/log-activity">Log Activity <i class="fa fa-chevron-right pull-right"></i></a>
      <a class="list-group-item" href="/bus-client#!/bus-reserved-seat">Bus Reserved Seat <i class="fa fa-chevron-right pull-right"></i></a>
    </div>
  </div>
</div>
