<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authentication property="principal" var="user"/>
<spring:eval expression='T(com.project.monitor.permission.service.AuthenticatedUserImpl).getConductorDetails("${user.getUsername()}")' var="conductor"/>
<html lang="en" ng-app="myApp" ng-controller="MainController">
<head>
    <meta charset="UTF-8">
    <title>Bus Monitoring System - Conductor Module</title>
    <link rel="stylesheet" href="/static/css/onsenui.min.css">
    <link rel="stylesheet" href="/static/css/onsen-css-components.min.css">
    <link rel="stylesheet" href="/static/css/font-awesome.css">
    <script src="/static/js/angular.min.js"></script>
    <script src="/static/js/angular-route.min.js"></script>
    <script src="/static/js/onsenui.min.js"></script>
    <script src="/static/js/angular-onsenui.min.js"></script>
    <script src="/static/jquery/jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" href="/static/css/activity-stream.css">
    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <script src="/static/js/sockjs/sockjs.min.js"></script>
    <script src="/static/js/stomp/stomp.min.js"></script>
    <script src="/static/js/websocket.js"></script>
    <style>
         body{font-size: 17px !important;
         }
         .toolbar{
            background: bottom no-repeat #ba0101c2;
            padding-top: 10;
         }
         .toolbar__center, .toolbar__title {
            color: #ffffff;
        }
        .toolbar-button{
            color:white;
        }
        .toolbar, .toolbar__item {
            height: 60px;
        }
        .page, .page__background, .page__content {
            background-color: #e65656d9 !important;
        }
        .page {
            color: #ffffff;
        }
        .list-to {
            background: #ff8d8dd9;
        }
    </style>
    <script>
        var companyId = "${conductor.getCompanyId()}";
        var id = "${conductor.getBusAssignee().getBus().getBusId()}";
        var plateNo = "${conductor.getBusAssignee().getBus().getPlateNumber()}";
        var stationId = "${conductor.getBusAssignee().getBus().getNextBusStop().getStationId()}";
        var baseUrl = "http://localhost:8090/api/v1"
        var busStations = [];
        var availableSeat = "${conductor.getBusAssignee().getBus().getAvailableSeat()}";
    </script>
</head>

<body >


<ons-navigator id="myNavigator" page="splitter.html">
    <div id="map-canvas" style="width:100%;height:500px;"></div>
</ons-navigator>
<template id="splitter.html">
    <ons-page>
        <ons-splitter id="mySplitter">
            <ons-splitter-side id="sidemenu" page="view/bus/html/menu.jsp" swipeable width="250px" collapse swipe-target-width="50px">
            </ons-splitter-side>
            <ons-splitter-content page="tabbar.html">
            </ons-splitter-content>
        </ons-splitter>
    </ons-page>
</template>

<!-- Third navigation component: Tabbar. This will disappear if the first or second components change their content. -->
<template id="tabbar.html">
    <ons-page id="tabbarPage">
        <ons-toolbar>
            <div class="left">
                <ons-toolbar-button component="button/menu">
                    <ons-icon icon="ion-navicon, material:md-menu" size="32px, material:24px"></ons-icon>
                </ons-toolbar-button>
            </div>
            <div class="center" ng-model="title">Bus Monitoring System</div>
        </ons-toolbar>
        <ons-if>
            <ons-fab position="right bottom" component="button/new-task">
                <ons-icon icon="md-edit"></ons-icon>
            </ons-fab>
        </ons-if>

        <ons-tabbar swipeable id="appTabbar" position="auto">

            <ons-tab title="Animations" page="/view/bus/html/home.jsp" active></ons-tab>
              <ons-tab title="Animations" page="/view/bus/html/profile.jsp"></ons-tab>
              <ons-tab title="Animations" page="/bus-client/log-activities" ></ons-tab>
              <ons-tab title="Animations" page="http://localhost:8090/bus-client/reservedSeat/${conductor.getBusAssignee().getBus().getBusId()}/${conductor.getBusAssignee().getBus().getNextBusStop().getStationId()}"></ons-tab>
        </ons-tabbar>
        <script>
              ons.getScriptPage().addEventListener('prechange', function(event) {
                if (event.target.matches('#appTabbar')) {
                  event.currentTarget.querySelector('ons-toolbar .center').innerHTML = event.tabItem.getAttribute('title');
                }
              });
        </script>
        <div id="map-canvas" style="width:100%;height:300px;"></div>
        <script>

        </script>
    </ons-page>
</template>

<ons-fab position="bottom center" class="tracking" ng-style="trackingColor" ng-click="enableTracking()">
    <ons-icon icon="md-power"></ons-icon>
</ons-fab>

<script src="/static/js/app.js"></script>
<script src="/static/js/controllers.js"></script>
<script src="/static/js/mobile.js"></script>
<script>
    connect();
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBRMdeKYLeFVApPCn2TjCbhin55dGkezaY&libraries=geometry"></script>
<script>
    var geocoder = null;
    var map = null;
    var customerMarker = null;
    var gmarkers = [];
    var closest = [];
    var nextStop;
    function callMe(){
        var currentLocation = {"type":"Source","title":"Current Location","lat":${conductor.getBusAssignee().getBus().getCurrentGeoLocation().getLat()}, "lng":${conductor.getBusAssignee().getBus().getCurrentGeoLocation().getLng()}}

        nextStop = {"id":${conductor.getBusAssignee().getBus().getNextBusStop().getStationId()}, "type":"next_stop","title":"${conductor.getBusAssignee().getBus().getNextBusStop().getStationName()}","lat":"${conductor.getBusAssignee().getBus().getNextBusStop().getLocation().getLat()}", "lng":"${conductor.getBusAssignee().getBus().getNextBusStop().getLocation().getLng()}"}

        var source = {"id":${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSourceId()}, "type":"Source","title":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSource()}","lat":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getLocation().getLat()}", "lng":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getLocation().getLng()}"}
        var destination = {"id":${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getSource().getSourceId()}, "type":"Destination","title":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getSource()}","lat":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getLocation().getLat()}", "lng":"${conductor.getBusAssignee().getBus().getBusTrips().getBusTrips().getDestination().getLocation().getLng()}"}
        busStations.unshift(source)
        busStations.push(destination)
        geocoder = new google.maps.Geocoder();
            var country = "Philippines";
            
            map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 6
            });
            geocoder.geocode( {'address' : country}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                }
            });
            var infowindow = new google.maps.InfoWindow();
            var marker, i;
            var bounds = new google.maps.LatLngBounds();
            for (i = 0; i < busStations.length; i++) {
              var pt = new google.maps.LatLng(busStations[i].lat, busStations[i].lng);
              bounds.extend(pt);
              marker = new google.maps.Marker({
                position: pt,
                map: map,
                title: busStations[i].id.toString(),
                address: busStations[i].title + '|' + busStations[i].type ,
              });
              gmarkers.push(marker);
              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                  return function() {
                    infowindow.setContent(marker.html);
                    infowindow.open(map, marker);
                  }
                })
                (marker, i));
            }
            map.fitBounds(bounds);

            codeAddress(currentLocation)
    }

    var codeAddress = function (address) {
      var numberOfResults = 25;
      var numberOfDrivingResults = 5;
      var latlng = {lat: address.lat, lng: address.lng};
      geocoder.geocode({
        'location': latlng
      }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          customerMarker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
          });
          closest = findClosestN(results[0].geometry.location);
          // get driving distance
          console.log( results[0] )
          calculateDistances(results[0].geometry.location, closest);
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
      });
    }

    var findClosestN = function (pt) {
      var closest = [];
      for (var i = 0; i < gmarkers.length; i++) {
        gmarkers[i].distance = google.maps.geometry.spherical.computeDistanceBetween(pt, gmarkers[i].getPosition());
        closest.push(gmarkers[i]);
      }
      closest.sort(sortByDist);
      return closest;
    }

    var sortByDist = function(a, b) {
      return (a.distance - b.distance)
    }

    var calculateDistances = function (pt, closest) {
      var service = new google.maps.DistanceMatrixService();
      var request = {
        origins: [pt],
        destinations: [],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
      };
      for (var i = 0; i < closest.length; i++) {
        request.destinations.push(closest[i].getPosition());
      }
      service.getDistanceMatrix(request, function(response, status) {
        if (status != google.maps.DistanceMatrixStatus.OK) {
          alert('Error was: ' + status);
        } else {
          var origins = response.originAddresses;
          var destinations = response.destinationAddresses;
          var outputDiv = document.getElementById('side_bar');

          var results = response.rows[0].elements;
          // save title and address in record for sorting
          for (var i = 0; i < closest.length; i++) {
            results[i].title = closest[i].title;
            results[i].address = closest[i].address;
          }
          results.sort(sortByDistDM);
          var potentialSource = results[0];
          var potentialCurrentStop = results[1];
          if( potentialSource.address.search(/Source|Destination/g) >= 0 ){
            var type = potentialSource.address.split('|')[1];
            if( type == 'source' ) {
              //set next stop
            }
            if( type == 'Destination') {
              //reverse source/destination
              console.log( 'arrived destination' )

            }
          } else {
            if( potentialSource.address.search( nextStop.title ) >= 0 ) {
              console.log( 'arrived current stop' )
              //set next stop
            }
            //if( potentialSource)
          }
          // for (var i = 0; ( (i < closest.length)); i++) {
          //   closest[i].setMap(map);
          //   console.log(results[i].address + "<br>" + results[i].distance.text + ' appoximately ' + results[i].duration.text + '<br><hr>');
          // }
        }
      });
    }

    var sortByDistDM = function(a, b) {
      return (a.distance.value - b.distance.value)
    }
</script>
<c:forEach var="station" items="${conductor.getBusAssignee().getBus().getBusTrips().getCompanyBusStations()}">
    <script>
        busStations.push({ "id":${station.getStationId()}, "type":"bus_stop","title":"${station.getStationName()}", "lat":"${station.getLocation().getLat()}", "lng":"${station.getLocation().getLng()}"})
    </script>
</c:forEach>
<script>
    callMe();
</script>
</body>
</html>
