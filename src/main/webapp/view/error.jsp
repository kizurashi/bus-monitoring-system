<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Error page - HTML</title>
  <link rel="stylesheet" href="css/style.css">
  <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic&subset=latin,cyrillic" rel="stylesheet" type="text/css" />
  <link href="/static/css/error.css" media="all" rel="stylesheet" type="text/css" />
  <link href="/favicon.png" rel="shortcut icon" type="image/vnd.microsoft.icon">
</head>

<body>
  <div class="layout">
		<div class="title">${status}</div>
		<div class="text icon_500">Error ${error}<br><br>${message}</div>
		<div class="buttons">
            <a href="/"<p class="text_buttons_intro">Return to home page!</p></a>
		</div>
  </div>
</body>
</html>
